@echo off

set compiler_flags=-Od -MTd -nologo -D_CRT_SECURE_NO_WARNINGS -fp:fast -fp:except- -Gm- -GR- -EHa- -Zo -Oi -WX -W4 -wd4201 -wd4100 -wd4101 -wd4189 -wd4505 -wd4127 -wd4701 -wd4702 -FC -Z7 -I%PROJECT_PATH%/src -I%CPP_LIB_PATH% %PROJECT_COMPILER_FLAGS%

set linker_flags=-incremental:no -STACK:100000000000 -opt:ref %PROJECT_LINKER_FLAGS%

IF NOT exist %BUILD_PATH% ( mkdir %BUILD_PATH% )
pushd %BUILD_PATH%

cl %compiler_flags% %PROJECT_PATH%\src\main.cpp -Fe%PROJECT_NAME%.exe /link %linker_flags%

popd

