inline bool
is_power_of_two(uint32_t value) {
    return value != 0 && (value & (value - 1)) == 0;
}

uint32_t log2(uint32_t value) {
    uint32_t n = 0;
    while (value > 1) {
        value /= 2;
        n++;
    }
    return n;
}

enum {
    RAX = 0,
    RCX = 1,
    RDX = 2,
    RBX = 3,
    RSP = 4,
    RBP = 5,
    RSI = 6,
    RDI = 7,
    R8 = 8,
    R9 = 9,
    R10 = 10,
    R11 = 11,
    R12 = 12,
    R13 = 13,
    R14 = 14,
    R15 = 15
};

typedef uint8_t Register;

enum Scale : uint8_t {
    X1 = 0,
    X2 = 1,
    X4 = 2,
    X8 = 3
};

enum Mode {
    INDIRECT = 0,
    INDIRECT_BYTE_DISPLACED = 1,
    INDIRECT_DISPLACED = 2,
    DIRECT = 3,
};

enum Condition_Code : uint8_t {
    O,
    NO,
    B,
    NB,
    E,
    NE,
    NA,
    A,
    S,
    NS,
    P,
    NP,
    L,
    NL,
    NG,
    G,
    NAE = B,
    C = B,
    AE = NB,
    NC = NB,
    Z = E,
    NZ = NE,
    BE = NA,
    NBE = A,
    PE = P,
    PO = NP,
    NGE = L,
    GE = NL,
    LE = NG,
    NLE = G
};

uint8_t *code;
uint8_t *next_code;

internal_proc void
emit(uint8_t value) {
    *next_code = value;
    next_code++;
}

internal_proc void
emit4(uint32_t value) {
    emit( value        & 0xFF);
    emit((value >>  8) & 0xFF);
    emit((value >> 16) & 0xFF);
    emit((value >> 24) & 0xFF);
}

internal_proc void
emit8(uint64_t value) {
    emit( value        & 0xFF);
    emit((value >>  8) & 0xFF);
    emit((value >> 16) & 0xFF);
    emit((value >> 24) & 0xFF);
    emit((value >> 32) & 0xFF);
    emit((value >> 40) & 0xFF);
    emit((value >> 48) & 0xFF);
    emit((value >> 56) & 0xFF);
}

internal_proc void
emit_mod_rx_rm(uint8_t mod, uint8_t rx, uint8_t rm) {
    assert(mod < 4);
    assert(rx < 16);
    assert(rm < 16);
    emit((mod << 6) | ((rx & 7) << 3) | (rm & 7));
}

internal_proc void
emit_rex_indexed(uint8_t rx, uint8_t base, uint8_t index) {
    emit(0x48 | (base >> 3) | ((index >> 3) << 1) | ((rx >> 3) << 2));
}

internal_proc void
emit_rex(uint8_t rx, uint8_t base) {
    emit_rex_indexed(rx, base, 0);
}

internal_proc void
emit_direct(uint8_t rx, Register reg) {
    emit_mod_rx_rm(DIRECT, rx, reg);
}

internal_proc void
emit_indirect(uint8_t rx, Register base) {
    assert((base & 7) != RSP);
    assert((base & 7) != RBP);
    emit_mod_rx_rm(INDIRECT, rx, base);
}

internal_proc void
emit_indirect_displaced_rip(uint8_t rx, int32_t displacement) {
    emit_mod_rx_rm(INDIRECT, rx, RBP);
    emit4(displacement);
}

internal_proc void
emit_indirect_byte_displaced(uint8_t rx, Register base, int8_t displacement) {
    assert((base & 7) != RSP);
    emit_mod_rx_rm(INDIRECT_BYTE_DISPLACED, rx, base);
    emit(displacement);
}

internal_proc void
emit_indirect_displaced(uint8_t rx, Register base, int32_t displacement) {
    assert((base & 7) != RSP);
    emit_mod_rx_rm(INDIRECT_DISPLACED, rx, base);
    emit4(displacement);
}

internal_proc void
emit_indirect_indexed(uint8_t rx, Register base, Register index, Scale scale) {
    assert((base & 7) != RBP);
    emit_mod_rx_rm(INDIRECT, rx, RSP);
    emit_mod_rx_rm(scale, index, base);
}

internal_proc void
emit_indirect_indexed_byte_displaced(uint8_t rx, Register base, Register index,
        Scale scale, int8_t displacement)
{
    emit_mod_rx_rm(INDIRECT_BYTE_DISPLACED, rx, RSP);
    emit_mod_rx_rm(scale, index, base);
    emit(displacement);
}

internal_proc void
emit_indirect_indexed_displaced(uint8_t rx, Register base, Register index,
        Scale scale, int32_t displacement)
{
    emit_mod_rx_rm(INDIRECT_DISPLACED, rx, RSP);
    emit_mod_rx_rm(scale, index, base);
    emit4(displacement);
}

internal_proc void
emit_displaced(uint8_t rx, int32_t displacement) {
    emit_mod_rx_rm(INDIRECT, rx, RSP);
    emit_mod_rx_rm(X1, RSP, RBP);
    emit4(displacement);
}

#define EMIT_I(operation, source_immediate) \
    emit_rex(0, 0); \
    emit_##operation##_I(); \
    emit4(source_immediate)

#define EMIT_MOV64_R_I(destination, source_immediate) \
    emit_rex(0, destination); \
    emit(0xB8 + (destination & 7)); \
    emit8(source_immediate)

#define EMIT_MOV64_RAX_OFF(source_offset) \
    emit_rex(0, 0); \
    emit(0xA1); \
    emit8(source_offset)

#define EMIT_MOV64_OFF_RAX(destination_offset) \
    emit_rex(0, 0); \
    emit(0xA3); \
    emit8(destination_offset)

#define EMIT_R_R(operation, destination, source) \
    emit_rex(destination, source); \
    emit_##operation##_R(); \
    emit_direct(destination, source)

#define EMIT_R_RIPD(operation, destination, source_displacement) \
    emit_rex(destination, 0); \
    emit_##operation##_R(); \
    emit_indirect_displaced_rip(destination, source_displacement);

#define EMIT_R_D(operation, destination, source_displacement) \
    emit_rex(destination, 0); \
    emit_##operation##_R(); \
    emit_displaced(destination, source_displacement)

#define EMIT_R_M(operation, destination, source) \
    emit_rex(destination, source); \
    emit_##operation##_R(); \
    emit_indirect(destination, source)

#define EMIT_R_MD1(operation, destination, source, source_displacement) \
    emit_rex(destination, source); \
    emit_##operation##_R(); \
    emit_indirect_byte_displaced(destination, source, source_displacement)

#define EMIT_R_MD(operation, destination, source, source_displacement) \
    emit_rex(destination, source); \
    emit_##operation##_R(); \
    emit_indirect_displaced(destination, source, source_displacement)

#define EMIT_R_SIB(operation, destination, source_base, source_scale, source_index) \
    emit_rex_indexed(destination, source_base, source_index); \
    emit_##operation##_R(); \
    emit_indirect_indexed(destination, source_base, source_index, source_scale)

#define EMIT_R_SIBD1(operation, destination, source_base, source_scale, source_index, source_displacement) \
    emit_rex_indexed(destination, source_base, source_index); \
    emit_##operation##_R(); \
    emit_indirect_indexed_byte_displaced(destination, source_base, source_index, source_scale, source_displacement)

#define EMIT_R_SIBD(operation, destination, source_base, source_scale, source_index, source_displacement) \
    emit_rex_indexed(destination, source_base, source_index); \
    emit_##operation##_R(); \
    emit_indirect_indexed_displaced(destination, source_base, source_index, source_scale, source_displacement)

#define EMIT_M_R(operation, destination, source) \
    emit_rex(source, destination); \
    emit_##operation##_M(); \
    emit_indirect(source, destination)

#define EMIT_D_R(operation, destination_displacement, source) \
    emit_rex(source, 0); \
    emit_##operation##_M(); \
    emit_displaced(source, destination_displacement)

#define EMIT_RIPD_R(operation, destination_displacement, source) \
    emit_rex(source, 0); \
    emit_##operation##_M(); \
    emit_indirect_displaced_rip(source, destination_displacement);

#define EMIT_MD1_R(operation, destination, destination_displacement, source) \
    emit_rex(source, destination); \
    emit_##operation##_M(); \
    emit_indirect_byte_displaced(source, destination, destination_displacement)

#define EMIT_MD_R(operation, destination, destination_displacement, source) \
    emit_rex(source, destination); \
    emit_##operation##_M(); \
    emit_indirect_displaced(source, destination, destination_displacement)

#define EMIT_SIB_R(operation, destination_base, destination_scale, destination_index, source) \
    emit_rex_indexed(source, destination_base, destination_index); \
    emit_##operation##_M(); \
    emit_indirect_indexed(source, destination_base, destination_index, destination_scale)

#define EMIT_SIBD1_R(operation, destination_base, destination_scale, destination_index, destination_displacement, source) \
    emit_rex_indexed(source, destination_base, destination_index); \
    emit_##operation##_M(); \
    emit_indirect_indexed_byte_displaced(source, destination_base, destination_index, destination_scale, destination_displacement)

#define EMIT_SIBD_R(operation, destination_base, destination_scale, destination_index, destination_displacement, source) \
    emit_rex_indexed(source, destination_base, destination_index); \
    emit_##operation##_M(); \
    emit_indirect_indexed_displaced(source, destination_base, destination_index, destination_scale, destination_displacement)

#define EMIT_R_I(operation, destination, source_immediate) \
    emit_rex(0, destination); \
    emit_##operation##_I(); \
    emit_direct(extension_##operation##_I, destination); \
    emit4(source_immediate)

#define EMIT_R_I1(operation, destination, source_immediate) \
    emit_rex(0, destination); \
    emit_##operation##_I1(); \
    emit_direct(extension_##operation##_I1, destination); \
    emit4(source_immediate)

#define EMIT_M_I(operation, destination, source_immediate) \
    emit_rex(0, destination); \
    emit_##operation##_I(); \
    emit_indirect(extension_##operation##_I, destination); \
    emit4(source_immediate)

#define EMIT_D_I(operation, destination_displacement, source_immediate) \
    emit_rex(0, 0); \
    emit_##operation##_I(); \
    emit_displaced(extension_##operation##_I, destination_displacement); \
    emit4(source_immediate)

#define EMIT_RIPD_I(operation, destination_displacement, source_immediate) \
    emit_rex(0, 0); \
    emit_##operation##_I(); \
    emit_indirect_displaced_rip(extension_##operation##_I, destination_displacement); \
    emit4(source_immediate)

#define EMIT_MD1_I(operation, destination, destination_displacement, source_immediate) \
    emit_rex(0, destination); \
    emit_##operation##_I(); \
    emit_indirect_byte_displaced(extension_##operation##_I, destination, destination_displacement); \
    emit4(source_immediate)

#define EMIT_MD_I(operation, destination, destination_displacement, source_immediate) \
    emit_rex(0, destination); \
    emit_##operation##_I(); \
    emit_indirect_displaced(extension_##operation##_I, destination, destination_displacement); \
    emit4(source_immediate)

#define EMIT_SIB_I(operation, destination_base, destination_scale, destination_index, source_immediate) \
    emit_rex_indexed(0, destination_base, destination_index); \
    emit_##operation##_I(); \
    emit_indirect_indexed(extension_##operation##_I, destination_base, destination_index, destination_scale); \
    emit4(source_immediate)

#define EMIT_SIBD1_I(operation, destination_base, destination_scale, destination_index, destination_displacement, source_immediate) \
    emit_rex_indexed(0, destination_base, destination_index); \
    emit_##operation##_I(); \
    emit_indirect_indexed_byte_displaced(extension_##operation##_I, destination_base, destination_index, destination_scale, destination_displacement); \
    emit4(source_immediate)

#define EMIT_SIBD_I(operation, destination_base, destination_scale, destination_index, destination_displacement, source_immediate) \
    emit_rex_indexed(0, destination_base, destination_index); \
    emit_##operation##_I(); \
    emit_indirect_indexed_displaced(extension_##operation##_I, destination_base, destination_index, destination_scale, destination_displacement); \
    emit4(source_immediate)

#define EMIT_X_R(operation, source) \
    emit_rex(0, source); \
    emit_##operation##_X(); \
    emit_direct(extension_##operation##_X, source)

#define EMIT_X_RIPD(operation, source_displacement) \
    emit_rex(0, 0); \
    emit_##operation##_X(); \
    emit_indirect_displaced_rip(extension_##operation##_X, source_displacement);

#define EMIT_X_D(operation, source_displacement) \
    emit_rex(0, 0); \
    emit_##operation##_X(); \
    emit_displaced(extension_##operation##_X, source_displacement)

#define EMIT_X_M(operation, source) \
    emit_rex(0, source); \
    emit_##operation##_X(); \
    emit_indirect(extension_##operation##_X, source)

#define EMIT_X_MD1(operation, source, source_displacement) \
    emit_rex(0, source); \
    emit_##operation##_X(); \
    emit_indirect_byte_displaced(extension_##operation##_X, source, source_displacement)

#define EMIT_X_MD(operation, source, source_displacement) \
    emit_rex(0, source); \
    emit_##operation##_X(); \
    emit_indirect_displaced(extension_##operation##_X, source, source_displacement)

#define EMIT_X_SIB(operation, source_base, source_scale, source_index) \
    emit_rex_indexed(0, source_base, source_index); \
    emit_##operation##_X(); \
    emit_indirect_indexed(extension_##operation##_X, source_base, source_index, source_scale)

#define EMIT_X_SIBD1(operation, source_base, source_scale, source_index, source_displacement) \
    emit_rex_indexed(0, source_base, source_index); \
    emit_##operation##_X(); \
    emit_indirect_indexed_byte_displaced(extension_##operation##_X, source_base, source_index, source_scale, source_displacement)

#define EMIT_X_SIBD(operation, source_base, source_scale, source_index, source_displacement) \
    emit_rex_indexed(0, source_base, source_index); \
    emit_##operation##_X(); \
    emit_indirect_indexed_displaced(extension_##operation##_X, source_base, source_index, source_scale, source_displacement)

#define EMIT_C_I(operation, condition_code, source_immediate) \
    emit_##operation##_C_I(condition_code); \
    emit4(source_immediate)

#define OP1R(operation, opcode) \
    void emit_##operation##_R() { \
        emit(opcode); \
    }

#define OP1M(operation, opcode) \
    void emit_##operation##_M() { \
        emit(opcode); \
    }

#define OP1I(operation, opcode, extension) \
    void emit_##operation##_I() { \
        emit(opcode); \
    } \
    enum { extension_##operation##_I = extension };

#define OP1I1(operation, opcode, extension) \
    void emit_##operation##_I1() { \
        emit(opcode); \
    } \
    enum { extension_##operation##_I1 = extension };

#define OP1X(operation, opcode, extension) \
    void emit_##operation##_X() { \
        emit(opcode); \
    } \
    enum { extension_##operation##_X = extension };

#define OP2CI(operation, opcode) \
    void emit_##operation##_C_I(Condition_Code condition_code) { \
        emit(0x0F); \
        emit(opcode + condition_code); \
    }

OP1R(MOV, 0x8B)
OP1M(MOV, 0x89)
OP1I(MOVSX, 0xC7, 0x00)

OP1I1(SHL, 0xC1, 0x04)

OP1I1(SHR, 0xC1, 0x05)

OP1R(ADD, 0x03)
OP1M(ADD, 0x01)
OP1I(ADD, 0x81, 0x00)

OP1R(SUB, 0x2B)
OP1M(SUB, 0x29)
OP1I(SUB, 0x81, 0x05)

OP1X(MUL, 0xF7, 0x04)

OP1X(DIV, 0xF7, 0x06)

OP1I(CMP, 0x81, 0x07)

OP1I(JMP, 0xE9, 0x00)

OP2CI(J, 0x80)

void test_emitter() {
    EMIT_MOV64_R_I(RBX, 0x12345678deadbeefull);
    EMIT_MOV64_RAX_OFF(0x12345678deadbeefull);
    EMIT_MOV64_OFF_RAX(0x12345678deadbeefull);
    EMIT_R_R(MOV, RAX, R10);
    EMIT_M_R(MOV, RAX, R10);
    EMIT_I(JMP, 0x1234);
    EMIT_C_I(J, NZ, 0x1234);
    EMIT_C_I(J, NZ, 0x1234);
    EMIT_C_I(J, NZ, 0x1234);
    EMIT_D_I(ADD, 0x1234578, 0xDEADBEEF);
    EMIT_RIPD_I(ADD, 0x1234578, 0xDEADBEEF);
    for (uint8_t d = RAX; d <= R15; d++) {
        Register destination = (Register)d;
        EMIT_X_R(MUL, destination);
        if ((destination & 7) != RSP) {
            EMIT_X_MD1(MUL, destination, 0x12);
            EMIT_X_MD(MUL, destination, 0x12345678);
            if ((destination & 7) != RBP) {
                EMIT_X_M(MUL, destination);
                EMIT_X_SIB(MUL, destination, X4, R8);
                EMIT_X_SIBD1(MUL, destination, X4, R8, 0x12);
                EMIT_X_SIBD(MUL, destination, X4, R8, 0x12345678);
            }
        }
        EMIT_R_I(ADD, destination, 0xDEADBEEF);
        if ((destination & 7) != RSP) {
            EMIT_MD1_I(ADD, destination, 0x12, 0xDEADBEEF);
            EMIT_MD_I(ADD, destination, 0x12345678, 0xDEAFBEEF);
            if ((destination & 7) != RBP) {
                EMIT_SIB_I(ADD, destination, X4, R8, 0xDEADBEEF);
            }
        }
        EMIT_R_RIPD(ADD, destination, 0x1235678);
        EMIT_R_D(ADD, destination, 0x1235678);
        EMIT_RIPD_R(ADD, 0x1235678, destination);
        EMIT_D_R(ADD, 0x1235678, destination);
        for (uint8_t s = RAX; s <= R15; s++) {
            Register source = (Register)s;
            EMIT_R_R(ADD, destination, source);
            if ((source & 7) != RBP) {
                EMIT_R_SIB(ADD, destination, source, X4, destination);
                EMIT_R_SIBD1(ADD, destination, source, X4, destination, 0x12);
                EMIT_R_SIBD(ADD, destination, source, X4, destination, 0x12345678);
            }
            if ((destination & 7) != RBP) {
                EMIT_SIB_R(ADD, destination, X4, source, source);
                EMIT_SIBD1_R(ADD, destination, X4, source, 0x12, source);
                EMIT_SIBD_R(ADD, destination, X4, source, 0x12345678, source);
            }
            if ((source & 7) != RSP && (source & 7) != RBP) {
                EMIT_R_M(ADD, destination, source);
                EMIT_R_MD1(ADD, destination, source, 0x12);
                EMIT_R_MD(ADD, destination, source, 0x12345678);
            }
            if ((destination & 7) != RSP && (destination & 7) != RBP) {
                EMIT_M_R(ADD, destination, source);
                EMIT_MD1_R(ADD, destination, 0x12, source);
                EMIT_MD_R(ADD, destination, 0x12345678, source);
            }
            if ((source & 7) == RSP) {
                EMIT_R_SIB(ADD, destination, source, X1, RSP);
            }
        }
    }
}

uint32_t free_register_mask;

internal_proc void
initialize_free_registers() {
    Register available_registers[] = {RCX, RBX, RSI, RDI, R8, R9, R10, R11, R12, R13, R14, R15};
    for (size_t i = 0; i < sizeof(available_registers) / sizeof(*available_registers); i++) {
        free_register_mask |= 1 << available_registers[i];
    }
}

internal_proc Register
allocate_register() {
    assert(free_register_mask != 0);
    DWORD free_register;
    _BitScanForward(&free_register,free_register_mask);
    free_register_mask &= ~(1 << free_register);
    return (Register)free_register;
}

internal_proc void
free_register(Register allocated_register) {
    assert((free_register_mask & (1 << allocated_register)) == 0);
    free_register_mask |= 1 << allocated_register;
}

enum Operand_Type {
    OP_NULL,
    OP_FRAME_OFFSET,
    OP_REGISTER,
    OP_IMMEDIATE,
    OP_ADDRESS
};

struct Operand {
    Operand_Type type;

    union {
        Register operand_register;
        int32_t operand_immediate;
    };
};

internal_proc void
emit_operand_to_register(Operand *operand, Register target_register) {
    if (operand->type == OP_IMMEDIATE) {
        EMIT_R_I(MOVSX, target_register, operand->operand_immediate);
    } else if (operand->type == OP_REGISTER) {
        if (operand->operand_register != target_register) {
            EMIT_R_R(MOV, target_register, operand->operand_register);
        }
    }
}

internal_proc void
emit_mul(Operand *destination, Operand *operand) {
    if (destination->type == OP_IMMEDIATE && operand->type == OP_IMMEDIATE) {
        destination->operand_immediate *= operand->operand_immediate;
    } else if (operand->type == OP_IMMEDIATE && is_power_of_two(operand->operand_immediate)) {
        EMIT_R_I1(SHL, destination->operand_register, log2(operand->operand_immediate));
    } else if (destination->type == OP_IMMEDIATE && is_power_of_two(destination->operand_immediate)) {
        EMIT_R_I1(SHL, operand->operand_register, log2(destination->operand_immediate));
    } else {
        emit_operand_to_register(destination, RAX);
        EMIT_X_R(MUL, operand->operand_register);
        EMIT_R_R(MOV, destination->operand_register, RAX);
    }
}

internal_proc void
emit_div(Operand *destination, Operand *operand) {
    if (destination->type == OP_IMMEDIATE && operand->type == OP_IMMEDIATE) {
        assert(operand->operand_immediate != 0);
        destination->operand_immediate /= operand->operand_immediate;
    } else if (operand->type == OP_IMMEDIATE && is_power_of_two(operand->operand_immediate)) {
        EMIT_R_I1(SHR, destination->operand_register, log2(operand->operand_immediate));
    } else {
        emit_operand_to_register(destination, RAX);
        EMIT_X_R(DIV, operand->operand_register);
        EMIT_R_R(MOV, destination->operand_register, RAX);
    }
}

internal_proc void
emit_add(Operand *destination, Operand *operand) {
    if (destination->type == OP_IMMEDIATE && operand->type == OP_IMMEDIATE) {
        destination->operand_immediate += operand->operand_immediate;
    } else if (destination->type == OP_IMMEDIATE) {
        EMIT_R_I(ADD, operand->operand_register, destination->operand_immediate);
        destination->type = OP_REGISTER;
        destination->operand_register = operand->operand_register;
    } else {
        if (operand->type == OP_IMMEDIATE) {
            EMIT_R_I(ADD, destination->operand_register, operand->operand_immediate);
        } else {
            assert(operand->type == OP_REGISTER);
            EMIT_R_R(ADD, destination->operand_register, operand->operand_register);
        }
    }
}

internal_proc void
emit_sub(Operand *destination, Operand *operand) {
    if (destination->type == OP_IMMEDIATE && operand->type == OP_IMMEDIATE) {
        destination->operand_immediate -= operand->operand_immediate;
    } else if (destination->type == OP_IMMEDIATE) {
        EMIT_R_I(ADD, operand->operand_register, -destination->operand_immediate);
        destination->type = OP_REGISTER;
        destination->operand_register = operand->operand_register;
    } else {
        if (operand->type == OP_IMMEDIATE) {
            EMIT_R_I(SUB, destination->operand_register, operand->operand_immediate);
        } else {
            assert(operand->type == OP_REGISTER);
            EMIT_R_R(SUB, destination->operand_register, operand->operand_register);
        }
    }
}

