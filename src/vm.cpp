#define DEBUG_TRACE_EXEC

enum Op_Kind {
    OP_CONST,
    OP_ADD,
    OP_SUB,
    OP_MUL,
    OP_DIV,
    OP_NEG,
    OP_RET,
};

struct Vm_Chunk {
    uint8_t *code;
    Val     *consts;
    int     *lines;
};

enum { STACK_MAX = 256 };
struct Vm {
    Vm_Chunk *chunk;
    uint8_t  *ip;
    Val       stack[STACK_MAX];
    Val      *sp;
} vm;

enum Interp_Result {
    INTERP_OK,
    INTERP_COMPILE_ERROR,
    INTERP_RUNTIME_ERROR,
};

internal_proc int disassemble_inst(Vm_Chunk *chunk, int offset);

internal_proc void
stack_push(Val value) {
    if ( vm.sp == vm.stack + STACK_MAX ) {
        error(zero_site, "stack overflow!");
    }

    *vm.sp = value;
    vm.sp++;
}

internal_proc Val
stack_pop() {
    --vm.sp;
    return *vm.sp;
}

internal_proc void
vm_init() {
    vm.chunk = 0;
    vm.sp = vm.stack;
}

internal_proc void
vm_free() {
    //
}

internal_proc Interp_Result
run() {
#define READ_BYTE()  (*vm.ip++)
#define READ_CONST() (vm.chunk->consts[READ_BYTE()])
#define BINARY_OP(op) do { Val b = stack_pop(); Val a = stack_pop(); stack_push(a op b); } while(false)

    for (;;) {
#ifdef DEBUG_TRACE_EXEC
        printf("          ");
        for (Val* slot = vm.stack; slot < vm.sp; slot++) {
            printf("[ ");
            print(*slot);
            printf(" ]");
        }
        printf("\n");
        disassemble_inst(vm.chunk, (int)(vm.ip - vm.chunk->code));
#endif

        uint8_t inst = READ_BYTE();
        switch ( inst ) {
            case OP_RET: {
                print(stack_pop());
                printf("\n");
                return INTERP_OK;
            } break;

            case OP_ADD: {
                BINARY_OP(+);
            } break;

            case OP_SUB: {
                BINARY_OP(-);
            } break;

            case OP_MUL: {
                BINARY_OP(*);
            } break;

            case OP_DIV: {
                BINARY_OP(/);
            } break;

            case OP_NEG: {
                stack_push(-stack_pop());
            } break;

            case OP_CONST: {
                stack_push(READ_CONST());
            } break;
        }
    }

#undef READ_CONST
#undef READ_BYTE
}

internal_proc Interp_Result
interp(Vm_Chunk *chunk) {
    vm.chunk = chunk;
    vm.ip    = chunk->code;

    return run();
}

internal_proc Vm_Chunk *
chunk_new() {
    Vm_Chunk *result = (Vm_Chunk *)xmalloc(sizeof(Vm_Chunk));

    result->code   = 0;
    result->consts = 0;
    result->lines  = 0;

    return result;
}

internal_proc void
chunk_add(Vm_Chunk *chunk, uint8_t op, int line) {
    buf_push(chunk->code, op);
    buf_push(chunk->lines, line);
}

internal_proc void
chunk_free(Vm_Chunk *chunk) {
    buf_free(chunk->code);
    buf_free(chunk->consts);
    buf_free(chunk->lines);
}

internal_proc uint8_t
const_add(Vm_Chunk *chunk, Val value) {
    uint8_t index = (uint8_t)buf_len(chunk->consts);
    buf_push(chunk->consts, value);

    return index;
}

internal_proc int
inst_simple(char *name, int offset) {
    printf("%s\n", name);

    return offset + 1;
}

internal_proc int
inst_const(char *name, Vm_Chunk *chunk, int offset) {
    int index = chunk->code[offset + 1];

    printf("%-16s %4d '", name, index);
    print(chunk->consts[index]);
    printf("'\n");

    return offset + 2;
}

internal_proc int
disassemble_inst(Vm_Chunk *chunk, int offset) {
    printf("%04d ", offset);

    if (offset > 0 && chunk->lines[offset] == chunk->lines[offset - 1]) {
        printf("   | ");
    } else {
        printf("%4d ", chunk->lines[offset]);
    }

    uint8_t inst = chunk->code[offset];
    switch ( inst ) {
        case OP_CONST: {
            return inst_const("OP_CONST", chunk, offset);
        } break;

        case OP_ADD: {
            return inst_simple("OP_ADD", offset);
        } break;

        case OP_SUB: {
            return inst_simple("OP_SUB", offset);
        } break;

        case OP_MUL: {
            return inst_simple("OP_MUL", offset);
        } break;

        case OP_DIV: {
            return inst_simple("OP_DIV", offset);
        } break;

        case OP_NEG: {
            return inst_simple("OP_NEG", offset);
        } break;

        case OP_RET: {
            return inst_simple("OP_RET", offset);
        } break;

        default: {
            printf("nicht unterstützte operation!\n");
            return offset + 1;
        } break;
    }
}

internal_proc void
disassemble_code(Vm_Chunk *chunk, char *name) {
    printf("=== %s ===\n", name);
    for ( int offset = 0; offset < buf_len(chunk->code);) {
        offset = disassemble_inst(chunk, offset);
    }
}

internal_proc void
vm_test() {
    vm_init();
    Vm_Chunk *chunk = chunk_new();

    int line_num = 123;

    chunk_add(chunk, OP_CONST, line_num);
    chunk_add(chunk, const_add(chunk, val_f64(1.2)), line_num);
    chunk_add(chunk, OP_CONST, line_num);
    chunk_add(chunk, const_add(chunk, val_f64(3.4)), line_num);
    chunk_add(chunk, OP_ADD, line_num);

    chunk_add(chunk, OP_CONST, line_num);
    chunk_add(chunk, const_add(chunk, val_f64(5.6)), line_num);
    chunk_add(chunk, OP_DIV, line_num);

    chunk_add(chunk, OP_NEG, line_num);
    chunk_add(chunk, OP_RET, line_num);

    disassemble_code(chunk, "test chunk");
    interp(chunk);

    vm_free();
    chunk_free(chunk);
}
