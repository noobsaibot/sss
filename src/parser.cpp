internal_proc Expr *        parse_expr(Lexer *lex);
internal_proc Expr *        parse_expr_unary(Lexer *lex);
internal_proc Decl *        parse_decl(Lexer *lex);
internal_proc Stmt *        parse_stmt(Lexer *lex);
internal_proc Stmt_Block    parse_stmts(Lexer *lex);
internal_proc Typespec*     parse_typespec(Lexer *lex);
internal_proc Decl *        parse_decl_tail(Lexer *lex, char *name);
internal_proc Lexer         init_compiler(char *filename);

struct Parser {
    Lexer *lexers;
    Lexer *current_lexer;
    size_t num_lexers;
};

global_var Parser parser;

internal_proc void
add_lexer(Lexer lex) {
    buf_push(parser.lexers, lex);
    parser.num_lexers = buf_len(parser.lexers);
}

internal_proc Site
get_site(Typespec *typespec) {
    return typespec->site;
}

internal_proc Site
get_site(Expr *expr) {
    return expr->site;
}

internal_proc Site
get_site(Decl *decl) {
    return decl->site;
}

internal_proc Site
get_site(Stmt *stmt) {
    return stmt->site;
}

internal_proc Site
get_site(Compound_Argument arg) {
    return arg.site;
}

internal_proc Site
get_site(Switch_Case switch_case) {
    return switch_case.site;
}

internal_proc char *
parse_name(Lexer *lex) {
    Expr *expr = parse_expr(lex);

    if ( expr->kind != EXPR_NAME ) {
        error(get_site(expr), "bezeichner erwartet!");
    }

    return expr->expr_name.value;
}

internal_proc char *
parse_str(Lexer *lex) {
    Expr *expr = parse_expr(lex);

    if ( expr->kind != EXPR_STR ) {
        error(get_site(expr), "string literal erwartet!");
        context(get_site(expr));
    }

    return expr->expr_str.value;
}

internal_proc uint64_t
parse_int(Lexer *lex) {
    Expr *expr = parse_expr(lex);

    if ( expr->kind != EXPR_INT ) {
        error(get_site(expr), "integer erwartet!");
    }

    return expr->expr_int.value;
}

internal_proc Note *
parse_notes(Lexer *lex) {
    Note *notes = 0;

    while ( match_token(lex, T_NOTE) ) {
        Token tag = expect_token(lex, T_IDENT);
        buf_push(notes, note_new(tag.site, tag.ident));
    }

    return notes;
}

internal_proc Expr *
parse_expr_base(Lexer *lex) {
    Expr *result = 0;

    Token token = lex->token;
    Site site = token.site;

    if ( is_token(lex, T_IDENT) ) {
        /* @TODO: sollte bool als eigener datentyp behandelt werden? */
        if ( is_keyword(lex, keyword_true) ) {
            result = expr_int(site, 1, 10);
        } else if ( is_keyword(lex, keyword_false) ) {
            result = expr_int(site, 0, 10);
        } else {
            result = expr_name(site, token.ident);
        }
        get_token(lex);
    } else if ( is_token(lex, T_STR) ) {
        result = expr_str(site, token.str_val);
        get_token(lex);
    } else if ( is_token(lex, T_INT) ) {
        result = expr_int(site, token.int_val, token.int_base);
        get_token(lex);
    } else if ( is_token(lex, T_CHAR) ) {
        result = expr_char(site, token.int_val);
        get_token(lex);
    } else if ( is_token(lex, T_FLOAT) ) {
        result = expr_float(site, token.float_val);
        get_token(lex);
    } else if ( match_token(lex, T_CALL_OPEN) ) {
        result = expr_paren(site, parse_expr(lex));
        expect_token(lex, T_CALL_CLOSE);
    } else if ( match_token(lex, T_BLOCK_BEGIN) ) {
        Compound_Argument *args = 0;
        Expr_Comp_Kind kind = EXPR_COMP_LIST;
        Comp_Arg_Kind arg_kind = COMPOUND_LIST;

        if ( !is_token(lex, T_BLOCK_END) ) {
            char *name = 0;
            Expr *expr = parse_expr(lex);
            Expr *index = 0;

            if ( expr->kind == EXPR_INDEX ) {
                arg_kind = COMPOUND_INDEX;
                index = expr->expr_index.index;
                expect_token(lex, T_EQL_ASSIGN);
                expr = parse_expr(lex);
            } else if ( expr->kind == EXPR_NAME ) {
                if ( match_token(lex, T_EQL_ASSIGN) ) {
                    kind = EXPR_COMP_NAME;
                    arg_kind = COMPOUND_NAME;
                    name = expr->expr_name.value;
                    expr = parse_expr(lex);
                }
            }

            buf_push(args, compound_argument(get_site(expr), name, index, expr, arg_kind));

            while ( match_token(lex, T_PARAM_SEPARATOR) && !is_token(lex, T_BLOCK_END) ) {
                arg_kind = COMPOUND_LIST;
                name = 0;
                expr = parse_expr(lex);

                if ( expr->kind == EXPR_INDEX ) {
                    arg_kind = COMPOUND_INDEX;
                    index = expr->expr_index.index;
                    expect_token(lex, T_EQL_ASSIGN);
                    expr = parse_expr(lex);
                } else if ( expr->kind == EXPR_NAME ) {
                    if ( match_token(lex, T_EQL_ASSIGN) ) {
                        kind = EXPR_COMP_NAME;
                        arg_kind = COMPOUND_NAME;
                        name = expr->expr_name.value;
                        expr = parse_expr(lex);
                    }
                }

                buf_push(args, compound_argument(get_site(expr), name, index, expr, arg_kind));
            }
        }
        expect_token(lex, T_BLOCK_END);

        result = expr_compound(site, kind, 0, args, buf_len(args));
    } else {
        expr_illegal->site = lex->token.site;
        expr_illegal->expr_name.value = lex->token.ident;

        return expr_illegal;
    }

    return result;
}

internal_proc Call_Arg
parse_procedure_call_argument(Lexer *lex) {
    Call_Arg result = {};

    Expr *expr = parse_expr(lex);

    if ( expr->kind != T_IDENT ) {
        result = call_arg(expr);
    }

    if ( is_token(lex, T_EQL_ASSIGN) ) {
        get_token(lex);

        result = call_arg(parse_expr(lex), expr);
    }

    return result;
}

internal_proc Expr *
parse_expr_field_or_call_or_index(Lexer *lex) {
    Site site = get_site(lex);
    Expr *expr = parse_expr_base(lex);

    while ( is_token(lex, T_CALL_OPEN) || is_token(lex, T_INDEX_OPEN) || is_token(lex, T_DOT) ) {
        if ( match_token(lex, T_CALL_OPEN) ) {
            Call_Arg *args = 0;

            if ( !is_token(lex, T_CALL_CLOSE) ) {
                buf_push(args, parse_procedure_call_argument(lex));
                while ( match_token(lex, T_PARAM_SEPARATOR) ) {
                    buf_push(args, parse_procedure_call_argument(lex));
                }
            }

            expect_token(lex, T_CALL_CLOSE);
            expr = expr_call(site, expr, args, buf_len(args));
        } else if ( match_token(lex, T_INDEX_OPEN) ) {
            Expr *index = parse_expr(lex);
            expect_token(lex, T_INDEX_CLOSE);
            expr = expr_index(site, expr, index);
        } else if ( match_token(lex, T_DOT ) ) {
            char *field = lex->token.ident;
            expect_token(lex, T_IDENT);
            expr = expr_field(site, expr, field);
        }
    }

    return expr;
}

internal_proc Expr *
parse_expr_range(Lexer *lex) {
    Site site = get_site(lex);
    Expr *left = parse_expr_field_or_call_or_index(lex);

    if ( match_token(lex, T_RANGE) ) {
        left = expr_binary(site, T_RANGE, left, parse_expr(lex));
    }

    return left;
}

internal_proc Expr *
parse_expr_offsetof(Site site, Lexer *lex) {
    expect_token(lex, T_CALL_OPEN);
    Typespec *typespec = parse_typespec(lex);
    expect_token(lex, T_PARAM_SEPARATOR);
    Expr *expr = parse_expr(lex);
    assert(expr->kind == EXPR_NAME);

    expect_token(lex, T_CALL_CLOSE);

    return expr_offsetof(site, typespec, expr);
}

internal_proc Expr *
parse_expr_cast_sizeof_typeof_alignof_offsetof(Lexer *lex) {
    Site site = get_site(lex);

    first_if  ( match_keyword( lex, keyword_cast ) ) {
        expect_token(lex, T_CALL_OPEN);
        Typespec *typespec = parse_typespec(lex);
        expect_token(lex, T_CALL_CLOSE);

        return expr_cast(site, typespec, parse_expr_range(lex));
    } else if ( match_keyword( lex, keyword_sizeof ) ) {
        expect_token(lex, T_CALL_OPEN);
        Typespec *typespec = parse_typespec(lex);
        expect_token(lex, T_CALL_CLOSE);

        return expr_sizeof(site, typespec);
    } else if ( match_keyword( lex, keyword_typeof ) ) {
        expect_token(lex, T_CALL_OPEN);
        Typespec *typespec = parse_typespec(lex);
        expect_token(lex, T_CALL_CLOSE);

        return expr_typeof(site, typespec);
    } else if ( match_keyword(lex, keyword_typeinfo) ) {
        expect_token(lex, T_CALL_OPEN);
        Expr *expr = parse_expr(lex);
        expect_token(lex, T_CALL_CLOSE);

        return expr_typeinfo(site, expr);
    } else if ( match_keyword(lex, keyword_alignof) ) {
        expect_token(lex, T_CALL_OPEN);
        Typespec *typespec = parse_typespec(lex);
        expect_token(lex, T_CALL_CLOSE);

        return expr_alignof(site, typespec);
    } else if ( match_keyword( lex, keyword_offsetof ) ) {
        return parse_expr_offsetof(site, lex);
    } else {
        return parse_expr_range(lex);
    }
}

internal_proc Expr *
parse_expr_unary(Lexer *lex) {
    Site site = get_site(lex);

    if ( is_unary(lex->token) ) {
        Token op = eat_token(lex);
        return expr_unary(site, op.kind, parse_expr_unary(lex));
    } else {
        return parse_expr_cast_sizeof_typeof_alignof_offsetof(lex);
    }
}

internal_proc Expr *
parse_expr_mul(Lexer *lex) {
    Site site = get_site(lex);
    Expr *left = parse_expr_unary(lex);

    while ( is_token(lex, T_MUL) || is_token(lex, T_DIV) || is_token(lex, T_MODULO) ||
            is_token(lex, T_LSHIFT) || is_token(lex, T_RSHIFT) || is_token(lex, T_BIT_AND) ||
            is_token(lex, T_BIT_OR) || is_token(lex, T_XOR) )
    {
        Token op = eat_token(lex);
        left = expr_binary(site, op.kind, left, parse_expr_unary(lex));
    }

    return left;
}

internal_proc Expr *
parse_expr_add(Lexer *lex) {
    Site site = get_site(lex);
    Expr *left = parse_expr_mul(lex);

    while ( is_token(lex, T_PLUS) || is_token(lex, T_MINUS) ) {
        Token op = eat_token(lex);
        left = expr_binary(site, op.kind, left, parse_expr_mul(lex));
    }

    return left;
}

internal_proc Expr *
parse_expr_cmp(Lexer *lex) {
    Site site = get_site(lex);
    Expr *left = parse_expr_add(lex);

    while ( is_cmp(lex->token) ) {
        Token op = eat_token(lex);
        left = expr_binary(site, op.kind, left, parse_expr_add(lex));
    }

    return left;
}

internal_proc Expr *
parse_expr_and(Lexer *lex) {
    Site site = get_site(lex);
    Expr *left = parse_expr_cmp(lex);

    while ( match_token(lex, T_AND) ) {
        left = expr_binary(site, T_AND, left, parse_expr_cmp(lex));
    }

    return left;
}

internal_proc Expr *
parse_expr_or(Lexer *lex) {
    Site site = get_site(lex);
    Expr *left = parse_expr_and(lex);

    while ( match_token(lex, T_OR) ) {
        left = expr_binary(site, T_OR, left, parse_expr_and(lex));
    }

    return left;
}

internal_proc Expr *
parse_expr_ternary(Lexer *lex) {
    Site site = get_site(lex);
    Expr *left = parse_expr_or(lex);

    if ( match_token(lex, '?') ) {
        Expr *then_expr = parse_expr(lex);
        expect_token(lex, T_TYPESPEC_SEPARATOR);

        left = expr_ternary(site, left, then_expr, parse_expr(lex));
    }

    return left;
}

internal_proc Expr *
parse_expr(Lexer *lex) {
    Expr *result = parse_expr_ternary(lex);
    result->site.end = (char *)lex->stream.data;

    return result;
}

internal_proc Stmt *
parse_stmt_defer(Lexer *lex) {
    Stmt *stmt = parse_stmt(lex);

    if ( stmt->kind == STMT_RETURN || stmt->kind == STMT_BREAK) {
        error(get_site(stmt), "diese anweisung kann nicht nachgeschoben werden!");
    }

    return stmt_defer(get_site(lex), stmt);
}

internal_proc Stmt *
parse_stmt_if(Lexer *lex) {
    Site site = get_site(lex);

    Expr *expr = parse_expr(lex);
    expect_token(lex, T_BLOCK_BEGIN);
    Stmt_Block block = parse_stmts(lex);
    expect_token(lex, T_BLOCK_END);

    Else_If *else_ifs = 0;
    Stmt_Block else_block = {};
    while ( match_keyword( lex, keyword_else ) ) {
        if ( match_keyword( lex, keyword_if ) ) {
            Expr *else_expr = parse_expr(lex);
            expect_token(lex, T_BLOCK_BEGIN);
            buf_push(else_ifs, stmt_elseif(get_site(lex), else_expr, parse_stmts(lex)));
            expect_token(lex, T_BLOCK_END);
        } else {
            expect_token(lex, T_BLOCK_BEGIN);
            else_block = parse_stmts(lex);
            expect_token(lex, T_BLOCK_END);
        }
    }

    return stmt_if(site, expr, block, else_ifs, buf_len(else_ifs), else_block);
}

internal_proc Stmt *
parse_stmt_for(Lexer *lex) {
    Site site = get_site(lex);

    Expr *expr = parse_expr(lex);
    Expr *init = 0;
    Expr *iterator = 0;
    Expr *condition = 0;

    if ( match_token(lex, T_EQL_ASSIGN) ) {
        iterator = expr;
        init = parse_expr(lex);
        expect_token(lex, T_TYPESPEC_SEPARATOR);
        condition = parse_expr(lex);
    } else {
        if ( match_token(lex, T_TYPESPEC_SEPARATOR) ) {
            iterator = expr;
            condition = parse_expr(lex);
        } else {
            condition = expr;
        }
    }

    expect_token(lex, T_BLOCK_BEGIN);
    Stmt_Block block = parse_stmts(lex);
    expect_token(lex, T_BLOCK_END);

    return stmt_for(site, iterator, init, condition, block);
}

internal_proc Switch_Case *
parse_switch_cases(Lexer *lex) {
    Switch_Case *result = 0;

    do {
        Expr **exprs = 0;
        b32 is_default = 0;

        Site site;
        if ( match_keyword(lex, keyword_case) ) {
            site = get_site(lex);
            buf_push(exprs, parse_expr(lex));
            while ( match_token(lex, T_PARAM_SEPARATOR) ) {
                buf_push(exprs, parse_expr(lex));
            }
        } else if ( match_keyword(lex, keyword_default ) ) {
            site = get_site(lex);
            is_default = 1;
        }

        expect_token(lex, T_TYPESPEC_SEPARATOR);

        Stmt_Block block = {};
        while ( !is_keyword(lex, keyword_case) && !is_keyword(lex, keyword_default) && !is_token(lex, T_BLOCK_END) ) {
            Stmt *stmt = parse_stmt(lex);

            if ( stmt->kind == STMT_BREAK ) {
                warning(get_site(stmt), "es sind keine 'break' anweisungen für switch fälle notwendig.");
            }

            buf_push(block.stmts, stmt);
        }
        block.num_stmts = buf_len(block.stmts);

        buf_push(result, switch_case(site, exprs, buf_len(exprs), is_default, block));
    } while ( !is_token(lex, T_BLOCK_END) );

    return result;
}

internal_proc Stmt *
parse_stmt_switch(Lexer *lex) {
    Site site = get_site(lex);
    Expr *expr = parse_expr(lex);

    expect_token(lex, T_BLOCK_BEGIN);
    Switch_Case *switch_cases = parse_switch_cases(lex);
    expect_token(lex, T_BLOCK_END);

    return stmt_switch(site, expr, switch_cases, buf_len(switch_cases));
}

internal_proc Stmt *
parse_stmt_while(Lexer *lex) {
    Site site = get_site(lex);
    Expr *condition = parse_expr(lex);

    expect_token(lex, T_BLOCK_BEGIN);
    Stmt_Block block = parse_stmts(lex);
    expect_token(lex, T_BLOCK_END);

    return stmt_while(site, condition, block);
}

internal_proc Stmt *
parse_stmt_return(Lexer *lex) {
    Expr **exprs = 0;
    Site site = get_site(lex);

    if ( !is_token(lex, T_STMT_END) ) {
        buf_push(exprs, parse_expr(lex));
        while ( match_token(lex, T_PARAM_SEPARATOR) ) {
            buf_push(exprs, parse_expr(lex));
        }
    }
    expect_token(lex, T_STMT_END);

    return stmt_return(site, exprs, buf_len(exprs));
}

internal_proc Stmt *
parse_stmt_expr(Lexer *lex) {
    Site site = get_site(lex);

    Expr *expr = parse_expr(lex);
    expect_token(lex, T_STMT_END);
    Stmt *result = stmt_expr(site, expr);

    return result;
}

internal_proc Stmt *
parse_stmt_break(Lexer *lex) {
    Stmt *result = stmt_break(get_site(lex));
    expect_token(lex, T_STMT_END);

    return result;
}

internal_proc Stmt *
parse_stmt_continue(Lexer *lex) {
    Stmt *result = stmt_continue(get_site(lex));
    expect_token(lex, T_STMT_END);

    return result;
}

internal_proc Stmt *
parse_stmt_using(Lexer *lex) {
    Stmt *result = stmt_using(get_site(lex), parse_expr(lex));
    expect_token(lex, T_STMT_END);

    return result;
}

internal_proc Stmt *
parse_stmt_free(Lexer *lex) {
    Stmt *result = stmt_free(get_site(lex), parse_expr(lex));
    expect_token(lex, T_STMT_END);

    return result;
}

internal_proc Stmt *
parse_stmt(Lexer *lex) {
    Stmt *result = 0;

    Note *notes = parse_notes(lex);
    size_t num_notes = buf_len(notes);
    Site site = get_site(lex);

    if ( is_keyword(lex) ) {
        first_if  ( match_keyword( lex, keyword_if ) ) {
            result = parse_stmt_if(lex);
        } else if ( match_keyword( lex, keyword_for ) ) {
            result = parse_stmt_for(lex);
        } else if ( match_keyword( lex, keyword_switch ) ) {
            result = parse_stmt_switch(lex);
        } else if ( match_keyword( lex, keyword_while ) ) {
            result = parse_stmt_while(lex);
        } else if ( match_keyword( lex, keyword_return ) ) {
            result = parse_stmt_return(lex);
        } else if ( match_keyword( lex, keyword_break ) ) {
            result = parse_stmt_break(lex);
        } else if ( match_keyword( lex, keyword_continue ) ) {
            result = parse_stmt_continue(lex);
        } else if ( match_keyword( lex, keyword_using ) ) {
            result = parse_stmt_using(lex);
        } else if ( match_keyword( lex, keyword_defer ) ) {
            result = parse_stmt_defer(lex);
        } else if ( match_keyword(lex, keyword_free) ) {
            result = parse_stmt_free(lex);
        } else {
            assert(0);
        }
    } else if ( match_token(lex, T_BLOCK_BEGIN) ) {
        Stmt_Block stmts = parse_stmts(lex);
        expect_token(lex, T_BLOCK_END);
        result = stmt_block(site, stmts);
    } else {
        Expr *expr = parse_expr(lex);

        if ( expr->kind == EXPR_CALL ) {
            result = stmt_expr(site, expr);
            expect_token(lex, T_DECL_END);
        } else if ( is_assign(lex->token) ) {
            Token op = eat_token(lex);
            result = stmt_assign(site, op.kind, expr, parse_expr(lex));
            expect_token(lex, T_DECL_END);
        } else if ( is_token(lex, T_TYPESPEC_SEPARATOR) ) {
            assert(expr->kind == EXPR_NAME);
            result = stmt_decl(site, parse_decl_tail(lex, expr->expr_name.value));
        } else if ( expr->kind == EXPR_FIELD ) {
            result = stmt_expr(site, expr);
            expect_token(lex, T_DECL_END);
        } else {
            assert(0);
        }
    }

    assert(result);
    result->notes = notes;
    result->num_notes = num_notes;
    result->site.end = (char *)lex->stream.data;

    return result;
}

internal_proc Typespec *
parse_intrinsic_typespec(Lexer *lex) {
    Site site = get_site(lex);

    first_if  ( match_datatype(lex, datatype_u8)  ) {
        return typespec_name(site, datatype_u8);
    } else if ( match_datatype(lex, datatype_u16) ) {
        return typespec_name(site, datatype_u16);
    } else if ( match_datatype(lex, datatype_u32) ) {
        return typespec_name(site, datatype_u32);
    } else if ( match_datatype(lex, datatype_u64) ) {
        return typespec_name(site, datatype_u64);
    } else if ( match_datatype(lex, datatype_s8)  ) {
        return typespec_name(site, datatype_s8);
    } else if ( match_datatype(lex, datatype_s16) ) {
        return typespec_name(site, datatype_s16);
    } else if ( match_datatype(lex, datatype_s32) ) {
        return typespec_name(site, datatype_s32);
    } else if ( match_datatype(lex, datatype_s64) ) {
        return typespec_name(site, datatype_s64);
    } else if ( match_datatype(lex, datatype_f32) ) {
        return typespec_name(site, datatype_f32);
    } else if ( match_datatype(lex, datatype_f64) ) {
        return typespec_name(site, datatype_f64);
    } else if ( match_datatype(lex, datatype_bool)) {
        return typespec_name(site, datatype_bool);
    } else if ( match_token(lex, T_ELLIPSIS) ) {
        return typespec_variadic(site);
    } else if ( is_token(lex, T_IDENT) ) {
        Expr *expr = parse_expr(lex);

        Typespec *result = NULL;
        if ( expr->kind == EXPR_NAME ) {
            result = typespec_name(site, expr->expr_name.value);
        } else {
            assert(expr->kind == EXPR_FIELD);
            result = typespec_scope(site, expr);
        }

        return result;
    } else if ( match_token(lex, T_BLOCK_BEGIN) ) {
        Decl **decls = 0;
        while ( !match_token(lex, T_BLOCK_END) ) {
            buf_push(decls, parse_decl(lex));
        }

        return typespec_block(site, decls, buf_len(decls));
    } else {
        assert(0);
    }

    return 0;
}

internal_proc Typespec *
parse_ptr_or_array_typespec(Lexer *lex) {
    Site site = get_site(lex);

    if ( match_token(lex, T_POINTER) ) {
        return typespec_ptr(site, parse_typespec(lex));
    } else if ( match_token(lex, T_INDEX_OPEN) ) {
        Expr *expr = 0;
        if ( !is_token(lex, T_INDEX_CLOSE) ) {
            expr = parse_expr(lex);
        }
        expect_token(lex, T_INDEX_CLOSE);

        return typespec_array(site, parse_typespec(lex), expr);
    }

    return parse_intrinsic_typespec(lex);
}

internal_proc Typespec *
parse_variable_typespec(Lexer *lex) {
    Typespec *typespec = parse_ptr_or_array_typespec(lex);

    return typespec;
}

internal_proc Typespec *
parse_typespec_proc_parameter(Lexer *lex) {
    Site site = get_site(lex);
    Token name = expect_token(lex, T_IDENT);

    if ( match_token(lex, T_TYPESPEC_SEPARATOR) ) {
        name = expect_token(lex, T_IDENT);
    }

    return typespec_name(site, name.ident);
}

internal_proc Typespec *
parse_procedure_typespec(Lexer *lex) {
    Site site = get_site(lex);
    Typespec **params = 0;

    expect_token(lex, T_CALL_OPEN);
    if ( !is_token(lex, T_CALL_CLOSE) ) {
        buf_push(params, parse_typespec_proc_parameter(lex));
        while ( match_token(lex, T_PARAM_SEPARATOR) ) {
            buf_push(params, parse_typespec_proc_parameter(lex));
        }
    }
    expect_token(lex, T_CALL_CLOSE);

    Typespec **rets = 0;;
    if ( match_token(lex, T_ARROW) ) {
        buf_push(rets, parse_typespec_proc_parameter(lex));
        while ( match_token(lex, T_PARAM_SEPARATOR) ) {
            buf_push(rets, parse_typespec_proc_parameter(lex));
        }
    }

    return typespec_proc(site, params, buf_len(params), rets, buf_len(rets));
}

internal_proc Typespec *
parse_typespec(Lexer *lex) {
    if ( !is_token(lex, T_CALL_OPEN) ) {
        return parse_variable_typespec(lex);
    } else {
        return parse_procedure_typespec(lex);
    }
}

internal_proc Stmt_Block
parse_stmts(Lexer *lex) {
    Stmt_Block result = {};

    while ( !is_token(lex, T_BLOCK_END) ) {
        buf_push(result.stmts, parse_stmt(lex));
    }

    result.num_stmts = buf_len(result.stmts);

    return result;
}

internal_proc Proc_Param
parse_decl_parameter(Lexer *lex) {
    uint32_t flags = 0;

    if ( match_keyword( lex, keyword_using ) ) {
        flags |= PF_USING;
    }

    Token name = expect_token(lex, T_IDENT);
    expect_token(lex, T_TYPESPEC_SEPARATOR);

    Typespec *typespec = parse_typespec(lex);

    Expr *default_value = 0;
    if ( match_token( lex, T_EQL_ASSIGN ) ) {
        default_value = parse_expr(lex);
    }

    return proc_param(name.ident, typespec, default_value, flags);
}

internal_proc Proc_Param
parse_decl_ret_parameter(Lexer *lex) {
    Site site = get_site(lex);

    char *name = "";
    Typespec *typespec = 0;
    Expr *default_value = 0;
    Expr *expr = NULL;

    if ( is_token(lex, T_IDENT) ) {
        expr = parse_expr(lex);
    } else {
        typespec = parse_typespec(lex);
    }

    if ( match_token( lex, T_TYPESPEC_SEPARATOR ) ) {
        assert(expr->kind == EXPR_NAME);
        name = expr->expr_name.value;
        typespec = parse_typespec(lex);

        if ( match_token( lex, T_EQL_ASSIGN ) ) {
            default_value = parse_expr(lex);
        }
    } else if ( match_token( lex, T_EQL_ASSIGN ) ) {
        assert(expr->kind == EXPR_NAME);
        typespec = typespec_name(site, expr->expr_name.value);
        default_value = parse_expr(lex);
    } else if ( !typespec ) {
        assert(expr->kind == EXPR_NAME);
        typespec = typespec_name(site, expr->expr_name.value);
    }

    assert(typespec);
    return proc_param(name, typespec, default_value);
}

internal_proc Aggregate_Field
parse_aggregate_enum_field(Lexer *lex, char *name) {
    Site site = get_site(lex);

    /*
       @INFO: an dieser stelle kann leider keine parse_decl verwendet werden,
              da ein enum feld auch nur aus einem IDENT, gefolgt von semikolon,
              bestehen kann. - 08.02.2019
     */
    Expr *expr = 0;
    if ( match_token(lex, T_TYPESPEC_SEPARATOR) ) {
        expect_token(lex, T_TYPESPEC_SEPARATOR);
        expr = parse_expr(lex);

        assert(expr->kind == EXPR_INT);
    }

    return aggregate_item(site, name, typespec_name(zero_site, intern_str("s64")), expr, 0);
}

internal_proc Typespec *
parse_possible_typespec(Lexer *lex) {
    Typespec *result = parse_typespec(lex);

    return result;
}

internal_proc Decl *
parse_decl_tail(Lexer *lex, char *name) {
    Site site = get_site(lex);

    expect_token(lex, T_TYPESPEC_SEPARATOR);

    Typespec *typespec = 0;
    if ( !is_token(lex, T_EQL_ASSIGN) && !is_token(lex, T_TYPESPEC_SEPARATOR) ) {
        typespec = parse_typespec(lex);
    }

    if ( match_token(lex, T_DECL_END) ) {
        return decl_var(site, name, typespec);
    }

    if ( match_token(lex, T_EQL_ASSIGN) ) {
        Decl *decl = decl_var(site, name, typespec, parse_expr(lex));
        expect_token(lex, T_DECL_END);
        return decl;
    }

    expect_token(lex, T_TYPESPEC_SEPARATOR);

    if ( lex->token.flags & TF_KEYWORD ) {
        if ( match_keyword(lex, keyword_struct) ) {
            expect_token(lex, T_BLOCK_BEGIN);

            Aggregate_Field *fields = 0;
            while ( !match_token(lex, T_BLOCK_END) ) {
                Site field_site = get_site(lex);

                int32_t flags = 0;
                if ( match_keyword(lex, keyword_using) ) {
                    flags |= PF_USING;
                }

                char **field_names = 0;
                buf_push(field_names, parse_name(lex));
                while ( match_token(lex, T_PARAM_SEPARATOR) ) {
                    buf_push(field_names, parse_name(lex));
                }
                expect_token(lex, T_TYPESPEC_SEPARATOR);

                Typespec *field_typespec = 0;
                if ( !is_token(lex, T_EQL_ASSIGN) && !is_token(lex, T_TYPESPEC_SEPARATOR) ) {
                    field_typespec = parse_typespec(lex);
                }

                Expr *field_expr = 0;
                if ( !is_token(lex, T_DECL_END) ) {
                    field_expr = parse_expr(lex);
                }

                expect_token(lex, T_DECL_END);

                for ( int i = 0; i < buf_len(field_names); ++i ) {
                    buf_push(fields, aggregate_item(field_site, field_names[i], field_typespec, field_expr, flags));
                }
            }

            return decl_struct(site, name, fields, buf_len(fields));
        } else if ( match_keyword(lex, keyword_union) ) {
            expect_token(lex, T_BLOCK_BEGIN);

            Aggregate_Field *fields = 0;
            while ( !match_token(lex, T_BLOCK_END) ) {
                char *field_name = parse_name(lex);
                Decl *field_decl = parse_decl_tail(lex, field_name);
                buf_push(fields, aggregate_item(get_site(field_decl), field_decl->name, field_decl->decl_var.typespec, field_decl->decl_var.init, PF_NONE));
            }

            return decl_union(site, name, fields, buf_len(fields));
        } else if ( match_keyword(lex, keyword_enum) ) {
            Typespec *typespec_enum = NULL;
            if ( match_token(lex, T_EQL_ASSIGN) ) {
                typespec_enum = parse_typespec(lex);
            }

            expect_token(lex, T_BLOCK_BEGIN);

            Aggregate_Field *fields = 0;
            int64_t default_value = 0;
            while ( !match_token(lex, T_BLOCK_END) ) {
                char *field_name = parse_name(lex);

                Expr *expr = 0;
                if ( match_token(lex, T_TYPESPEC_SEPARATOR) ) {
                    expect_token(lex, T_TYPESPEC_SEPARATOR);
                    expr = parse_expr(lex);
                }

                buf_push(fields, aggregate_item(site, field_name, typespec_name(zero_site, intern_str("s64")), expr, PF_NONE));
                expect_token(lex, T_DECL_END);
            }

            return decl_enum(site, name, fields, buf_len(fields), typespec_enum);
        } else if ( match_keyword(lex, keyword_sizeof) ) {
            expect_token(lex, T_CALL_OPEN);
            Typespec *sizeof_typespec = parse_typespec(lex);
            expect_token(lex, T_CALL_CLOSE);
            Decl *decl = decl_const(site, name, typespec, expr_sizeof(site, sizeof_typespec));
            expect_token(lex, T_DECL_END);

            return decl;
        } else if ( match_keyword(lex, keyword_typeof) ) {
            expect_token(lex, T_CALL_OPEN);
            Typespec *typeof_typespec = parse_typespec(lex);
            expect_token(lex, T_CALL_CLOSE);
            Decl *decl = decl_const(site, name, typespec, expr_typeof(site, typeof_typespec));
            expect_token(lex, T_DECL_END);

            return decl;
        } else if ( match_keyword(lex, keyword_alignof) ) {
            expect_token(lex, T_CALL_OPEN);
            Typespec *alignof_typespec = parse_typespec(lex);
            expect_token(lex, T_CALL_CLOSE);
            Decl *decl = decl_const(site, name, typespec, expr_alignof(site, alignof_typespec));
            expect_token(lex, T_DECL_END);

            return decl;
        } else if ( match_keyword(lex, keyword_offsetof) ) {
            Decl *decl = decl_const(site, name, typespec, parse_expr_offsetof(site, lex));
            expect_token(lex, T_DECL_END);

            return decl;
        } else if ( match_keyword(lex, keyword_typedef) ) {
            Typespec *typedef_typespec = parse_typespec(lex);
            expect_token(lex, T_DECL_END);

            return decl_typedef(site, name, typedef_typespec);
        } else if ( match_keyword(lex, keyword_proc) ) {
            Proc_Param *params = 0;
            if ( match_token(lex, T_CALL_OPEN) ) {
                if ( !is_token(lex, T_CALL_CLOSE) ) {
                    buf_push(params, parse_decl_parameter(lex));
                    while ( match_token(lex, T_PARAM_SEPARATOR) ) {
                        buf_push(params, parse_decl_parameter(lex));
                    }
                }
                expect_token(lex, T_CALL_CLOSE);
            }

            Proc_Param *rets = 0;
            if ( match_token(lex, T_ARROW) ) {
                buf_push(rets, parse_decl_ret_parameter(lex));
                while ( match_token(lex, T_PARAM_SEPARATOR) ) {
                    buf_push(rets, parse_decl_ret_parameter(lex));
                }
            }

            b32 is_incomplete = false;
            Stmt_Block block = {};
            if ( match_token(lex, T_BLOCK_BEGIN) ) {
                block = parse_stmts(lex);
                expect_token(lex, T_BLOCK_END);
            } else {
                expect_token(lex, T_DECL_END);
                is_incomplete = true;
            }

            return decl_proc(site, name, params, buf_len(params), rets,
                    buf_len(rets), block, is_incomplete);
        } else {
            assert(0);
        }
    } else  {
        Decl *decl = decl_const(site, name, typespec, parse_expr(lex));
        expect_token(lex, T_DECL_END);

        return decl;
    }

    return 0;
}

char **loaded_files;
internal_proc b32
file_already_loaded(char *filename) {
    for ( int i = 0; i < buf_len(loaded_files); ++i ) {
        if ( loaded_files[i] == filename ) {
            return true;
        }
    }

    return false;
}

internal_proc Decl *
parse_compiler_cmd(Lexer *lex) {
    Site site = get_site(lex);

    if ( match_command(lex, command_load) ) {
        char *filename = parse_str(lex);

        if ( !file_already_loaded(filename) ) {
            buf_push(loaded_files, filename);
            add_lexer(init_compiler(filename));
        }
    } else if ( match_command(lex, command_import) ) {
        Expr *alias = NULL;
        Expr *expr = parse_expr(lex);

        if ( match_token(lex, T_EQL_ASSIGN) ) {
            alias = expr;
            assert(alias->kind == EXPR_NAME);
            expr = parse_expr(lex);
        }

        if ( expr->kind != EXPR_NAME && expr->kind != EXPR_FIELD ) {
            error(get_site(expr), "import anweisung braucht validen operanden als parameter!");
        }

        Module_Desc desc = module_descriptor(expr, alias);
        assert(os_env("SSS_PATH"));
        buf_push(modules, module_load(os_env("SSS_PATH"), desc));
    } else if ( match_command(lex, command_foreign) ) {
        char *foreign_name = NULL;
        if ( match_token(lex, T_CALL_OPEN) ) {
            Expr *expr = parse_expr(lex);
            if (expr) { foreign_name = expr->expr_name.value; }
            expect_token(lex, T_CALL_CLOSE);
        }

        return decl_foreign(site, foreign_name, parse_decl(lex));
    } else {
        assert(0);
    }

    return 0;
}

internal_proc Decl *
parse_decl(Lexer *lex) {
    Note *notes = parse_notes(lex);
    size_t num_notes = buf_len(notes);

    Decl *result = 0;
    Expr **exprs = 0;

    char *name = parse_name(lex);
    result = parse_decl_tail(lex, name);
    result->notes = notes;
    result->num_notes = num_notes;
    result->site.end = (char *)lex->stream.data;

    return result;
}

internal_proc Decl **
parse_program() {
    Decl **decls = 0;

    for ( int i = 0; i < parser.num_lexers; ++i ) {
        Lexer *lex = &parser.lexers[i];
        parser.current_lexer = lex;

        while ( !is_token(lex, T_EOF) ) {
            if ( match_token(lex, T_COMPILER_CMD) ) {
                Decl *decl = parse_compiler_cmd(lex);
                if ( decl ) {
                    buf_push(decls, decl);
                }
            } else {
                buf_push(decls, parse_decl(lex));
            }
        }
    }

    return decls;
}

internal_proc Lexer
init_compiler(char *filename) {
    Lexer lex = {};
    String code = {};
    char path[MAX_PATH];
    path_canonical(path, filename);

    if ( file_read(path, &code) ) {
        init_stream(code, &lex, path);
    } else {
        error(zero_site, "konnte datei %s nicht lesen.", filename);
    }

    return lex;
}

