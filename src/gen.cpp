#define genf(...)   gen_result = strf("%s%s", gen_result, strf(__VA_ARGS__))
#define genlnf(...) gen_result = strf("%s\n", gen_result); gen_indentation(); genf(__VA_ARGS__); ++gen_row
#define genln()     gen_result = strf("%s\n", gen_result); gen_indentation(); ++gen_row

char *gen_result = "";
int gen_indent = 0;
size_t gen_row = 1;
char *gen_filename;

internal_proc void gen_headers();
internal_proc void gen_expr(Expr *expr);
internal_proc void gen_stmt_block(Stmt_Block block);
internal_proc void gen_decl(Decl *decl);
internal_proc void gen_cdecl_var(Decl *decl);
internal_proc void gen_cdecl_const(Decl *decl);

internal_proc void
gen_indentation() {
    gen_result = strf("%s%*.s", gen_result, 4 * gen_indent, "         ");
}

internal_proc void
gen_str(char *str) {
    genf("\"");
    for ( char * it = str; it != str + strlen(str); ++it ) {
        if ( *it == '\\' ) {
            genf("\\");
        }
        genf("%c", *it);
    }
    genf("\"");
}

internal_proc void
gen_cline(Site site) {
#if 1
    gen_row = site.row;

    genlnf("#line %d", gen_row);

    if ( gen_filename != site.name ) {
        gen_filename = site.name;
        genf(" ");
        gen_str(site.name);
    }
#endif
}

internal_proc char *
to_binary_op(Token_Kind op) {
    switch ( op ) {
        case T_MUL: {
            return "*";
        } break;

        case T_PLUS: {
            return "+";
        } break;

        case T_MINUS: {
            return "-";
        } break;

        case T_DIV: {
            return "/";
        } break;

        case T_EQ: {
            return "==";
        } break;

        case T_NEQ: {
            return "!=";
        } break;

        case T_GT: {
            return ">";
        } break;

        case T_GEQ: {
            return ">=";
        } break;

        case T_LT: {
            return "<";
        } break;

        case T_LEQ: {
            return "<=";
        } break;

        case T_AND: {
            return "&&";
        } break;

        case T_OR: {
            return "||";
        } break;

        case T_LSHIFT: {
            return "<<";
        } break;

        case T_RSHIFT: {
            return ">>";
        } break;

        case T_BIT_AND: {
            return "&";
        } break;

        case T_BIT_OR: {
            return "|";
        } break;

        case T_XOR: {
            return "^";
        } break;

        default: {
            assert(!"nicht unterstützter operator!");
            return "";
        } break;
    }
}

internal_proc char *
to_unary_op(Token_Kind op) {
    switch ( op ) {
        case T_DEREF: {
            return "*";
        } break;

        case T_POINTER: {
            return "&";
        } break;

        case T_PLUS: {
            return "+";
        } break;

        case T_MINUS: {
            return "-";
        } break;

        case T_DIV: {
            return "/";
        } break;

        case T_NOT: {
            return "!";
        } break;

        case T_NEGATE: {
            return "~";
        } break;

        default: {
            assert(!"nicht unterstützter operator!");
            return "";
        } break;
    }
}

internal_proc char *
cdecl_paren(char *str, char b) {
    return (b && b != '[') ? strf("(%s)", str) : str;
}

internal_proc char *
sym_name(Sym *sym) {
    // return sym->foreign_name ? sym->foreign_name : sym->name;
    return sym->name;
}

internal_proc char *
cdecl_name(Type *type) {
    switch ( type->kind ) {
        case TYPE_VOID:   { return "void";   }
        case TYPE_CHAR:   { return "char";   }
        case TYPE_U8:     { return "u8";     }
        case TYPE_U16:    { return "u16";    }
        case TYPE_U32:    { return "u32";    }
        case TYPE_U64:    { return "u64";    }
        case TYPE_S8:     { return "s8";     }
        case TYPE_S16:    { return "s16";    }
        case TYPE_S32:    { return "s32";    }
        case TYPE_S64:    { return "s64";    }
        case TYPE_F32:    { return "float";  }
        case TYPE_F64:    { return "double"; }
        case TYPE_ENUM:
        case TYPE_UNION:
        case TYPE_STRUCT: { return sym_name(type->sym); }
        default:          { assert(!"nicht unterstützter typ!"); return NULL; }
    }
}

internal_proc char *
cdecl_type(Type *type, char *str) {
    switch (type->kind) {
        case TYPE_F32:
        case TYPE_F64:
        case TYPE_CHAR:
        case TYPE_U8:
        case TYPE_U16:
        case TYPE_U32:
        case TYPE_U64:
        case TYPE_S8:
        case TYPE_S16:
        case TYPE_S32:
        case TYPE_S64:
        case TYPE_VOID:
        case TYPE_STRUCT:
        case TYPE_UNION:
        case TYPE_ENUM: {
            char *result = strf("%s%s%s", cdecl_name(type), *str ? " " : "", str);
            return result;
        } break;

        case TYPE_PTR: {
            return cdecl_type(type->type_ptr.base, cdecl_paren(strf("*%s", str), *str));
        } break;

        case TYPE_ARRAY: {
            if (type->type_array.index == 0) {
                return cdecl_type(type->type_array.base, cdecl_paren(strf("%s[]", str), *str));
            } else {
                return cdecl_type(type->type_array.base, cdecl_paren(strf("%s[%llu]", str, type->type_array.index), *str));
            }
        } break;

        case TYPE_PROC: {
            char *result = strf("%s(", cdecl_paren(strf("*%s", str), *str));

            if ( type->type_proc.num_params ) {
                for ( int i = 0; i < type->type_proc.num_params; ++i ) {
                    result = strf("%s%s%s", result, (i == 0 ? "" : ", "), cdecl_type(type->type_proc.params[i], ""));
                }
            } else {
                result = strf("%s%s", result, "void");
            }

            for ( int i = 1; i < type->type_proc.num_rets; ++i ) {
                result = strf("%s, %s%s", result, cdecl_type(type_ptr(type->type_proc.rets[i]), ""));
            }

            result = strf("%s)", result);
            return cdecl_type(type->type_proc.rets[0], result);
        } break;

        case TYPE_VARIADIC: {
            return "...";
        } break;

        default: {
            assert(0);
            return "";
        } break;
    }
}

internal_proc char *
gen_expr_str(Expr *expr) {
    char *temp = gen_result;
    gen_result = "";
    gen_expr(expr);
    char *result = gen_result;
    gen_result = temp;

    return result;
}

internal_proc void
gen_headers() {
    genlnf("#include <windows.h>");
    genlnf("#include <stdbool.h>");
    genlnf("#include <stdint.h>");
    genlnf("#include <stddef.h>");
    genlnf("#include <stdio.h>");
    genlnf("#include <stdlib.h>");
    genlnf("#include <malloc.h>");
    genlnf("#include <ctype.h>");
    genlnf("#include <assert.h>");
}

internal_proc char *
gen_val(Val value) {
    switch ( value.kind ) {
        case VAL_U8: {
            return strf("%d", value.u8);
        } break;

        case VAL_U16: {
            return strf("%d", value.u16);
        } break;

        case VAL_U32: {
            return strf("%d", value.u32);
        } break;

        case VAL_U64: {
            return strf("%lld", value.u64);
        } break;

        case VAL_S8: {
            return strf("%d", value.s8);
        } break;

        case VAL_S16: {
            return strf("%d", value.s16);
        } break;

        case VAL_S32: {
            return strf("%d", value.s32);
        } break;

        case VAL_S64: {
            return strf("%lld", value.s64);
        } break;

        case VAL_F32: {
            return strf("%f", value.f32);
        } break;

        case VAL_F64: {
            return strf("%f", value.f64);
        } break;

        case VAL_STR: {
            return strf("%s", value.s);
        } break;

        case VAL_NONE: {
            return "0";
        } break;

        default: {
            assert(0);
            return NULL;
        } break;
    }
}

internal_proc char *
cdecl_typespec(Typespec *typespec, char *str) {
    if ( !typespec ) {
        return strf("void %s", str);
    }

    switch ( typespec->kind ) {
        case TYPESPEC_NAME: {
            return strf("%s%s%s", typespec->typespec_name.value, *str ? " " : "", str);
        } break;

        case TYPESPEC_PTR: {
            return cdecl_typespec(typespec->typespec_ptr.elem, cdecl_paren(strf("*%s", str), *str));
        } break;

        case TYPESPEC_ARRAY: {
            char *index = ( typespec->typespec_array.size ) ? gen_expr_str(typespec->typespec_array.size) : "";
            return cdecl_typespec(typespec->typespec_array.elem, cdecl_paren(strf("%s[%s]", str, index), *str));
        } break;

        case TYPESPEC_PROC: {
            char *result = strf("%s(", cdecl_paren(strf("*%s", str), *str));

            if ( typespec->typespec_proc.num_params ) {
                for ( int i = 0; i < typespec->typespec_proc.num_params; ++i ) {
                    result = strf("%s%s%s", result, (i == 0 ? "" : ", "), cdecl_typespec(typespec->typespec_proc.params[i], ""));
                }
            } else {
                result = strf("%s%s", result, "void");
            }

            /* @TODO: restliche return argumente als übergabeparameter anhängen */
            result = strf("%s)", result);

            Typespec *rets = typespec->typespec_proc.rets ? typespec->typespec_proc.rets[0] : 0;
            return cdecl_typespec(rets, result);
        } break;

        case TYPESPEC_BLOCK: {
            char *result = strf("struct { ");
            for ( int i = 0; i < typespec->typespec_block.num_decls; ++i ) {
                Decl *decl = typespec->typespec_block.decls[i];
                result = strf("%s%s; ", result, cdecl_typespec(decl->decl_var.typespec, decl->name));
            }

            return strf("%s} %s", result, str);
        } break;

        case TYPESPEC_VARIADIC: {
            return "...";
        } break;

        case TYPESPEC_SCOPE: {
            Expr *expr = typespec->typespec_scope.expr->expr_field.field;
            return strf("%s %s", gen_expr_str(expr), str);
        } break;

        default: {
            assert(!"nicht unterstützter typespec!");
            return NULL;
        } break;
    }
}

internal_proc void
gen_cdecl_proc_header(Decl *decl) {
    char *proc_name = strf("%s(", decl->name);

    if ( decl->decl_proc.num_rets ) {
        genlnf("%s %s", cdecl_typespec(decl->decl_proc.rets[0].typespec, ""), proc_name);
    } else {
        genlnf("void %s", proc_name);
    }

    if ( decl->decl_proc.num_params == 0 && decl->decl_proc.num_rets < 2 ) {
        genf("void");
    } else {
        for ( int i = 0; i < decl->decl_proc.num_params; ++i ) {
            Proc_Param param = decl->decl_proc.params[i];
            if ( i != 0 ) {
                genf(", ");
            }
            genf("%s", cdecl_typespec(param.typespec, param.name));
        }

        for ( int i = 1; i < decl->decl_proc.num_rets; ++i ) {
            if ( i != 0 ) {
                genf(", ");
            }

            genf("%s", cdecl_typespec(typespec_ptr(zero_site, decl->decl_proc.rets[i].typespec), strf("ret_____%d", i)));
        }
    }
    genf(")");
}

internal_proc Expr *
last_field(Expr *expr) {
    switch (expr->kind) {
        case EXPR_NAME: {
            return expr;
        } break;

        case EXPR_FIELD: {
            return last_field(expr->expr_field.field);
        } break;

        default: {
            return expr;
        } break;
    }
}

internal_proc void
gen_stmt(Stmt *stmt) {
    gen_cline(get_site(stmt));

    switch ( stmt->kind ) {
        case STMT_IF: {
            genlnf("if (");
            gen_expr(stmt->stmt_if.if_expr);
            genf(") ");
            gen_stmt_block(stmt->stmt_if.then_block);

            for ( int i = 0; i < stmt->stmt_if.num_else_ifs; ++i ) {
                genlnf("else if(");
                gen_expr(stmt->stmt_if.else_ifs[i].condition);
                genf(") ");
                gen_stmt_block(stmt->stmt_if.else_ifs[i].block);
            }

            if ( stmt->stmt_if.else_block.num_stmts ) {
                genf(" else ");
                gen_stmt_block(stmt->stmt_if.else_block);
            }
        } break;

        case STMT_FOR: {
            Expr *cond_expr = stmt->stmt_for.condition;
            Expr *it_expr   = stmt->stmt_for.iterator;
            Expr *init_expr = stmt->stmt_for.init;

            if ( cond_expr->kind == EXPR_BINARY ) {
                genlnf("for (%s = ", cdecl_type(type_int, it_expr->expr_name.value));
                b32 increment_it = true;

                if ( cond_expr->expr_binary.op == T_RANGE ) {
                    Expr *left  = cond_expr->expr_binary.left;
                    Expr *right = cond_expr->expr_binary.right;

                    if ( init_expr ) {
                        gen_expr(init_expr);
                        genf("; ");
                        gen_expr(left);
                        genf(" <= ");
                        /*
                        fall 1: for it=5; it..10 -> for ( int it = 5; it <= 10; ++it )
                        fall 2: for it=10; 5..it -> for ( int it = 10; 5 <= it; --it )

                        alles gleich, bis auf inkrement/dekrement des iterators
                        */
                        if ( left->kind != EXPR_NAME || left->expr_name.value != it_expr->expr_name.value ) {
                            increment_it = false;
                        }
                        gen_expr(right);
                    } else {
                        gen_expr(cond_expr->expr_binary.left);
                        genf("; %s <= ", it_expr->expr_name.value);
                        gen_expr(cond_expr->expr_binary.right);
                    }
                } else {
                    assert(is_cmp(cond_expr->expr_binary.op));

                    if ( init_expr ) {
                        gen_expr(init_expr);
                        genf("; ");
                        gen_expr(cond_expr);
                    } else {
                        gen_expr(cond_expr->expr_binary.left);
                        genf("; %s %s ", it_expr->expr_name.value, to_string(cond_expr->expr_binary.op));
                        gen_expr(cond_expr->expr_binary.right);
                    }
                }

                if ( increment_it ) {
                    genf("; ++%s) ", it_expr->expr_name.value);
                } else {
                    genf("; --%s) ", it_expr->expr_name.value);
                }
            } else if ( cond_expr->kind == EXPR_FIELD ) {
                Expr *iterator = last_field(cond_expr);

                char *it = it_expr->expr_name.value;
                genlnf("for (int %s = 0; %s < %d; ++%s) ", it, it, it_type->type_array.index, it);
            } else if ( cond_expr->kind == EXPR_NAME ) {
                genlnf("for (char ");
                gen_expr(it_expr);
                genf(" = *");
                gen_expr(cond_expr);
                genf("; ");
                gen_expr(it_expr);
                genf("; ");
                gen_expr(it_expr);
                genf(" = *++");
                gen_expr(cond_expr);
                genf(") ");
            } else {
                assert(!"unbekannter ausdruck in der for anweisung!");
            }
            gen_stmt_block(stmt->stmt_for.block);
        } break;

        case STMT_SWITCH: {
            genlnf("switch (");
            gen_expr(stmt->stmt_switch.expr);
            genf(") {");

            for ( int i = 0; i < stmt->stmt_switch.num_switch_cases; ++i ) {
                Switch_Case switch_case = stmt->stmt_switch.switch_cases[i];

                gen_indent++;
                if ( switch_case.is_default ) {
                    genlnf("default: ");
                    gen_stmt_block(switch_case.block);
                } else {
                    for ( int j = 0; j < switch_case.num_exprs; ++j ) {
                        Expr *expr = switch_case.exprs[j];
                        gen_cline(get_site(expr));

                        genlnf("case ");
                        if ( expr->kind == EXPR_BINARY && expr->expr_binary.op == T_RANGE ) {
                            Expr *left  = expr->expr_binary.left;
                            Expr *right = expr->expr_binary.right;

                            uint64_t k = 0;
                            if ( left->kind == EXPR_FIELD ) {
                                Operand *operand = fetch_resolved_operand(left->expr_field.field);
                                k = operand->value.s64;
                            } else if ( left->kind == EXPR_INT ) {
                                k = left->expr_int.value;
                            } else if ( left->kind == EXPR_CHAR ) {
                                k = (int)left->expr_char.value;
                            } else {
                                assert(0);
                            }

                            uint64_t end = 0;
                            if ( right->kind == EXPR_FIELD ) {
                                Operand *operand = fetch_resolved_operand(right->expr_field.field);
                                end = operand->value.s64;
                            } else if ( right->kind == EXPR_INT ) {
                                end = right->expr_int.value;
                            } else if ( right->kind == EXPR_CHAR ) {
                                end = (int)right->expr_char.value;
                            } else {
                                assert(0);
                            }

                            for ( ; k <= end; ++k ) {
                                if ( left->kind == EXPR_INT || left->kind == EXPR_FIELD ) {
                                    genf("%d", k);
                                } else {
                                    genf("'%c'", k);
                                }

                                if ( k < end ) {
                                    genf(":");
                                    genlnf("case ");
                                }
                            }
                        } else {
                            gen_expr(expr);
                        }

                        genf((j < switch_case.num_exprs-1) ? ":" : ": ");
                    }
                    gen_stmt_block(switch_case.block);
                }
                genf(" break;");
                gen_indent--;
            }

            genlnf("}");
        } break;

        case STMT_WHILE: {
            genlnf("while (");
            gen_expr(stmt->stmt_while.expr);
            genf(") ");
            gen_stmt_block(stmt->stmt_while.block);
        } break;

        case STMT_RETURN: {
            if ( stmt->stmt_return.num_exprs ) {
                for ( int i = 1; i < stmt->stmt_return.num_exprs; ++i ) {
                    genlnf("(*ret_____%d) = ", i);
                    gen_expr(stmt->stmt_return.exprs[i]);
                    genf(";");
                }
            }

            genlnf("return");
            if ( stmt->stmt_return.num_exprs > 0 ) {
                genf(" ");
                gen_expr(stmt->stmt_return.exprs[0]);
            }
            genf(";");
        } break;

        case STMT_EXPR: {
            genln();
            gen_expr(stmt->stmt_expr.expr);
            genf(";");
        } break;

        case STMT_ASSIGN: {
            genln();
            gen_expr(stmt->stmt_assign.left);
            genf(" %s ", to_string(stmt->stmt_assign.op));
            gen_expr(stmt->stmt_assign.right);
            genf(";");
        } break;

        case STMT_BREAK: {
            genlnf("break;");
        } break;

        case STMT_CONTINUE: {
            genlnf("continue;");
        } break;

        case STMT_USING: {
            Expr *expr = stmt->stmt_using.expr;
            Type *type = fetch_resolved_type(expr);
            assert(is_aggregate(type));

            for ( int i = 0; i < type->type_aggr.num_fields; ++i ) {
                Type_Field field = type->type_aggr.fields[i];
                genlnf("const %s = %s;", cdecl_type(field.type, field.name), gen_val(field.value));
            }
        } break;

        case STMT_BLOCK: {
            genln();
            gen_stmt_block(stmt->stmt_block.block);
        } break;

        case STMT_DECL: {
            Decl *decl = stmt->stmt_decl.decl;

            switch ( decl->kind ) {
                case DECL_VAR: {
                    gen_cdecl_var(decl);
                } break;

                case DECL_CONST: {
                    gen_cdecl_const(decl);
                } break;

                default: {
                    error(get_site(decl), "andere deklarationstypen werden derzeit nicht unterstützt!");
                } break;
            };
        } break;

        case STMT_DEFER: {
            gen_stmt(stmt->stmt_defer.stmt);
        } break;

        case STMT_FREE: {
            genlnf("free(");
            gen_expr(stmt->stmt_free.expr);
            genf(");");
        } break;

        default: {
            assert(!"nicht unterstütztes statement!");
        } break;
    }
}

enum { MAX_DEFERRED_STMTS = 1024 };
Stmt deferred_stmts[MAX_DEFERRED_STMTS];
Stmt *stack_marker_end = deferred_stmts;

internal_proc Stmt *
get_deferred_marker() {
    return stack_marker_end;
}

internal_proc void
set_deferred_marker(Stmt *marker) {
    stack_marker_end = marker;
}

internal_proc void
add_deferred_stmt(Stmt stmt) {
    assert(stack_marker_end != deferred_stmts + MAX_DEFERRED_STMTS);
    *stack_marker_end++ = stmt;
}

internal_proc void
gen_deferred_stmts(Stmt *stack_top, Stmt *stack_bottom) {
    for ( Stmt * it = stack_top; it != stack_bottom; --it ) {
        Stmt *stmt = it - 1;
        gen_stmt(stmt->stmt_defer.stmt);
    }
}

internal_proc void
gen_stmt_block(Stmt_Block block) {
    /* @TODO: einen stack aus defer statements generieren, einen zeiger generieren, sobald man
     *               einen block betritt, der auf den aktuellen stand des stacks zeigt und alle defer
     *               statements nach diesem kennzeichnet, die ausgeführt werden müssen, kurz bevor der
     *               block wieder verlassen wird. außer bei return; da müssen ALLE bisherigen defer
     *               statements ausgeführt werden */
    genf("{");
    gen_indent++;
    Stmt *stack_marker = get_deferred_marker();

    for ( int i = 0; i < block.num_stmts; ++i ) {
        Stmt *stmt = block.stmts[i];
        if ( stmt->kind != STMT_DEFER ) {
            /* @TODO: an dieser stelle entscheiden ob was getan werden muss. falls ein return
             *               statement auftaucht, muss der gesamte stack rückwärts (LIFO) ausgerollt
             *               werden. bei breaks und continues nur bis zum stack_marker              */
            if ( stmt->kind == STMT_RETURN ) {
                gen_deferred_stmts(stack_marker_end, deferred_stmts);
            } else if ( stmt->kind == STMT_BREAK ) {
                gen_deferred_stmts(stack_marker_end, stack_marker);
            }

            gen_stmt(stmt);
        } else {
            add_deferred_stmt(*stmt);
        }
    }

    gen_deferred_stmts(stack_marker_end, stack_marker);
    set_deferred_marker(stack_marker);

    gen_indent--;
    genlnf("}");
}

internal_proc void
gen_cdecl_var(Decl *decl) {
    assert(decl->kind == DECL_VAR);
    gen_decl(decl);
}

internal_proc void
gen_cdecl_const(Decl *decl) {
    assert(decl->kind == DECL_CONST);
    gen_decl(decl);
}

internal_proc void
gen_cdecl_proc(Decl *decl) {
    assert(decl->kind == DECL_PROC);

    if ( decl->is_incomplete ) return;

    gen_cline(get_site(decl));
    gen_cdecl_proc_header(decl);
    genf(" ");
    gen_stmt_block(decl->decl_proc.block);
}

internal_proc void
gen_forward_decls() {
    for ( Sym **it = global_module.syms; it != buf_end(global_module.syms); ++it ) {
        Sym *sym = *it;
        Decl *decl = sym->decl;

        if ( !decl ) { continue; }

        switch ( decl->kind ) {
            case DECL_ENUM: {
                if ( !decl->decl_aggr.typespec ) {
                    genlnf("typedef enum %s %s;", sym_name(sym), sym_name(sym));
                } else {
                    genlnf("typedef %s %s;", cdecl_typespec(decl->decl_aggr.typespec, ""), sym_name(sym));
                }
            } break;

            case DECL_STRUCT: {
                genlnf("typedef struct %s %s;", sym_name(sym), sym_name(sym));
            } break;

            case DECL_UNION: {
                genlnf("typedef union %s %s;", sym_name(sym), sym_name(sym));
            } break;

            default: {
                /* ... */
            } break;
        }
    }
}

internal_proc Type_Field
get_typefield(Type *type, char *name) {
    Type_Field empty = {};
    for ( int i = 0; i < type->type_aggr.num_fields; ++i ) {
        Type_Field field = type->type_aggr.fields[i];
        if ( field.name == name ) {
            return field;
        }
    }
    return empty;
}

internal_proc void
gen_enum(Decl *decl) {
    assert(decl->kind == DECL_ENUM);

    char *name = fetch_resolved_entity_name(decl);
    Type *type = fetch_resolved_type(decl);

    if ( decl->decl_aggr.typespec ) {
        int64_t next_value = 0;
        for ( int i = 0; i < decl->decl_aggr.num_fields; ++i ) {
            Aggregate_Field field = decl->decl_aggr.fields[i];

            genlnf("#define %s__%s (", name, field.name);
            genf("(%s)(", cdecl_typespec(decl->decl_aggr.typespec, ""));

            if ( field.expr ) {
                gen_expr(field.expr);
                next_value = field.expr->expr_int.value + 1;
            } else {
                genf("%d", next_value++);
            }

            genf("))");
        }

        genlnf("#define %s__count (", name);
        genf("(%s)(%d))", cdecl_typespec(decl->decl_aggr.typespec, ""), decl->decl_aggr.num_fields);
    } else {
        genlnf("enum %s {", name);
        gen_indent++;
        for ( int i = 0; i < decl->decl_aggr.num_fields; ++i ) {
            Aggregate_Field field = decl->decl_aggr.fields[i];
            genlnf("%s", strf("%s__%s", name, field.name));
            if ( field.expr ) {
                genf(" = ");
                gen_expr(field.expr);
            }
            genf(",");
        }

        genlnf("%s", strf("%s__%s = %d", name, "count", get_typefield(type, intern_str("count")).value.s64));

        gen_indent--;
        genlnf("};");
    }
}

internal_proc void
gen_type(Type *type, char *name) {
    assert(type);

    switch ( type->kind ) {
        case TYPE_CHAR: { genlnf("char %s", name); } break;
        case TYPE_U8:   { genlnf("u8 %s", name); } break;
        case TYPE_U16:  { genlnf("u16 %s", name); } break;
        case TYPE_U32:  { genlnf("u32 %s", name); } break;
        case TYPE_U64:  { genlnf("u64 %s", name); } break;
        case TYPE_S8:   { genlnf("s8 %s", name); } break;
        case TYPE_S16:  { genlnf("s16 %s", name); } break;
        case TYPE_S32:  { genlnf("s32 %s", name); } break;
        case TYPE_S64:  { genlnf("s64 %s", name); } break;
        case TYPE_F32:  { genlnf("f32 %s", name); } break;
        case TYPE_F64:  { genlnf("f64 %s", name); } break;
        case TYPE_STR:  { genlnf("string %s", name); } break;
        case TYPE_PTR:  {
            gen_type(type->type_ptr.base, strf("* %s", name));
        } break;

        case TYPE_STRUCT:
        case TYPE_UNION:
        case TYPE_ENUM: {
            genlnf("%s %s", sym_name(type->sym), name);
        } break;

        default: {
            assert(0);
        } break;
    }
}

internal_proc void
gen_struct(Decl *decl) {
    assert(decl->kind == DECL_STRUCT);

    char *name = fetch_resolved_entity_name(decl);

    genlnf("struct %s {", name);
    gen_indent++;
    for ( int i = 0; i < decl->decl_aggr.num_fields; ++i ) {
        Aggregate_Field field = decl->decl_aggr.fields[i];
        genlnf("%s;", cdecl_typespec(field.typespec, field.name));

        if ( field.flags & PF_USING ) {
            Type *type = fetch_resolved_type(field.typespec);
            switch ( type->kind ) {
                case TYPE_STRUCT:
                case TYPE_ENUM: {
                    for ( int j = 0; j < type->type_aggr.num_fields; ++j ) {
                        Type_Field using_field = type->type_aggr.fields[j];
                        gen_type(using_field.type, using_field.name);
                    }
                } break;
            }
        }
    }
    gen_indent--;
    genlnf("};");
}

internal_proc void
gen_union(Sym *sym) {
    Decl *decl = sym->decl;
    assert(sym->type->kind == TYPE_UNION);

    genlnf("union %s {", sym_name(sym));
    gen_indent++;
    for ( int i = 0; i < decl->decl_aggr.num_fields; ++i ) {
        Aggregate_Field field = decl->decl_aggr.fields[i];
        genlnf("%s;", cdecl_typespec(field.typespec, field.name));

        if ( field.flags & PF_USING ) {
            Type *type = fetch_resolved_type(field.typespec);
            switch ( type->kind ) {
                case TYPE_STRUCT:
                case TYPE_ENUM: {
                    for ( int j = 0; j < type->type_aggr.num_fields; ++j ) {
                        Type_Field using_field = type->type_aggr.fields[j];
                        gen_type(using_field.type, using_field.name);
                    }
                } break;
            }
        }
    }
    gen_indent--;
    genlnf("};");
}

internal_proc void
gen_expr(Expr *expr) {
    Type *expected_type = fetch_resolved_expected_type(expr);
    Type *type = fetch_resolved_type(expr);
    b32 gen_any = expected_type == type_any && type != type_any;

    if ( gen_any ) {
        genf("(Any){%d, &(%s){", type->type_id, cdecl_type(type, ""));
    }

    switch (expr->kind) {
        case EXPR_INT: {
            genf("%lld", expr->expr_int.value);
        } break;

        case EXPR_CHAR: {
            if ( expr->expr_char.value == '\n' ) {
                genf("'\\n'");
            } else if ( expr->expr_char.value == '\t' ) {
                genf("'\\t'");
            } else if ( expr->expr_char.value == '\r' ) {
                genf("'\\r'");
            } else {
                genf("'%c'", expr->expr_char.value);
            }
        } break;

        case EXPR_FLOAT: {
            genf("%f", expr->expr_float.value);
        } break;

        case EXPR_STR: {
            genf("\"%s\"", expr->expr_str.value);
        } break;

        case EXPR_NAME: {
            char *resolved_expr_name    = fetch_resolved_entity_name(expr);
            char *resolved_foreign_name = fetch_resolved_foreign_name(expr);

            if ( resolved_foreign_name ) {
                genf("%s", resolved_foreign_name);
            } else if ( resolved_expr_name ) {
                genf("%s", resolved_expr_name);
            } else {
                genf("%s", expr->expr_name.value);
            }
        } break;

        case EXPR_CAST: {
            genf("(%s)(", cdecl_typespec(expr->expr_cast.typespec, ""));
            gen_expr(expr->expr_cast.expr);
            genf(")");
        } break;

        case EXPR_CALL: {
            gen_expr(expr->expr_call.expr);
            genf("(");
            for ( int i = 0; i < expr->expr_call.num_args; ++i ) {
                if (i != 0) {
                    genf(", ");
                }
                gen_expr(expr->expr_call.args[i].expr);
            }
            genf(")");
        } break;

        case EXPR_INDEX: {
            gen_expr(expr->expr_index.expr);
            genf("[");
            gen_expr(expr->expr_index.index);
            genf("]");
        } break;

        case EXPR_FIELD: {
#if 0
            /*
               @INFO: diese version funktioniert nicht, weil im fall von z. B.
                      Math.min, das symbol Math doppelt als präfix generiert wird. deshalb
                      muss darauf geprüft werden ob das symbol ein modul ist oder nicht.
             */
            char *resolved_expr_name = fetch_resolved_entity_name(expr->expr_field.expr);
            if ( resolved_expr_name ) {
                genf("%s", resolved_expr_name);
            } else {
                gen_expr(expr->expr_field.expr);
            }

            if ( expr->expr_field.expr->type ) {
                if ( expr->expr_field.expr->type->kind == TYPE_PTR ) {
                    genf("->");
                } else if ( expr->expr_field.expr->type->kind == TYPE_ENUM ) {
                    genf("__");
                } else {
                    genf(".");
                }
            }

            gen_expr(expr->expr_field.field);
#else
            b32 gen_prefix = true;
            if ( expr->expr_field.expr->kind == EXPR_NAME ) {
                /*
                   @FIX: hässliche lösung. hier sollte fetch_resolved_entity_name genutzt werden,
                         falls möglich!
                 */

                Sym *sym = resolve_name(expr->expr_field.expr->expr_name.value);
                if ( sym && sym->kind == SYM_MODULE ) {
                    gen_prefix = false;
                } else {
                    gen_expr(expr->expr_field.expr);
                }
            } else {
                gen_expr(expr->expr_field.expr);
            }

            Type *expr_type = fetch_resolved_type(expr->expr_field.expr);
            if ( expr_type ) {
                if ( expr_type->kind == TYPE_PTR ) {
                    genf("->");
                } else if ( expr_type->kind == TYPE_ENUM ) {
                    genf("__");
                } else {
                    genf(".");
                }
            } else {
                assert(expr->expr_field.expr->kind == EXPR_NAME);
                if ( gen_prefix ) {
                    genf("__");
                }
            }

            gen_expr(expr->expr_field.field);
#endif
        } break;

        case EXPR_COMPOUND: {
            if (expr->expr_compound.typespec) {
                genf("(%s){", cdecl_typespec(expr->expr_compound.typespec, ""));
            } else {
                Type *expr_type = fetch_resolved_type(expr);
                if ( expr_type->kind == TYPE_ARRAY ) {
                    genf("{");
                } else {
                    genf("(%s){", cdecl_type(expr_type, ""));
                }
            }

            if ( expr->expr_compound.num_args ) {
                for ( int i = 0; i < expr->expr_compound.num_args; ++i ) {
                    if (i != 0) {
                        genf(", ");
                    }

                    Compound_Argument arg = expr->expr_compound.args[i];

                    if ( expr->expr_compound.kind == EXPR_COMP_NAME) {
                        genf(".%s = ", arg.name);
                        gen_expr(arg.expr);
                    } else if (expr->expr_compound.kind == EXPR_COMP_LIST) {
                        if ( arg.kind == COMPOUND_LIST ) {
                            gen_expr(arg.expr);
                        } else if ( arg.kind == COMPOUND_INDEX ) {
                            genf("[");
                            gen_expr(arg.index);
                            genf("] = ");
                            gen_expr(arg.expr);
                        }
                    }
                }
            } else {
                genf("0");
            }

            genf("}");
        } break;

        case EXPR_UNARY: {
            genf("%s(", to_unary_op(expr->expr_unary.op));
            gen_expr(expr->expr_unary.expr);
            genf(")");
        } break;

        case EXPR_BINARY: {
            genf("(");
            gen_expr(expr->expr_binary.left);
            genf(") %s (", to_binary_op(expr->expr_binary.op));
            gen_expr(expr->expr_binary.right);
            genf(")");
        } break;

        case EXPR_TERNARY: {
            genf("(");
            gen_expr(expr->expr_ternary.expr);
            genf(" ? ");
            gen_expr(expr->expr_ternary.then_expr);
            genf(" : ");
            gen_expr(expr->expr_ternary.else_expr);
            genf(")");
        } break;

        case EXPR_PAREN: {
            genf("(");
            gen_expr(expr->expr_paren.expr);
            genf(")");
        } break;

        case EXPR_SIZEOF: {
            genf("sizeof(%s)", cdecl_typespec(expr->expr_sizeof.typespec, ""));
        } break;

        case EXPR_TYPEOF: {
            Type *typeof_type = fetch_resolved_type(expr->expr_typeof.typespec);
            genf("%u", typeof_type->type_id);
        } break;

        case EXPR_ALIGNOF: {
            genf("alignof(%s)", cdecl_typespec(expr->expr_alignof.typespec, ""));
        } break;

        case EXPR_OFFSETOF: {
            Typespec *typespec = expr->expr_offsetof.typespec;
            Expr *field = expr->expr_offsetof.expr;
            genf("offsetof(%s, %s)", cdecl_typespec(typespec, ""), field->expr_name.value);
        } break;

        case EXPR_TYPEINFO: {
            genf("typeinfos[");
            gen_expr(expr->expr_typeinfo.expr);
            genf("]");
        } break;

        default: {
            assert(!"nicht unterstützte expression!");
        } break;
    }

    if ( gen_any ) {
        genf("}}");
    }
}

internal_proc void
gen_sym(Sym *sym) {
    Decl *decl = sym->decl;
    if ( !decl ) {
        return;
    }

    if ( decl->is_foreign ) {
        return;
    }

    gen_cline(get_site(decl));

    switch ( decl->kind ) {
        case DECL_CONST: {
            genlnf("#define %s (", sym_name(sym));
            genf("(%s)(", cdecl_type(sym->type, ""));
            gen_expr(decl->decl_const.expr);
            genf("))");
        } break;

        case DECL_VAR: {
            genlnf("extern ");

            if ( decl->decl_var.typespec ) {
                genf("%s", cdecl_typespec(decl->decl_var.typespec, sym_name(sym)));
            } else {
                genf("%s", cdecl_type(sym->type, sym_name(sym)));
            }

            genf(";");
        } break;

        case DECL_PROC: {
            gen_cdecl_proc_header(decl);
            genf(";");
        } break;

        case DECL_STRUCT: {
            gen_struct(decl);
        } break;

        case DECL_UNION: {
            gen_union(sym);
        } break;

        case DECL_TYPEDEF: {
            genlnf("typedef %s;", cdecl_type(sym->type, sym_name(sym)));
        } break;

        case DECL_ENUM: {
            gen_enum(decl);
        } break;

        default: {
            assert(0);
        } break;
    }
    genln();
}

internal_proc void
gen_decl(Decl *decl) {
    Type *type = fetch_resolved_type(decl);

    switch ( decl->kind ) {
        case DECL_CONST: {
            genlnf("enum { %s = ", decl->name);
            gen_expr(decl->decl_const.expr);
            genf(" };");
        } break;

        case DECL_VAR: {
            genlnf("%s", decl->kind == DECL_STRUCT ? "struct " : "");
            genf("%s", cdecl_type(type, decl->name));

            if (decl->decl_var.expr) {
                genf(" = ");
                gen_expr(decl->decl_var.expr);
            } else if ( type->kind == TYPE_STRUCT ) {
                genf(" = (%s){", cdecl_type(type, ""));
                for ( int i = 0; i < type->type_aggr.num_fields; ++i ) {
                    Type_Field field = type->type_aggr.fields[i];

                    genf(".%s = %s", field.name, gen_val(field.value));
                    if ( i < type->type_aggr.num_fields-1 ) {
                        genf(", ");
                    }
                }
                genf("}");
            } else if (type->kind == TYPE_ARRAY ) {
                genf(" = {0}");
            } else {
                genf(" = 0");
            }

            genf(";");
        } break;

        case DECL_PROC: {
            gen_cdecl_proc(decl);
        } break;

        case DECL_STRUCT: {
            gen_struct(decl);
        } break;

        case DECL_ENUM: {
            gen_enum(decl);
        } break;

        case DECL_TYPEDEF: {
            genlnf("typedef %s;", cdecl_type(type, decl->name));
        } break;

        default: {
            assert(!"nicht unterstützter deklarationstyp!");
        } break;
    }
}

internal_proc void
gen_ordered_decls() {
    for ( int i = 0; i < buf_len(ordered_symbols); ++i ) {
        gen_sym(ordered_symbols[i]);
    }
}

internal_proc void
gen_typedefs() {
    genlnf("typedef unsigned char       u8;");
    genlnf("typedef unsigned short      u16;");
    genlnf("typedef unsigned long       u32;");
    genlnf("typedef unsigned long long  u64;");
    genlnf("typedef          char       s8;");
    genlnf("typedef          short      s16;");
    genlnf("typedef          long       s32;");
    genlnf("typedef          long long  s64;");
    genlnf("typedef float               f32;");
    genlnf("typedef double              f64;");
    genlnf("typedef char *              string;");
    genlnf("#ifdef _MSC_VER");
    genlnf("#define alignof(x) __alignof(x)");
    genlnf("#else");
    genlnf("#define alignof(x) __alignof__(x)");
    genlnf("#endif");
}

internal_proc void
gen_typeinfo_fields(Type *type) {
    assert(type->kind == TYPE_UNION || type->kind == TYPE_STRUCT || type->kind == TYPE_ENUM);

    gen_indent++;
    for ( int i = 0; i < type->type_aggr.num_fields; ++i ) {
        Type_Field field = type->type_aggr.fields[i];
        genlnf("{.name = ");
        gen_str(field.name);
        genf(", .type = %d, .offset = %d},", field.type->type_id, field.offset);
    }
    gen_indent--;
}

internal_proc void
gen_typeinfo_header(char *kind, Type *type) {
    if ( type->sym ) {
        genf("&(Type_Info){.kind = %s, .size = sizeof(%s), .align = %d", kind, cdecl_type(type, ""), type->align);
    } else {
        genf("&(Type_Info){.kind = %s, .size = %d, .align = %d", kind, type->size, type->align);
    }
}

internal_proc void
gen_typeinfo(Type *type) {
#define GEN_TYPEINFO(type, ctype) \
    case type: { genf("&(Type_Info){.kind = Type_Kind__" #type ", .name = \"" #ctype "\", .size = sizeof(" #ctype "), .align = sizeof(" #ctype ")},"); } break

    switch ( type->kind ) {
        GEN_TYPEINFO(TYPE_BOOL, bool);
        GEN_TYPEINFO(TYPE_CHAR, char);
        GEN_TYPEINFO(TYPE_S8,   s8);
        GEN_TYPEINFO(TYPE_S16,  s16);
        GEN_TYPEINFO(TYPE_S32,  s32);
        GEN_TYPEINFO(TYPE_S64,  s64);
        GEN_TYPEINFO(TYPE_U8,   u8);
        GEN_TYPEINFO(TYPE_U16,  u16);
        GEN_TYPEINFO(TYPE_U32,  u32);
        GEN_TYPEINFO(TYPE_U64,  u64);
        GEN_TYPEINFO(TYPE_F32,  f32);
        GEN_TYPEINFO(TYPE_F64,  f64);
        GEN_TYPEINFO(TYPE_STR,  string);

        case TYPE_VOID: {
            genf("&(Type_Info){.kind = Type_Kind__TYPE_VOID, .name = \"void\"},");
        } break;

        case TYPE_PTR: {
            genf("&(Type_Info){.kind = Type_Kind__TYPE_PTR, .size = sizeof(void *), .align = sizeof(void *), .base = %d},", type->type_ptr.base->type_id);
        } break;

        case TYPE_ARRAY: {
            gen_typeinfo_header("Type_Kind__TYPE_ARRAY", type);
            genf(", .base = %d, .count = %d},", type->type_array.base->type_id, type->type_array.index);
        } break;

        case TYPE_ENUM:
        case TYPE_UNION:
        case TYPE_STRUCT: {
            if ( type->kind == TYPE_STRUCT ) {
                gen_typeinfo_header("Type_Kind__TYPE_STRUCT", type);
            } else if ( type->kind == TYPE_ENUM ) {
                gen_typeinfo_header("Type_Kind__TYPE_ENUM", type);
            } else {
                assert(type->kind == TYPE_UNION);
                gen_typeinfo_header("Type_Kind__TYPE_UNION", type);
            }

            genf(", .name = ");
            if ( type->sym ) {
                gen_str(sym_name(type->sym));
            } else {
                genf("NULL");
            }

            genf(", .num_fields = %d, .fields = {", type->type_aggr.num_fields);
            gen_typeinfo_fields(type);
            genlnf("}},");
        } break;

        case TYPE_PROC: {
            genf("&(Type_Info){.kind = Type_Kind__TYPE_PROC, .size = %d, .align = %d, .num_returns = %d, .num_params = %d},",
                    type->size, type->align, type->type_proc.num_rets, type->type_proc.num_params);
        } break;

        default: {
            assert(0);
        } break;
    }
#undef GENTYPEINFO
}

internal_proc void
gen_typeinfos() {
    int num_typeinfos = next_type_id;

    genlnf("Type_Info *typeinfo_table[%d] = {", num_typeinfos);
    gen_indent++;

    for ( int i = 0; i < num_typeinfos; ++i ) {
        genlnf("[%d] = ", i);
        Type *type = typeinfo(i);
        if ( !type || type->is_foreign ) {
            genf("NULL,");
            continue;
        }

        gen_typeinfo(type);
    }

    gen_indent--;
    genlnf("};");
    genlnf("int num_typeinfos = %d;", num_typeinfos);
    genlnf("Type_Info **typeinfos = typeinfo_table;");
}

internal_proc void
gen_defs() {
    for ( Sym **it = global_module.syms; it != buf_end(global_module.syms); it++) {
        Sym *sym = *it;
        Decl *decl = sym->decl;
        if ( !decl ) {
            continue;
        }

        if ( decl->kind == DECL_PROC ) {
            gen_cdecl_proc(decl);
        } else if ( decl->kind == DECL_VAR ) {
            if ( decl->decl_var.typespec ) {
                genlnf("%s", cdecl_typespec(decl->decl_var.typespec, sym_name(sym)));
            } else {
                genlnf("%s", cdecl_type(sym->type, sym_name(sym)));
            }

            if (decl->decl_var.expr) {
                genf(" = ");
                gen_expr(decl->decl_var.expr);
            }
            genf(";");
        }
    }
}

internal_proc void
gen_c() {
    gen_result = "";
    genf("/* headers {{{ */");
    gen_headers();
    genlnf("/* }}} */");
    genlnf("/* typedefs {{{ */");
    gen_typedefs();
    genlnf("/* }}} */");
    genlnf("/* forward declarations {{{ */");
    gen_forward_decls();
    genlnf("/* }}} */");
    genlnf("/* ordered declarations {{{ */");
    gen_ordered_decls();
    genlnf("/* }}} */");
    genlnf("/* typeinfo {{{ */");
    gen_typeinfos();
    genlnf("/* }}} */");
    genlnf("/* definitions {{{ */");
    gen_defs();
    genlnf("/* }}} */");
}

