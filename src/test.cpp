internal_proc void
buf_test() {
    int *buf = NULL;
    assert(buf_len(buf) == 0);
    int n = 1024;
    for (int i = 0; i < n; i++) {
        buf_push(buf, i);
    }
    assert(buf_len(buf) == n);
    for (size_t i = 0; i < buf_len(buf); i++) {
        assert(buf[i] == i);
    }
    buf_free(buf);
    assert(buf == NULL);
    assert(buf_len(buf) == 0);
    char *str = NULL;
    buf_printf(str, "One: %d\n", 1);
    assert(strcmp(str, "One: 1\n") == 0);
    buf_printf(str, "Hex: 0x%x\n", 0x12345678);
    assert(strcmp(str, "One: 1\nHex: 0x12345678\n") == 0);
}

internal_proc void
intern_test() {
    char a[] = "hello";
    assert(strcmp(a, intern_str(a)) == 0);
    assert(intern_str(a) == intern_str(a));
    assert(intern_str(intern_str(a)) == intern_str(a));
    char b[] = "hello";
    assert(a != b);
    assert(intern_str(a) == intern_str(b));
    char c[] = "hello!";
    assert(intern_str(a) != intern_str(c));
    char d[] = "hell";
    assert(intern_str(a) != intern_str(d));
}

internal_proc void
strf_test() {
    char *str1 = strf("%s %d %x", "aabb", 11, 3735928559);
    assert(strcmp("aabb 11 deadbeef", str1) == 0);
    char *str2 = strf("%s %s", str1, str1);
    assert(strcmp("aabb 11 deadbeef aabb 11 deadbeef", str2) == 0);
    char *str3 = strf("%s asdf %s", str2, str2);
    assert(strcmp("aabb 11 deadbeef aabb 11 deadbeef asdf aabb 11 deadbeef aabb 11 deadbeef", str3) == 0);
}

internal_proc void
map_test() {
    Map map = {};
    enum { N = 1024*1024 };

    for ( size_t i = 1; i < N; ++i ) {
        map_put(&map, (void *)i, (void *)(i+1));
    }
    for ( size_t i = 1; i < N; ++i ) {
        void *val = map_get(&map, (void *)i);
        assert( val == (void *)(i+1));
    }
}

internal_proc void
common_test() {
    buf_test();
    intern_test();
    strf_test();
    map_test();
}

internal_proc void
parser_test() {
    init_keywords();
    init_datatypes();
    init_commands();

    add_lexer(create_lexer(to_string("x := 5;")));
    Decl **decl = parse_program();

    for ( Decl **it = decl; it != buf_end(decl); ++it ) {
        print(*it);
        printf("\n");
    }
}

internal_proc void
test_fileread() {
    String result = {};

    char *filename_fail = "file_doesnt_exist.txt";
    char *filename_success = "../test/test1.sss";
    assert(!file_read(filename_fail, &result));
    assert(file_read(filename_success, &result));
}

internal_proc void
test_filewrite() {
    char *filename = "../test/test_out.txt";
    char *data    = "xxx";
    assert(file_write(filename, data, strlen(data)));
}

internal_proc void
test_filelist() {
    for ( Dir_Iterator it = dir_list("../test"); it.is_valid; dir_next(&it) ) {
        printf("[%s] %s(%zd bytes)\n", it.is_dir ? "D" : "F", it.name, it.size);
    }
}

internal_proc void
os_test() {
    test_fileread();
    test_filewrite();
    test_filelist();
}

#define assert_token(x) assert(match_token(&lex, x))
#define assert_token_name(x) assert(lex.token.ident == intern_str(x) && match_token(&lex, T_IDENT))
#define assert_token_int(x) assert(lex.token.int_val == (x) && match_token(&lex, T_INT))
#define assert_token_char(x) assert(lex.token.int_val == (x) && match_token(&lex, T_CHAR))
#define assert_token_float(x) assert(lex.token.float_val == (x) && match_token(&lex, T_FLOAT))
#define assert_token_str(x) assert(strcmp(lex.token.str_val, (x)) == 0 && match_token(&lex, T_STR))
#define assert_token_eof() assert(is_token(&lex, 0))

internal_proc void
lexer_test() {
    Lexer lex = {};

    init_stream(to_string("0 2147483647 16b7fffffff 8b42 2b1111"), &lex);
    assert_token_int(0);
    assert_token_int(2147483647);
    assert(lex.token.int_base == 16);
    assert_token_int(0x7fffffff);
    assert(lex.token.int_base == 8);
    assert_token_int(042);
    assert(lex.token.int_base == 2);
    assert_token_int(0xF);
    assert_token_eof();

    init_stream(to_string("3.14 .123 42. 3e10"), &lex);
    assert_token_float(3.14);
    /*
    assert_token_float(.123);
    assert_token_float(42.);
    assert_token_float(3e10);
    assert_token_eof();
    */

    init_stream(to_string("'a' '\n'"), &lex);
    assert_token_char('a');
    assert_token_char('\n');
    assert_token_eof();

    init_stream(to_string("\"foo\" \"a\nb\""), &lex);
    assert_token_str("foo");
    assert_token_str("a\nb");
    assert_token_eof();

    init_stream(to_string(": + += < <="), &lex);
    assert_token(T_TYPESPEC_SEPARATOR);
    assert_token(T_PLUS);
    assert_token(T_ADD_ASSIGN);
    assert_token(T_LT);
    assert_token(T_LEQ);
    assert_token_eof();

    init_stream(to_string("XY+(XY)_HELLO1,234+994"), &lex);
    assert_token_name("XY");
    assert_token(T_PLUS);
    assert_token(T_CALL_OPEN);
    assert_token_name("XY");
    assert_token(T_CALL_CLOSE);
    assert_token_name("_HELLO1");
    assert_token(T_PARAM_SEPARATOR);
    assert_token_int(234);
    assert_token(T_PLUS);
    assert_token_int(994);
    assert_token_eof();
}

#undef assert_token
#undef assert_token_name
#undef assert_token_int
#undef assert_token_char
#undef assert_token_float
#undef assert_token_str
#undef assert_token_eof

internal_proc void
resolver_test() {
#if 0
    char *foo_str  = intern_str("foo");
    Expr *foo_expr = expr_name(zero_site, foo_str);

    assert(sym_get(foo_str) == NULL);
    Decl *decl = decl_const(zero_site, &foo_expr, 1, 0, expr_int(zero_site, 42));
    buf_push(symbols, sym_decl(foo_expr, decl));
    Sym *sym1 = sym_get(foo_str);
    assert(sym1 && sym1->decl == decl);

    Type *int_ptr = type_ptr(type_uint32);
    assert(type_ptr(type_uint32) == int_ptr);

    Type *float_ptr = type_ptr(type_float);
    assert(type_ptr(type_float) == float_ptr);
    assert(int_ptr != float_ptr);

    Type *float4_array = type_array(type_float, 4);
    assert(type_array(type_float, 4) == float4_array);

    Type *float3_array = type_array(type_float, 3);
    assert(type_array(type_float, 3) == float3_array);
    assert(float4_array != float3_array);

    Type *int_ptr_ptr = type_ptr(type_ptr(type_uint32));
    assert(type_ptr(type_ptr(type_uint32)) == int_ptr_ptr);

    Type *int_int_proc = type_proc(&type_uint32, 1, &type_uint32, 1);
    assert(type_proc(&type_uint32, 1, &type_uint32, 1) == int_int_proc);

    Type *int_proc = type_proc(NULL, 0, &type_uint32, 1);
    assert(int_int_proc != int_proc);
    assert(int_proc == type_proc(NULL, 0, &type_uint32, 1));

    sb_reset(symbols);
#endif

    char *resolve_code = {
        "f1 :: (x: u32) -> u32, f32 { return 2*x, 10.0; }\n"
        "f2 :: (x: u32) -> u32 { if x { return -x; } else { return -1; } }\n"
        "f3 :: (x: u32) -> u32 { for i : 0..x-1 { if i % 3 == 0 { return x; } } return 0; }\n"
        "f4 :: (x: u32) -> u32 { switch x { case 0, 1: { return 42; } break; case 3: { return -1; } break; default: { return -1; } } }\n"
        "f5 :: (x: u32) -> u32, f32 { return 10, 1.0; }\n"
        "f6 :: (x: vec3, y: Vec) -> u32 { return 10; }\n"
        "f7 :: (x: u32) -> u32, f32, u32 { return 10, 17.0, 100; }\n"
        "f8 :: (x: u32) { for 0..x { for it2: 0..x { return; } } }\n"
        "a := f6({10.0, 12.0}, {5.0, 3.0});\n"
        "vec3 :: Vec;\n"
        "Vec :: struct { x: f32 = 1.0; y: f32; z: f32; }\n"
        "b, c, d := f7(5);\n"
        /*
        "e : Sq = { x = 10, y = 7 };\n"
        "f : [n] u32 = {1, -2, 3};\n"
        "g := *s[1];\n"
        "h := g[1];\n"
        "i :: ~1 != -2;\n"
        "j : *Sq;\n"
        "k := cast(*void)j;\n"
        "l : [2][2] vec3 = {{{0.0, 1.0, 2.0}, {3.0, 4.0, 5.0}}, {{6.0, 7.0, 8.0}, {9.0, 10.0, 11.0}}};\n"
        "n :: 3;\n"
        "s : [3] u32 = {1, -2, 3};\n"
        "x := c;\n"
        "z := \"ich bin ein string\";\n"
        "Sq :: struct { x, y: u32; }\n"
        "sub := z[2];\n"
        */
    };

    init_global_syms();

    Lexer lex = {};
    init_stream(to_string(resolve_code), &lex);
    add_lexer(lex);

    sym_global_decls(parse_program());
    finalize_syms();

    for ( Sym **it = ordered_symbols; it != buf_end(ordered_symbols); ++it ) {
        Sym *sym = *it;

        if ( sym->decl ) {
            // print(sym);
        } else {
            printf("%s", sym->name);
        }
        printf("\n");
    }
}

internal_proc void
cdecl_test() {
    char *cdecl1 = cdecl_type(type_u32, "x");
    char *cdecl2 = cdecl_type(type_ptr(type_u32), "x");
    char *cdecl3 = cdecl_type(type_array(type_ptr(type_u32), 10), "x");
    char *cdecl4 = cdecl_type(type_proc(&type_u32, 1, &type_u32, 1, false, false), "x");
    char *cdecl5 = cdecl_type(type_array(type_proc(&type_u32, 1, &type_f32, 1, false, false), 10), "x");

    Type *params1[] = {type_ptr(type_u32)};
    char *cdecl6 = cdecl_type(type_proc(params1, 1, &type_f32, 1, false, false), "x");

    Type *params2[] = {type_array(type_u32, 10)};
    Type *params3[] = {type_proc(params2, 1, &type_u32, 1, false, false)};
    char *cdecl7 = cdecl_type(type_proc(params2, 1, &type_char, 1, false, false), "x");
    char *cdecl8 = cdecl_type(type_proc(NULL, 0, params3, 1, false, false), "x");

    Type *params4[] = {type_array(type_proc(NULL, 0, &type_u32, 1, false, false), 10)};
    char *cdecl9 = cdecl_type(type_proc(NULL, 0, params4, 1, false, false), "x");
}

internal_proc void
gen_test() {
    // cdecl_test();

    char *code =
        "Color_Type :: enum { RGB :: 128; RGBA; HSL; HSLA; }\n"
        "n :: 10;\n"
        "s := true;\n"
        "x := (n == 3) ? 0 : 1;\n"
        "Vec :: struct { x: u32; y: u32; }\n"
        "f1 :: (n: u32) -> u32 = 5, fuzzle: f32, b: char = 'a' { if n > 5 { return 2*n, 3.0, 'b'; } else { return n, 5.0, 'c'; } }\n"
        "f2 :: (x: f32, y: char) -> char { yy := true; for 0..10 { return 'x'; } }\n"
        "f3 :: (x: u32) -> f32 { switch x { case 1, 2: { return 1.0; } break; default: { return 2.0; } } return -10.0; }\n"
        "f4 :: (x: u32) -> u32 { y: f32 = 10.0; x1, x2, x3 := f1(s); while (x < 1) { x += 1; } }\n"
        "f5 :: (x: u32) -> u32 { using Color_Type; str := \"1 string\"; return Color_Type.RGB; return RGB; }\n"
        "v1 : Vec = {x = 10, y = 20};\n";

    init_keywords();
    init_datatypes();
    init_commands();
    init_global_syms();

    Lexer lex = {};

    init_stream(to_string(code), &lex);
    add_lexer(lex);

    sym_global_decls(parse_program());
    finalize_syms();

    gen_c();
    printf("%s", gen_result);
}

internal_proc void
main_test() {
    common_test();
    os_test();
    lexer_test();
    parser_test();
    // resolver_test();
    // gen_test();
}

