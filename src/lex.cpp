enum T {
    T_EOF = 0,
    T_INT = 128,
    T_FLOAT,
    T_IDENT,
    T_STR,
    T_CHAR,

    T_FIRST_CMP,
    T_LT = T_FIRST_CMP,
    T_LEQ,
    T_GT,
    T_GEQ,
    T_EQ,
    T_NEQ,
    T_LAST_CMP = T_NEQ,

    T_FIRST_UNARY,
    T_STAR = T_FIRST_UNARY,
    T_MINUS,
    T_MUL,
    T_POINTER = T_MUL,
    T_PLUS,
    T_DEREF,
    T_NEGATE,
    T_NOT,
    T_LAST_UNARY = T_NOT,

    T_EQL_ASSIGN,
    T_FIRST_ASSIGN = T_EQL_ASSIGN,
    T_ADD_ASSIGN,
    T_SUB_ASSIGN,
    T_OR_ASSIGN,
    T_AND_ASSIGN,
    T_XOR_ASSIGN,
    T_MODULO_ASSIGN,
    T_MUL_ASSIGN,
    T_DIV_ASSIGN,
    T_LAST_ASSIGN = T_DIV_ASSIGN,

    T_LSHIFT,
    T_RSHIFT,
    T_MODULO,
    T_DIV,
    T_OR,
    T_XOR,
    T_BIT_AND,
    T_BIT_OR,
    T_AND,
    T_RANGE,

    T_COMPILER_CMD,
    T_NOTE,
    T_CALL_OPEN,
    T_CALL_CLOSE,
    T_PARAM_SEPARATOR,
    T_COMMA = T_PARAM_SEPARATOR,
    T_INDEX_OPEN,
    T_INDEX_CLOSE,
    T_DECL_END,
    T_STMT_END = T_DECL_END,
    T_TYPESPEC_SEPARATOR,

    T_ARROW,
    T_ELLIPSIS,
    T_FIELD,
    T_DOT = T_FIELD,

    T_BLOCK_BEGIN,
    T_BLOCK_END,

    T_ILLEGAL,
};

typedef uint32_t Token_Kind;

struct Site {
    size_t row;
    size_t col;
    char*  start;
    char*  end;
    char*  name;
};

global_var Site zero_site = {};

internal_proc char *
to_dbgstring(Token_Kind kind) {
    switch ( kind ) {
        case T_EOF:                 { return "<EOF>";                } break;
        case T_INT:                 { return "T_INT";                } break;
        case T_FLOAT:               { return "T_FLOAT";              } break;
        case T_IDENT:               { return "T_IDENT";              } break;
        case T_STR:                 { return "T_STR";                } break;
        case T_CHAR:                { return "T_CHAR";               } break;
        case T_LT:                  { return "T_LT";                 } break;
        case T_LEQ:                 { return "T_LEQ";                } break;
        case T_GT:                  { return "T_GT";                 } break;
        case T_GEQ:                 { return "T_GEQ";                } break;
        case T_EQ:                  { return "T_EQ";                 } break;
        case T_NEQ:                 { return "T_NEQ";                } break;
        case T_STAR:                { return "T_STAR";               } break;
        case T_MINUS:               { return "T_MINUS";              } break;
        case T_MUL:                 { return "T_MUL";                } break;
        case T_PLUS:                { return "T_PLUS";               } break;
        case T_DEREF:               { return "T_DEREF";              } break;
        case T_NEGATE:              { return "T_NEGATE";             } break;
        case T_NOT:                 { return "T_NOT";                } break;
        case T_EQL_ASSIGN:          { return "T_EQL_ASSIGN";         } break;
        case T_ADD_ASSIGN:          { return "T_ADD_ASSIGN";         } break;
        case T_SUB_ASSIGN:          { return "T_SUB_ASSIGN";         } break;
        case T_OR_ASSIGN:           { return "T_OR_ASSIGN";          } break;
        case T_AND_ASSIGN:          { return "T_AND_ASSIGN";         } break;
        case T_XOR_ASSIGN:          { return "T_XOR_ASSIGN";         } break;
        case T_MODULO_ASSIGN:       { return "T_MODULO_ASSIGN";      } break;
        case T_MUL_ASSIGN:          { return "T_MUL_ASSIGN";         } break;
        case T_DIV_ASSIGN:          { return "T_DIV_ASSIGN";         } break;
        case T_LSHIFT:              { return "T_LSHIFT";             } break;
        case T_RSHIFT:              { return "T_RSHIFT";             } break;
        case T_MODULO:              { return "T_MODULO";             } break;
        case T_DIV:                 { return "T_DIV";                } break;
        case T_OR:                  { return "T_OR";                 } break;
        case T_XOR:                 { return "T_XOR";                } break;
        case T_BIT_AND:             { return "T_BIT_AND";            } break;
        case T_BIT_OR:              { return "T_BIT_OR";             } break;
        case T_AND:                 { return "T_AND";                } break;
        case T_RANGE:               { return "T_RANGE";              } break;
        case T_COMPILER_CMD:        { return "T_COMPILER_CMD";       } break;
        case T_NOTE:                { return "T_NOTE";               } break;
        case T_CALL_OPEN:           { return "T_CALL_OPEN";          } break;
        case T_CALL_CLOSE:          { return "T_CALL_CLOSE";         } break;
        case T_PARAM_SEPARATOR:     { return "T_PARAM_SEPARATOR";    } break;
        case T_INDEX_OPEN:          { return "T_INDEX_OPEN";         } break;
        case T_INDEX_CLOSE:         { return "T_INDEX_CLOSE";        } break;
        case T_DECL_END:            { return "T_DECL_END";           } break;
        case T_TYPESPEC_SEPARATOR:  { return "T_TYPESPEC_SEPARATOR"; } break;
        case T_ARROW:               { return "T_ARROW";              } break;
        case T_ELLIPSIS:            { return "T_ELLIPSIS";           } break;
        case T_FIELD:               { return "T_FIELD";              } break;
        case T_BLOCK_BEGIN:         { return "T_BLOCK_BEGIN";        } break;
        case T_BLOCK_END:           { return "T_BLOCK_END";          } break;
        case T_ILLEGAL:             { return "T_ILLEGAL";            } break;
        default:                    { return "<UNBEKANNT>";          } break;
    }
}

internal_proc int64_t
site_len(Site site) {
    return site.end - site.start;
}

internal_proc char *
site_pos(Site site) {
    return site.start;
}

enum Token_Flags {
    TF_NONE,
    TF_KEYWORD,
};

struct Token {
    Token_Kind kind;
    Site site;
    uint32_t flags;
    uint8_t  int_base;
    char* start;
    char* end;
    char* ident;

    union {
        char*    str_val;
        uint64_t int_val;
        double   float_val;
    };
};

struct Lexer {
    Site site;
    String stream;
    char at[3];
    Token token;
};

internal_proc void init_stream(String code, Lexer *lex, char *name);
internal_proc Token get_token( Lexer *lex );

internal_proc void
refill( Lexer *lex ) {
    if ( lex->stream.count == 0 ) {
        lex->at[0] = 0;
        lex->at[1] = 0;
        lex->at[2] = 0;
    } else if ( lex->stream.count == 1 ) {
        lex->at[0] = lex->stream.data[0];
        lex->at[1] = 0;
        lex->at[2] = 0;
    } else if ( lex->stream.count == 2 ) {
        lex->at[0] = lex->stream.data[0];
        lex->at[1] = lex->stream.data[1];
        lex->at[2] = 0;
    } else {
        lex->at[0] = lex->stream.data[0];
        lex->at[1] = lex->stream.data[1];
        lex->at[2] = lex->stream.data[2];
    }
}

internal_proc void
init_lexer( Lexer *lex, String input ) {
    lex->stream = input;
    lex->site.row = 0;
    lex->site.col = 0;
    refill(lex);
}

internal_proc Lexer
create_lexer(String code, char *name = NULL) {
    Lexer result = {};

    init_lexer(&result, code);

    result.site.row = 1;
    result.site.name = (name) ? name : "<raw>";

    return result;
}

internal_proc void
init_stream(String code, Lexer *lex, char *name = NULL) {
    init_lexer(lex, code);

    lex->site.row  = 1;
    lex->site.name = (name) ? _strdup(name) : "<from string>";

    get_token(lex);
}

internal_proc char
ch0(Lexer *lex) {
    return lex->at[0];
}

internal_proc char
ch1(Lexer *lex) {
    return lex->at[1];
}

internal_proc char
ch2(Lexer *lex) {
    return lex->at[2];
}

global_var char **keywords;
global_var char *keyword_if;
global_var char *keyword_else;
global_var char *keyword_for;
global_var char *keyword_while;
global_var char *keyword_return;
global_var char *keyword_switch;
global_var char *keyword_struct;
global_var char *keyword_union;
global_var char *keyword_typedef;
global_var char *keyword_enum;
global_var char *keyword_proc;
global_var char *keyword_case;
global_var char *keyword_default;
global_var char *keyword_break;
global_var char *keyword_continue;
global_var char *keyword_cast;
global_var char *keyword_using;
global_var char *keyword_true;
global_var char *keyword_false;
global_var char *keyword_defer;
global_var char *keyword_claim;
global_var char *keyword_free;
global_var char *keyword_sizeof;
global_var char *keyword_typeof;
global_var char *keyword_typeinfo;
global_var char *keyword_alignof;
global_var char *keyword_offsetof;

global_var char **datatypes;
global_var char *datatype_u8;
global_var char *datatype_u16;
global_var char *datatype_u32;
global_var char *datatype_u64;
global_var char *datatype_s8;
global_var char *datatype_s16;
global_var char *datatype_s32;
global_var char *datatype_s64;
global_var char *datatype_f32;
global_var char *datatype_f64;
global_var char *datatype_string;
global_var char *datatype_bool;
global_var char *datatype_void;
global_var char *datatype_char;

global_var char **commands;
global_var char *command_run;
global_var char *command_import;
global_var char *command_load;
global_var char *command_if;
global_var char *command_foreign;

internal_proc void
init_keywords() {
#define ADD_KEYWORD(Key) keyword_##Key = intern_str(#Key); buf_push(keywords, keyword_##Key)

    ADD_KEYWORD(if);
    ADD_KEYWORD(else);
    ADD_KEYWORD(for);
    ADD_KEYWORD(while);
    ADD_KEYWORD(return);
    ADD_KEYWORD(switch);
    ADD_KEYWORD(struct);
    ADD_KEYWORD(union);
    ADD_KEYWORD(typedef);
    ADD_KEYWORD(enum);
    ADD_KEYWORD(proc);
    ADD_KEYWORD(case);
    ADD_KEYWORD(default);
    ADD_KEYWORD(break);
    ADD_KEYWORD(continue);
    ADD_KEYWORD(cast);
    ADD_KEYWORD(using);
    ADD_KEYWORD(true);
    ADD_KEYWORD(false);
    ADD_KEYWORD(defer);
    ADD_KEYWORD(claim);
    ADD_KEYWORD(free);
    ADD_KEYWORD(sizeof);
    ADD_KEYWORD(typeof);
    ADD_KEYWORD(typeinfo);
    ADD_KEYWORD(alignof);
    ADD_KEYWORD(offsetof);

#undef ADD_KEYWORD
}

internal_proc void
init_datatypes() {
#define ADD_DATATYPE(Key) datatype_##Key = intern_str(#Key); buf_push(datatypes, datatype_##Key)

    ADD_DATATYPE(u8);
    ADD_DATATYPE(u16);
    ADD_DATATYPE(u32);
    ADD_DATATYPE(u64);
    ADD_DATATYPE(s8);
    ADD_DATATYPE(s16);
    ADD_DATATYPE(s32);
    ADD_DATATYPE(s64);
    ADD_DATATYPE(f32);
    ADD_DATATYPE(f64);
    ADD_DATATYPE(string);
    ADD_DATATYPE(bool);
    ADD_DATATYPE(void);
    ADD_DATATYPE(char);

#undef ADD_DATATYPE
}

internal_proc void
init_commands() {
#define ADD_COMMAND(Key) command_##Key = intern_str(#Key); buf_push(commands, command_##Key)

    ADD_COMMAND(run);
    ADD_COMMAND(if);
    ADD_COMMAND(load);
    ADD_COMMAND(import);
    ADD_COMMAND(foreign);

#undef ADD_COMMAND
}

global_var Token token_illegal = {T_ILLEGAL, 0, 0, 0};

internal_proc Site
get_site(Lexer *lex) {
    return lex->token.site;
}

internal_proc b32
in_range(Token token, Token_Kind min, Token_Kind max) {
    b32 result = (min <= token.kind && token.kind <= max);

    return result;
}

internal_proc b32
is_cmp(Token token) {
    b32 result = in_range(token, T_FIRST_CMP, T_LAST_CMP);

    return result;
}

internal_proc b32
is_cmp(Token_Kind kind) {
    b32 result = T_FIRST_CMP <= kind && kind <= T_LAST_CMP;

    return result;
}

internal_proc b32
is_unary(Token token) {
    b32 result = in_range(token, T_FIRST_UNARY, T_LAST_UNARY);

    return result;
}

internal_proc b32
is_assign(Token token) {
    b32 result = in_range(token, T_FIRST_ASSIGN, T_LAST_ASSIGN);

    return result;
}

internal_proc b32
is_eof(char c) {
    b32 result = (c == '\0');

    return result;
}

internal_proc void
next( Lexer *lex, size_t count = 1 ) {
    for ( size_t i = count; i > 0; --i ) {
        if ( !is_eof(ch0(lex)) ) {
            advance_str( &lex->stream );
            lex->site.col++;
        }
    }

    refill(lex);
}

internal_proc b32
is_token(Lexer *lex, Token_Kind kind) {
    return (lex->token.kind == kind);
}

internal_proc b32
is_keyword(char *str) {
    for ( char **it = keywords; it != buf_end(keywords); ++it ) {
        if ( *it == str ) {
            return 1;
        }
    }

    return 0;
}

internal_proc b32
is_keyword(Lexer *lex, char *expected_keyword) {
    if ( lex->token.ident == expected_keyword ) {
        return 1;
    }

    return 0;
}

internal_proc b32
is_keyword(Lexer *lex) {
    return is_token(lex, T_IDENT) && is_keyword(lex->token.ident);
}

internal_proc b32
is_intrinsic(char *d) {
    for ( char **it = datatypes; it != buf_end(datatypes); ++it ) {
        if ( *it == d ) {
            return 1;
        }
    }

    return 0;
}

internal_proc void
skip_comment( Lexer *lex ) {
    if ( ch0(lex) == '#' && ch1(lex) == '#' ) {
        next(lex, 2);

        if ( ch0(lex) == '(' ) {
            while ( ch0(lex) != '#' || ch1(lex) != '#' || ch2(lex) != ')' ) {
                next(lex);

                if ( ch0(lex) == '\n' ) {
                    lex->site.row++;
                    lex->site.col = 1;
                }

                if ( ch0(lex) == '#' && ch1(lex) == '#' && ch2(lex) == '(') {
                    skip_comment( lex );
                }
            }

            next(lex, 3);
        } else {
            while ( ch0(lex) != '\n' ) {
                next(lex);
            }
        }
    }
}

internal_proc b32
in_int_range(char ch) {
    if ( '0' <= ch && ch <= '9' ||
         'A' <= ch && ch <= 'F' ||
         'a' <= ch && ch <= 'f' )
    {
        return true;
    }

    return false;
}

internal_proc Token
get_token( Lexer *lex ) {
retry:

    skip_comment(lex);

    Token token = {};
    token.start = (char *)lex->stream.data;
    token.site  = lex->site;
    token.site.start = token.start;

    switch ( ch0(lex) ) {
        case 0: {
            token.kind = T_EOF;
        } break;

        case ' ': case '\t': case '\r':
        case '\n': case '\v': {
            while( ch0(lex) != '\0' &&
                    ( ch0(lex) == ' ' || ch0(lex) == '\t' ||
                      ch0(lex) == '\r' || ch0(lex) == '\n') )
            {
                if ( ch0(lex) == '\n' ) {
                    lex->site.row++;
                    lex->site.col = 1;
                }

                next(lex);
            }

            goto retry;
        } break;

        case 'A': case 'B': case 'C':
        case 'D': case 'E': case 'F':
        case 'G': case 'H': case 'I':
        case 'J': case 'K': case 'L':
        case 'M': case 'N': case 'O':
        case 'P': case 'Q': case 'R':
        case 'S': case 'T': case 'U':
        case 'V': case 'W': case 'X':
        case 'Y': case 'Z':
        case 'a': case 'b': case 'c':
        case 'd': case 'e': case 'f':
        case 'g': case 'h': case 'i':
        case 'j': case 'k': case 'l':
        case 'm': case 'n': case 'o':
        case 'p': case 'q': case 'r':
        case 's': case 't': case 'u':
        case 'v': case 'w': case 'x':
        case 'y': case 'z': case '_': {
            token.kind = T_IDENT;

            while ( !is_eof(ch0(lex)) && (is_alpha(ch0(lex)) ||
                     is_digit(ch0(lex))) || ch0(lex) == '_' ) {
                next(lex);
            }

            token.site.end = (char *)lex->stream.data;
            token.ident = intern_str_range(token.site.start, token.site.end);

            if ( is_keyword(token.ident) ) {
                token.flags |= TF_KEYWORD;
            } else {
                token.flags &= ~(TF_KEYWORD);
            }
        } break;

        case '0': case '1': case '2':
        case '3': case '4': case '5':
        case '6': case '7': case '8':
        case '9': {
            token.kind = T_INT;

            uint8_t char_to_digit[256];
            char_to_digit['0'] = 0;
            char_to_digit['1'] = 1;
            char_to_digit['2'] = 2;
            char_to_digit['3'] = 3;
            char_to_digit['4'] = 4;
            char_to_digit['5'] = 5;
            char_to_digit['6'] = 6;
            char_to_digit['7'] = 7;
            char_to_digit['8'] = 8;
            char_to_digit['9'] = 9;
            char_to_digit['a'] = 10;
            char_to_digit['A'] = 10;
            char_to_digit['b'] = 11;
            char_to_digit['B'] = 11;
            char_to_digit['c'] = 12;
            char_to_digit['C'] = 12;
            char_to_digit['d'] = 13;
            char_to_digit['D'] = 13;
            char_to_digit['e'] = 14;
            char_to_digit['E'] = 14;
            char_to_digit['f'] = 15;
            char_to_digit['F'] = 15;

            token.int_val = 0;
            uint64_t int_val = 0;
            bool is_float = false;
            uint8_t base = 0;

            while ( !is_eof(ch0(lex)) && is_digit(ch0(lex)) ) {
                uint64_t digit = char_to_digit[ch0(lex)];

                token.int_val *= 10;
                token.int_val += digit;

                int_val *= 10;
                int_val += digit;

                next(lex);

                if ( ch0(lex) == '_' ) {
                    next(lex);
                    continue;
                }
            }

            if ( ch0(lex) == 'b' ) {
                base = (uint8_t)int_val;
                int_val = 0;

#define BASE_MAX 16
                assert(base <= BASE_MAX);

                token.int_base = base;
                next(lex);

                while ( !is_eof(ch0(lex)) && ( in_int_range(ch0(lex)) || ch0(lex) == '_' ) ) {
                    if ( ch0(lex) == '_' ) {
                        next(lex);
                        continue;
                    }

                    uint64_t digit = char_to_digit[ch0(lex)];

                    if ( digit >= base ) {
                        error(lex->site, "angegebene wert '%c' ist größer als die angegebene basis '%d'", ch0(lex), base);
                    }

                    int_val *= base;
                    int_val += digit;
                    next(lex);
                }

                token.int_val = int_val;
            } else if (ch0(lex) == '.' && is_digit(ch1(lex))) {
                is_float = true;
                next(lex);

                while ( !is_eof(ch0(lex)) && is_digit(ch0(lex)) ) {
                    next(lex);
                }
            }

            token.site.end = (char *)lex->stream.data;

            if ( is_float ) {
                token.kind = T_FLOAT;
                token.float_val = strtod(token.start, NULL);
            }
        } break;

        case '"': {
            token.kind = T_STR;

            next(lex);
            while ( !is_eof(ch0(lex)) && ch0(lex) != '"' ) {
                if (ch0(lex) == '\\') {
                    next(lex);
                }

                next(lex);
            }

            token.str_val = intern_str_range(token.start+1, (char *)lex->stream.data);
            next(lex);
        } break;

        case '\'': {
            token.kind = T_CHAR;
            next(lex);

            if ( ch0(lex) == '\\' ) {
                next(lex);

                if ( ch0(lex) == 'n' ) {
                    token.int_val = '\n';
                } else if ( ch0(lex) == 'r' ) {
                    token.int_val = '\r';
                } else if ( ch0(lex) == 't' ) {
                    token.int_val = '\t';
                }

                next(lex);
            } else {
                token.int_val = (int64_t)ch0(lex);
                next(lex);
            }

            if ( ch0(lex) != '\'' ) {
                assert(0);
            }

            next(lex);
        } break;

        case '<': {
            token.kind = T_LT;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_LEQ;
                next(lex);
            } else if ( ch0(lex) == '<' ) {
                token.kind = T_LSHIFT;
                next(lex);

                if ( ch0(lex) == '<' ) {
                    token.kind = T_DEREF;
                    next(lex);
                }
            }
        } break;

        case '>': {
            token.kind = T_GT;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_GEQ;
                next(lex);
            } else if ( ch0(lex) == '>' ) {
                token.kind = T_RSHIFT;
                next(lex);
            }
        } break;

        case '=': {
            token.kind = T_EQL_ASSIGN;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_EQ;
                next(lex);
            }
        } break;

        case '!': {
            token.kind = T_NOT;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_NEQ;
                next(lex);
            }
        } break;

        case '|': {
            token.kind = T_BIT_OR;
            next(lex);

            if ( ch0(lex) == '|' ) {
                token.kind = T_OR;
                next(lex);
            } else if ( ch0(lex) == '=' ) {
                token.kind = T_OR_ASSIGN;
                next(lex);
            }
        } break;

        case '^': {
            token.kind = T_XOR;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_XOR_ASSIGN;
                next(lex);
            }
        } break;

        case '&': {
            token.kind = T_BIT_AND;
            next(lex);

            if ( ch0(lex) == '&' ) {
                token.kind = T_AND;
                next(lex);
            } else if ( ch0(lex) == '=' ) {
                token.kind = T_AND_ASSIGN;
                next(lex);
            }
        } break;

        case '.': {
            token.kind = T_FIELD;
            next(lex);

            if ( ch0(lex) == '.' ) {
                token.kind = T_RANGE;
                next(lex);

                if (ch0(lex) == '.') {
                    token.kind = T_ELLIPSIS;
                    next(lex);
                }
            }
        } break;

        case '+': {
            token.kind = T_PLUS;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_ADD_ASSIGN;
                next(lex);
            }
        } break;

        case '-': {
            token.kind = T_MINUS;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_SUB_ASSIGN;
                next(lex);
            } else if ( ch0(lex) == '>' ) {
                token.kind = T_ARROW;
                next(lex);
            }
        } break;

        case '*': {
            token.kind = T_MUL;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_MUL_ASSIGN;
                next(lex);
            }
        } break;

        case '~': {
            token.kind = T_NEGATE;
            next(lex);
        } break;

        case '@': {
            token.kind = T_NOTE;
            next(lex);
        } break;

        case '%': {
            token.kind = T_MODULO;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_MODULO_ASSIGN;
                next(lex);
            }
        } break;

        case '#': {
            token.kind = T_COMPILER_CMD;
            next(lex);
        } break;

        case '/': {
            token.kind = T_DIV;
            next(lex);

            if ( ch0(lex) == '=' ) {
                token.kind = T_DIV_ASSIGN;
                next(lex);
            }
        } break;

        case '{': {
            token.kind = T_BLOCK_BEGIN;
            next(lex);
        } break;

        case '}': {
            token.kind = T_BLOCK_END;
            next(lex);
        } break;

        case '(': {
            token.kind = T_CALL_OPEN;
            next(lex);
        } break;

        case ')': {
            token.kind = T_CALL_CLOSE;
            next(lex);
        } break;

        case ',': {
            token.kind = T_PARAM_SEPARATOR;
            next(lex);
        } break;

        case '[': {
            token.kind = T_INDEX_OPEN;
            next(lex);
        } break;

        case ']': {
            token.kind = T_INDEX_CLOSE;
            next(lex);
        } break;

        case ';': {
            token.kind = T_DECL_END;
            next(lex);
        } break;

        case ':': {
            token.kind = T_TYPESPEC_SEPARATOR;
            next(lex);
        } break;

        default: {
            token.kind = ch0(lex);
            next(lex);
        } break;
    }

    token.end = lex->at;
    lex->token = token;

    return token;
}

internal_proc Token
eat_token(Lexer *lex) {
    Token result = lex->token;
    get_token(lex);

    return result;
}

internal_proc b32
match_token(Lexer *lex, Token_Kind expect_token) {
    if ( expect_token == lex->token.kind ) {
        get_token(lex);
        return 1;
    }

    return 0;
}

internal_proc b32
match_datatype(Lexer *lex, char *expected_datatype) {
    if ( lex->token.ident == expected_datatype ) {
        get_token(lex);
        return 1;
    }

    return 0;
}

internal_proc b32
match_string(Lexer *lex, char *expected_str) {
    if ( lex->token.ident == expected_str ) {
        get_token(lex);
        return 1;
    }

    return 0;
}

internal_proc b32
match_keyword(Lexer *lex, char *expected_keyword) {
    if ( is_keyword(lex, expected_keyword) ) {
        get_token(lex);
        return 1;
    }

    return 0;
}

internal_proc b32
match_command(Lexer *lex, char *expected_command) {
    if ( lex->token.ident == expected_command ) {
        get_token(lex);
        return 1;
    }

    return 0;
}

internal_proc Token
expect_token(Lexer *lex, Token_Kind expected_token) {
    if ( expected_token == lex->token.kind ) {
        Token result = lex->token;
        get_token(lex);
        return result;
    } else {
        error(get_site(lex), "erwartetes token '%s', vorgefunden wurde aber '%s'",
                to_dbgstring(expected_token), to_dbgstring(lex->token.kind));
        return token_illegal;
    }
}

