internal_proc Lexer init_compiler(char *filename);
internal_proc void add_lexer(Lexer lex);
internal_proc Decl * parse_decl(Lexer *lex);
internal_proc Decl * parse_compiler_cmd(Lexer *lex);

struct Scope {
    char *name;
    Map syms;
    Scope *parent;
};

struct Module_Desc {
    char **names;
    size_t num_names;
    char *alias;
};

struct Module {
    char *alias;

    Decl **decls;
    size_t num_decls;

    Scope *scope;
    Sym **syms;
};

global_var Module **modules;
global_var Map loaded_modules;
global_var Module global_module;
global_var Module *current_module = &global_module;
global_var Scope *current_scope;

internal_proc Scope *
scope_new(char *name = NULL) {
    Scope *result = (Scope *)xmalloc(sizeof(Scope));

    result->parent = 0;
    result->syms   = {};
    result->name   = name;

    return result;
}

internal_proc Scope *
scope_enter(char *name = NULL) {
    Scope *result = scope_new(name);

    result->parent = current_scope;
    current_scope = result;

    return result;
}

internal_proc Scope *
scope_set(Scope *scope) {
    Scope *temp = current_scope;

    current_scope = scope;

    return temp;
}

internal_proc void
scope_exit() {
    current_scope = current_scope->parent;
}

internal_proc void
record_loaded_module(char *key, char *value) {
    map_put(&loaded_modules, key, value);
}

internal_proc char *
fetch_loaded_module(char *key) {
    return (char *)map_get(&loaded_modules, key);
}

internal_proc Module *
module_enter(Module *module) {
    if ( !module ) {
        return current_module;
    }

    Module *old_module = current_module;
    current_module = module;

    return old_module;
}

internal_proc void
module_leave(Module *module) {
    current_module = module;
}

internal_proc Module *
module_new(char *alias, Decl **decls, size_t num_decls) {
    Module *result = (Module *)xcalloc(1, sizeof(Module));

    result->alias = alias;
    result->decls = decls;
    result->num_decls = num_decls;
    result->scope = scope_new();

    return result;
}

internal_proc Module *
module_compile(Dir_Iterator it, char *alias) {
    assert(it.is_valid);

    Decl **decls = NULL;
    for (;it.is_valid; dir_next(&it) ) {
        if ( strcmp( path_ext(it.full_path), "sss" ) == 0 ) {
            Lexer lexer = init_compiler(it.full_path);

            while ( !is_token(&lexer, T_EOF) ) {
                if ( match_token(&lexer, T_COMPILER_CMD) ) {
                    Decl *decl = parse_compiler_cmd(&lexer);
                    if ( decl ) {
                        buf_push(decls, decl);
                    }
                } else {
                    buf_push(decls, parse_decl(&lexer));
                }
            }
        }
    }

    return module_new(alias, decls, buf_len(decls));
}

internal_proc Module *
module_load(char *path, Module_Desc desc) {
    Dir_Iterator it = dir_list(path);
    char *module_name = "";
    for ( int i = 0; i < desc.num_names; ++i ) {
        module_name = desc.names[i];
        for ( ; it.is_valid ; dir_next(&it) ) {
            if ( strcmp(it.name, desc.names[i]) == 0 ) {
                if ( !it.is_dir ) {
                    error(zero_site, "'%s' im modulnamen muss ein verzeichnis sein", it.name);
                }

                Dir_Iterator temp = it;
                path_join(it.path, it.name);
                it = dir_list(it.path);
                dir_free(&temp);

                break; // springt zurück in die "fori" iteration
            }
        }
    }

    if ( it.is_valid && !it.error ) {
        return module_compile(it, desc.alias);
    } else {
        error(zero_site, "konnte modul '%s' in '%s' nicht laden!", module_name, path);
        return 0;
    }
}

internal_proc Module_Desc
module_descriptor(Expr *expr, Expr *alias) {
    Module_Desc result = {};
    char *modulespec = "";

    if ( alias ) {
        result.alias = alias->expr_name.value;
    }

    Expr *it = expr;
    if ( it->kind == EXPR_NAME ) {
        modulespec = strf("%s", expr->expr_name.value);
        buf_push(result.names, expr->expr_name.value);
        result.num_names = buf_len(result.names);

        return result;
    }

    char **names = 0;
    for (;;) {
        Expr *left  = it->expr_field.expr;
        char *right = it->expr_field.field;

        buf_push(names, right);
        modulespec = strf("%s.%s", modulespec, right);
        if ( left->kind == EXPR_NAME ) {
            modulespec = strf("%s%s", left->expr_name.value, modulespec);
            buf_push(names, left->expr_name.value);
            break;
        } else {
            it = left;
        }
    }

    for ( size_t i = buf_len(names); i > 0; --i ) {
        buf_push(result.names, names[i-1]);
    }
    result.num_names = buf_len(result.names);

    modulespec = intern_str(modulespec);
    if ( fetch_loaded_module(modulespec) ) {
        return {};
    }
    record_loaded_module(modulespec, modulespec);

    return result;
}

internal_proc Module_Desc
module_descriptor(char *modulespec) {
    Module_Desc result = {};

    modulespec = intern_str(modulespec);
    if ( fetch_loaded_module(modulespec) ) {
        return {};
    }

    char buf[MAX_PATH];
    char *ch = modulespec;
    char *start = modulespec;

    for ( ; *ch; ++ch ) {
        if ( *ch == '.' ) {
            strncpy(buf, start, ch - start);
            buf[ch - start] = 0;
            buf_push(result.names, _strdup(buf));
            start = ch + 1;
        }
    }

    strncpy(buf, start, ch - start);
    buf[ch - start] = 0;
    buf_push(result.names, _strdup(buf));
    result.num_names = buf_len(result.names);
    record_loaded_module(modulespec, modulespec);

    return result;
}

