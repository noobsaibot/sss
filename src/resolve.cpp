struct Type;
struct Sym;

internal_proc b32     resolve_stmt_block(Stmt_Block block, Type **rets, size_t num_rets);
internal_proc Type *  resolve_decl_var(Decl *decl);
internal_proc Type * resolve_typespec(Typespec *typespec);
internal_proc void   complete_type(Type *type);
internal_proc Type * type_incomplete(Sym *sym);
internal_proc Type * type_ptr(Type *elem);
internal_proc void   resolve_sym(Sym *sym);
internal_proc void   resolve_proc(Sym *sym);
internal_proc void   init_typeids();

enum Val_Kind {
    VAL_NONE,

    VAL_CHAR,
    VAL_BYTE,

    VAL_S8,
    VAL_S16,
    VAL_S32,
    VAL_INT = VAL_S32,
    VAL_S64,

    VAL_U8,
    VAL_U16,
    VAL_U32,
    VAL_UINT = VAL_U32,
    VAL_U64,

    VAL_F32,
    VAL_F64,

    VAL_STR,
    VAL_BOOL,
};

struct Val {
    Val_Kind kind;

    union {
        float    f32;
        double   f64;

        uint8_t  u8;
        uint16_t u16;
        uint32_t u32;
        uint64_t u64;

        int8_t   s8;
        int16_t  s16;
        int32_t  s32;
        int64_t  s64;

        uintptr_t ptr;

        char*    s;
        b32      b;
    };
};

global_var Val val_null = {};

internal_proc Val
val_new(Val_Kind kind) {
    Val result = { kind };

    return result;
}

internal_proc Val
val_f32(float value) {
    Val result = val_new(VAL_F32);

    result.f32 = value;

    return result;
}

internal_proc Val
val_f64(double value) {
    Val result = val_new(VAL_F64);

    result.f64 = value;

    return result;
}

internal_proc Val
val_s8(int8_t value) {
    Val result = val_new(VAL_S8);

    result.s8   = value;

    return result;
}

internal_proc Val
val_s16(int16_t value) {
    Val result = val_new(VAL_S16);

    result.s16  = value;

    return result;
}

internal_proc Val
val_s32(int32_t value) {
    Val result = val_new(VAL_S32);

    result.s32 = value;

    return result;
}

internal_proc Val
val_s64(int64_t value) {
    Val result = val_new(VAL_S64);

    result.s64 = value;

    return result;
}

internal_proc Val
val_int(int64_t value) {
    Val result = val_new(VAL_INT);

    result.s32  = (int32_t)value;

    return result;
}

internal_proc Val
val_u8(uint8_t value) {
    Val result = val_new(VAL_U8);

    result.u8 = value;

    return result;
}

internal_proc Val
val_u16(uint16_t value) {
    Val result = val_new(VAL_U16);

    result.u16 = value;

    return result;
}

internal_proc Val
val_u32(uint32_t value) {
    Val result = val_new(VAL_U32);

    result.u32 = value;

    return result;
}

internal_proc Val
val_u64(uint64_t value) {
    Val result = val_new(VAL_U64);

    result.u64 = value;

    return result;
}

internal_proc Val
val_str(char *value) {
    Val result = val_new(VAL_STR);

    result.s = value;

    return result;
}

internal_proc b32
is_int(Val value) {
    return ( VAL_S8 <= value.kind && value.kind <= VAL_U64 );
}

internal_proc Val
val_add(Val a, Val b) {
    Val result = {};

    if ( a.kind == VAL_F32 || b.kind == VAL_F32 ) {
        result.kind = VAL_F32;

        if (a.kind == VAL_F32 && is_int(b)) {
            result.f32 = a.f32 + b.s32;
        } else if (a.kind == VAL_INT && b.kind == VAL_F32) {
            result.f32 = a.s32 + b.f32;
        }
    } else if ( a.kind == VAL_F64 || b.kind == VAL_F64 ) {
        result.kind = VAL_F64;

        if (a.kind == VAL_F64 && is_int(b)) {
            result.f64 = a.f64 + b.s64;
        } else if (is_int(a) && b.kind == VAL_F64) {
            result.f64 = a.s64 + b.f64;
        } else if (a.kind == VAL_F64 && b.kind == VAL_F64) {
            result.f64 = a.f64 + b.f64;
        }
    } else {
        result.kind = VAL_INT;
        result.s32  = a.s32 + b.s32;
    }

    return result;
}

internal_proc Val
val_sub(Val a, Val b) {
    Val result = {};

    if ( a.kind == VAL_F32 || b.kind == VAL_F32 ) {
        result.kind = VAL_F32;

        if (a.kind == VAL_F32 && b.kind == VAL_INT) {
            result.f32 = a.f32 - b.s32;
        } else if (a.kind == VAL_INT && b.kind == VAL_F32) {
            result.f32 = a.s32 - b.f32;
        }
    } else {
        result.kind = VAL_INT;

        result.s32 = a.s32 - b.s32;
    }

    return result;
}

internal_proc Val
val_mul(Val a, Val b) {
    Val result = {};

    if ( a.kind == VAL_F32 || b.kind == VAL_F32 ) {
        result.kind = VAL_F32;

        if (a.kind == VAL_F32 && b.kind == VAL_INT) {
            result.f32 = a.f32 * b.s32;
        } else if (a.kind == VAL_INT && b.kind == VAL_F32) {
            result.f32 = a.s32 * b.f32;
        }
    } else {
        result.kind = VAL_INT;

        result.s32 = a.s32 * b.s32;
    }

    return result;
}

internal_proc Val
val_div(Val a, Val b) {
    Val result = {};

    if ( a.kind == VAL_F32 || b.kind == VAL_F32 ) {
        result.kind = VAL_F32;

        if (a.kind == VAL_F32 && b.kind == VAL_INT) {
            result.f32 = a.f32 / b.s32;
        } else if (a.kind == VAL_INT && b.kind == VAL_F32) {
            result.f32 = a.s32 / b.f32;
        }
    } else if ( a.kind == VAL_F64 || b.kind == VAL_F64 ) {
        result.kind = VAL_F64;

        if (a.kind == VAL_F64 && is_int(b)) {
            result.f64 = a.f64 / b.s64;
        } else if (is_int(a) && b.kind == VAL_F64) {
            result.f64 = a.s64 / b.f64;
        } else if (a.kind == VAL_F64 && b.kind == VAL_F64) {
            result.f64 = a.f64 / b.f64;
        }
    } else {
        result.kind = VAL_INT;

        result.s32 = a.s32 / b.s32;
    }

    return result;
}

internal_proc Val
val_mod(Val a, Val b) {
    Val result = {};

    assert(a.kind == VAL_INT && b.kind == VAL_INT);
    result.kind = VAL_INT;
    result.s32 = a.s32 % b.s32;

    return result;
}

internal_proc Val
operator+(Val a, Val b) {
    return val_add(a, b);
}

internal_proc Val
operator-(Val a, Val b) {
    return val_sub(a, b);
}

internal_proc Val
operator-(Val a) {
    if ( a.kind == VAL_F32 || a.kind == VAL_F64 ) {
        return val_f64(-a.f64);
    }

    if ( a.kind >= VAL_S8 && a.kind <= VAL_S64 ) {
        return val_s64(-a.s64);
    }

    if ( a.kind >= VAL_U8 && a.kind <= VAL_U64 ) {
        return a;
    }

    return a;
}

internal_proc Val
operator*(Val a, Val b) {
    return val_mul(a, b);
}

internal_proc Val
operator/(Val a, Val b) {
    return val_div(a, b);
}

internal_proc Val
val_bool(Token_Kind op, Val a, Val b) {
    assert(a.kind == b.kind);
    Val result = {VAL_BOOL};

    switch ( op ) {
        case T_EQ: {
            if ( a.kind == VAL_INT ) {
                result.b = (a.s32 == b.s32);
            } else if ( a.kind == VAL_BOOL ) {
                result.b = (a.b == b.b);
            } else {
                assert(!"nicht unterstützter datentyp für vergleichsoperator!");
            }
        } break;

        case T_NEQ: {
            if ( a.kind == VAL_INT ) {
                result.b = (a.s32 != b.s32);
            } else if ( a.kind == VAL_BOOL ) {
                result.b = (a.b != b.b);
            } else {
                assert(!"nicht unterstützter datentyp für vergleichsoperator!");
            }
        } break;
    }

    return result;
}

internal_proc Val
val_op(Token_Kind op, Val left, Val right) {
    switch ( op ) {
        case T_PLUS: {
            return val_add(left, right);
        } break;

        case T_MINUS: {
            return val_sub(left, right);
        } break;

        case T_MUL: {
            return val_mul(left, right);
        } break;

        case T_DIV: {
            return val_div(left, right);
        } break;

        case T_MODULO: {
            return val_mod(left, right);
        } break;

        case T_NEQ:
        case T_EQ: {
            return val_bool(op, left, right);
        } break;

        default: {
            assert(0);
            return val_null;
        } break;
    }
}

enum Sym_State {
    SYM_UNRESOLVED,
    SYM_RESOLVING,
    SYM_RESOLVED,
};

enum Sym_Kind {
    SYM_NONE,
    SYM_VAR,
    SYM_CONST,
    SYM_PROC,
    SYM_TYPE,
    SYM_MODULE,
};

struct Sym {
    Sym_State state;
    Sym_Kind kind;

    char *name;
    char *foreign_name;

    Decl *decl;
    Type *type;

    Module *module;

    Val value;
};

global_var Sym **  ordered_symbols;

internal_proc Sym *
sym_new(Sym_Kind kind, char *name, char *foreign_name, Decl *decl, Sym_State state = SYM_UNRESOLVED) {
    Sym *result = (Sym *)xcalloc(1, sizeof(Sym));

    result->kind  = kind;
    result->state = state;
    result->name  = name;
    result->foreign_name = foreign_name;
    result->decl  = decl;

    if ( decl ) {
        decl->sym = result;
    }

    return result;
}

internal_proc Sym *
sym_get_local(char *name) {
    Sym *result = (Sym *)map_get(&current_scope->syms, name);

    return result;
}

internal_proc Sym *
sym_get(char *name) {
    for ( Scope * it = current_scope; it; it = it->parent ) {
        Sym *sym = (Sym *)map_get(&it->syms, name);
        if ( sym ) {
            return sym;
        }
    }

    return (Sym *)map_get(&current_module->scope->syms, name);
}

internal_proc Sym *
sym_decl(char *name, Decl *decl) {
    Sym_Kind kind = SYM_NONE;

    switch ( decl->kind ) {
        case DECL_STRUCT:
        case DECL_UNION:
        case DECL_ENUM:
        case DECL_TYPEDEF: {
            kind = SYM_TYPE;
        } break;

        case DECL_VAR: {
            kind = SYM_VAR;
        } break;

        case DECL_CONST: {
            kind = SYM_CONST;
        } break;

        case DECL_PROC: {
            kind = SYM_PROC;
        } break;

        default: {
            assert(0);
        } break;
    }

    Sym *result = sym_new(kind, name, decl->foreign_name, decl);
    if ( decl->kind == DECL_UNION || decl->kind == DECL_STRUCT || decl->kind == DECL_ENUM ) {
        result->state = SYM_RESOLVED;
        result->type  = type_incomplete(result);
    }

    return result;
}

internal_proc void
sym_global_put(Sym *sym) {
    if ( map_get(&global_module.scope->syms, sym->name) ) {
        error(sym->decl->site, "mehrfache deklaration von '%s'", sym->name);
    }

    map_put(&global_module.scope->syms, (void *)sym->name, sym);
    buf_push(global_module.syms, sym);
}

internal_proc Sym *
sym_global_decl(char *name, Decl *decl) {
    Sym *sym = sym_decl(name, decl);
    sym_global_put(sym);
    sym->decl = decl;

    return sym;
}

internal_proc void
sym_global_decls(Decl **decls) {
    for ( int i = 0; i < buf_len(decls); ++i ) {
        Decl *decl = decls[i];
        sym_global_decl(decl->name, decl);
    }
}

internal_proc void
sym_module_decls(Module *module) {
    if ( module->alias ) {
        char *foreign_name = "";
        Sym *module_sym = sym_new(SYM_MODULE, module->alias, foreign_name, 0, SYM_RESOLVED);
        module_sym->module = module;
        sym_global_put(module_sym);

        for ( int i = 0; i < module->num_decls; ++i ) {
            Decl *decl = module->decls[i];
            char *sym_name = NULL;
            buf_printf(sym_name, "%s__%s", module->alias, decl->name);
            Sym *sym = sym_global_decl(sym_name, decl);
            sym->module = module;
            buf_push(module->syms, sym);

            if ( map_get(&module->scope->syms, decl->name) ) {
                error(get_site(decl), "mehrfach deklaration des symbols '%s'!", decl->name);
            }

            map_put(&module->scope->syms, decl->name, sym);
        }
    } else {
        sym_global_decls(module->decls);
    }
}

internal_proc b32
sym_push_const(char *name, Type *type, Decl *decl, Val value) {
    if (sym_get_local(name)) {
        return false;
    }

    Sym *sym = sym_new(SYM_CONST, name, 0, decl, SYM_RESOLVED);
    sym->type  = type;
    sym->value = value;
    map_put(&current_scope->syms, name, sym);

    return true;
}

internal_proc b32
sym_push_var(char *name, Type *type, Decl *decl = NULL) {
    if (sym_get_local(name)) {
        return false;
    }

    Sym *sym = sym_new(SYM_VAR, name, 0, decl, SYM_RESOLVED);
    sym->type  = type;
    map_put(&current_scope->syms, name, sym);

    return true;
}

internal_proc Sym *
sym_install(Expr *expr, Type *type) {
    assert(sym_get(expr->expr_name.value) == NULL);
    assert(expr->kind == EXPR_NAME);

    char *foreign_name = "";
    Sym *result = sym_new(SYM_TYPE, expr->expr_name.value, foreign_name, NULL, SYM_RESOLVED);

    result->type = type;
    map_put(&current_module->scope->syms, result->name, result);
    buf_push(current_module->syms, result);

    return result;
}

internal_proc Sym *
sym_install_intrinsic(char *name, Type *type) {
    Expr *type_name = expr_name(zero_site, intern_str(name));
    return sym_install(type_name, type);
}

internal_proc void
finalize_sym(Sym *sym) {
    Module *prev_module = module_enter(sym->module);

    resolve_sym(sym);
    if ( sym->kind == SYM_TYPE ) {
        complete_type(sym->type);
    } else if ( sym->kind == SYM_PROC ) {
        resolve_proc(sym);
    }

    module_leave(prev_module);
}

internal_proc void
finalize_syms(void) {
    for ( int i = 0; i != buf_len(current_module->syms); ++i ) {
        Sym *sym = current_module->syms[i];
        if ( sym->decl ) {
            finalize_sym(sym);
        }
    }
}

struct Type_Field {
    char *name;
    Type *type;
    Val value;
    size_t offset;
};

enum Type_Kind {
    TYPE_NONE,
    TYPE_INCOMPLETE,
    TYPE_COMPLETING,

    /* int, arithmetic, scalar */
    TYPE_CHAR,
    TYPE_U8,
    TYPE_U16,
    TYPE_U32,
    TYPE_UINT = TYPE_U32,
    TYPE_U64,

    TYPE_S8,
    TYPE_S16,
    TYPE_S32,
    TYPE_INT = TYPE_S32,
    TYPE_S64,

    TYPE_ENUM,
    TYPE_BOOL,
    /* ende: int */

    TYPE_F32,
    TYPE_FLOAT = TYPE_F32,
    TYPE_F64,
    /* ende: arithmetic */

    TYPE_PTR,
    TYPE_PROC,
    /* ende: scalar */

    TYPE_ARRAY,
    TYPE_STRUCT,
    TYPE_UNION,
    TYPE_STR,
    TYPE_VOID,
    TYPE_VARIADIC,

    MAX_TYPES,
};

typedef uint32_t Type_Id;
struct Type {
    Type_Kind kind;
    size_t size;
    size_t align;
    Type_Id type_id;
    b32 is_foreign;

    Sym *sym;
    Typespec *typespec;

    union {
        struct {
            Type *base;
        } type_ptr;
        struct {
            Type *base;
            size_t index;
            b32 is_inferred;
        } type_array;
        struct {
            Type_Field *fields;
            size_t num_fields;
            Scope *scope;
        } type_aggr;
        struct {
            Type **params;
            size_t num_params;
            Type **rets;
            size_t num_rets;
            b32 is_variadic;
        } type_proc;
    };
};

Map typeid_map;

internal_proc void
register_typeid(Type *type) {
    map_put(&typeid_map, (void *)(uintptr_t)type->type_id, type);
}

#define TYPE(name, kind, size, type_id) \
    global_var Type name##_val = {kind, size, size, type_id}; \
    global_var Type *name = &name##_val

TYPE(type_void,     TYPE_VOID,      0, 1);
TYPE(type_bool,     TYPE_BOOL,      1, 2);
TYPE(type_char,     TYPE_CHAR,      1, 3);

TYPE(type_u8,       TYPE_U8,        1, 4);
TYPE(type_u16,      TYPE_U16,       2, 5);
TYPE(type_u32,      TYPE_U32,       4, 6);
TYPE(type_u64,      TYPE_U64,       8, 7);

TYPE(type_s8,       TYPE_S8,        1, 8);
TYPE(type_s16,      TYPE_S16,       2, 9);
TYPE(type_s32,      TYPE_S32,       4, 10);
TYPE(type_s64,      TYPE_S64,       8, 11);

TYPE(type_f32,      TYPE_F32,       4, 12);
TYPE(type_f64,      TYPE_F64,       8, 13);
TYPE(type_string,   TYPE_STR,       8, 14);
TYPE(type_variadic, TYPE_VARIADIC,  0, 15);

Type_Id next_type_id = 16;

Type *type_any;

#undef TYPE

internal_proc Type *
typeinfo(Type_Id id) {
    if ( id == 0 ) {
        return NULL;
    }
    return (Type *)map_get(&typeid_map, (void *)(uintptr_t)id);
}

internal_proc Type *
type_new(Type_Kind kind) {
    Type *result = (Type *)xcalloc(1, sizeof(Type));

    result->kind = kind;
    result->is_foreign = false;
    result->type_id = next_type_id++;

    register_typeid(result);

    return result;
}

internal_proc b32
is_signed(Type *type) {
    b32 result = (TYPE_S8 <= type->kind && type->kind <= TYPE_S64);

    return result;
}

internal_proc b32
is_int(Type *type) {
    b32 result = (TYPE_CHAR <= type->kind && type->kind <= TYPE_BOOL);

    return result;
}

internal_proc b32
is_float(Type *type) {
    b32 result = (TYPE_F32 <= type->kind && type->kind <= TYPE_F64);

    return result;
}

internal_proc b32
is_arithmetic(Type *type) {
    b32 result = (TYPE_CHAR <= type->kind && type->kind <= TYPE_F64);

    return result;
}

internal_proc b32
is_aggregate(Type *type) {
    return ( type->kind == TYPE_UNION || type->kind == TYPE_STRUCT || type->kind == TYPE_ENUM );
}

internal_proc b32
is_scalar(Type *type) {
    b32 result = TYPE_CHAR <= type->kind && type->kind <= TYPE_PROC;

    return result;
}

internal_proc b32
is_ptr(Type *type) {
    return type->kind == TYPE_PTR;
}

internal_proc b32
is_array(Type *type) {
    return type->kind == TYPE_ARRAY;
}

internal_proc b32
is_string(Type *type) {
    return type->kind == TYPE_PTR && type->type_ptr.base->kind == TYPE_CHAR;
}

internal_proc b32
is_proc(Type *type) {
    b32 result = type->kind == TYPE_PROC;

    return result;
}

internal_proc b32
is_variadic(Type *type) {
    assert(is_proc(type));
    b32 result = type->type_proc.is_variadic;

    return result;
}

internal_proc b32
is_any(Type *type) {
    return type == type_any;
}

internal_proc Type *
unsigned_type(Type *type) {
    switch (type->kind) {
        case TYPE_CHAR:
        case TYPE_S8:
        case TYPE_U8: {
            return type_u8;
        } break;

        case TYPE_S16:
        case TYPE_U16: {
            return type_u16;
        } break;

        case TYPE_S32:
        case TYPE_U32: {
            return type_u32;
        } break;

        case TYPE_S64:
        case TYPE_U64: {
            return type_u64;
        } break;

        default: {
            assert(0);
            return NULL;
        } break;
    }
}

internal_proc int
type_rank(Type *type) {
    int type_ranks[MAX_TYPES] = {};

    type_ranks[TYPE_CHAR] = 1;
    type_ranks[TYPE_S8]   = 1;
    type_ranks[TYPE_U8]   = 1;
    type_ranks[TYPE_S16]  = 2;
    type_ranks[TYPE_U16]  = 2;
    type_ranks[TYPE_S32]  = 3;
    type_ranks[TYPE_U32]  = 3;
    type_ranks[TYPE_S64]  = 5;
    type_ranks[TYPE_U64]  = 5;

    return type_ranks[type->kind];
}

internal_proc size_t
type_sizeof(Type *type) {
    assert(type->kind > TYPE_COMPLETING);
    assert(type->size != 0);
    return type->size;
}

struct Operand {
    Type *type;
    b32 is_lvalue;
    b32 is_const;
    b32 is_module;
    Val value;
    Module *module;
};

internal_proc Operand resolve_expr(Expr *expr);
internal_proc Operand resolve_expected_expr(Expr *expr, Type *expected_type);
internal_proc Operand resolve_expected_expr(Expr *expr, Type *expected_type);
internal_proc Operand resolve_expr_compound(Expr *expr, Type *expected_type);
internal_proc Operand resolve_expr_call(Expr *expr);
internal_proc Operand resolve_binary_op(Token_Kind op, Operand left, Operand right);
internal_proc Operand resolve_expr_rvalue(Expr *expr);

Operand operand_null;

internal_proc Operand *
operand_alloc(Type *type, b32 is_lvalue = false, b32 is_const = false, Val value = {}) {
    Operand *result = (Operand *)xcalloc(1, sizeof(Operand));

    result->type = type;
    result->is_lvalue = is_lvalue;
    result->is_const = is_const;
    result->value = value;

    return result;
}

internal_proc Operand
operand_new(Type *type, b32 is_lvalue = false, b32 is_const = false,
        Val value = {})
{
    Operand result = {};

    result.type      = type;
    result.is_lvalue = is_lvalue;
    result.is_const  = is_const;
    result.value     = value;

    return result;
}

internal_proc Operand
operand_lvalue(Type *type) {
    return operand_new(type, true);
}

internal_proc Operand
operand_rvalue(Type *type) {
    return operand_new(type);
}

internal_proc Operand
operand_module(Module *module) {
    Operand result = operand_new(0);

    result.module = module;
    result.is_module = true;

    return result;
}

internal_proc Operand
operand_const(Type *type, Val value) {
    return operand_new(type, false, true, value);
}

#define CASE(k, t) \
    case k: \
        switch (type->kind) { \
        case TYPE_CHAR: \
            operand->value.s8  = (char)operand->value.t; \
            break; \
        case TYPE_U8: \
            operand->value.u8  = (unsigned char)operand->value.t; \
            break; \
        case TYPE_S8: \
            operand->value.s8  = (signed char)operand->value.t; \
            break; \
        case TYPE_S16: \
            operand->value.s16 = (short)operand->value.t; \
            break; \
        case TYPE_U16: \
            operand->value.u16 = (unsigned short)operand->value.t; \
            break; \
        case TYPE_S32: \
            operand->value.s32 = (int)operand->value.t; \
            break; \
        case TYPE_U32: \
            operand->value.u32 = (unsigned)operand->value.t; \
            break; \
        case TYPE_S64: \
            operand->value.s64 = (long long)operand->value.t; \
            break; \
        case TYPE_U64: \
            operand->value.u64 = (unsigned long long)operand->value.t; \
            break; \
        case TYPE_F32: \
            operand->value.f32 = (float)operand->value.t; \
            break; \
        case TYPE_F64: \
            operand->value.f64 = (double)operand->value.t; \
            break; \
        case TYPE_ENUM: \
            operand->value.s64 = (int64_t)operand->value.t; \
            break; \
        case TYPE_PTR: \
            operand->value.ptr = (uintptr_t)operand->value.t; \
            break; \
        default: \
            operand->is_const = false; \
            break; \
        } \
        break;

internal_proc b32 is_convertible(Operand *operand, Type *dest);
internal_proc b32
is_castable(Operand *operand, Type *dest) {
    Type *src = operand->type;
    if (is_convertible(operand, dest)) {
        return true;
    } else if (is_int(dest)) {
        return is_ptr(src);
    } else if (is_int(src)) {
        return is_ptr(dest);
    } else if (is_ptr(dest) && is_ptr(src)) {
        return true;
    } else if (is_any(dest)) {
        return true;
    } else {
        return false;
    }
}

internal_proc b32
cast_operand(Operand *operand, Type *type) {
    if (operand->type != type) {
        if (!is_castable(operand, type)) {
            return false;
        }

        if (operand->is_const) {
            if (is_float(operand->type)) {
                operand->is_const = !is_int(type);
            } else {
                switch (operand->type->kind) {
                    CASE(TYPE_BOOL,   b)
                    CASE(TYPE_U8,    u8)
                    CASE(TYPE_U16,  u16)
                    CASE(TYPE_U32,  u32)
                    CASE(TYPE_U64,  u64)
                    CASE(TYPE_S8,    s8)
                    CASE(TYPE_S16,  s16)
                    CASE(TYPE_S32,  s32)
                    CASE(TYPE_S64,  s64)
                    CASE(TYPE_ENUM, s32)
                    CASE(TYPE_PTR,  ptr)
                    default: {
                        operand->is_const = false;
                    } break;
                }
            }
        }
    }
    operand->type = type;
    return true;
}

internal_proc b32
is_null(Operand operand) {
    if (operand.is_const && (is_ptr(operand.type) || is_int(operand.type))) {
        cast_operand(&operand, type_u64);
        return operand.value.u64 == 0;
    } else {
        return false;
    }
}

internal_proc b32
is_convertible(Operand *operand, Type *dest) {
    Type *src = operand->type;
    if (src == dest) {
        return true;
    } else if (is_arithmetic(src) && is_arithmetic(dest)) {
        return true;
    } else if (dest == type_void) {
        return true;
    } else if (is_ptr(dest) && is_ptr(src)) {
        return dest->type_ptr.base == type_void || src->type_ptr.base == type_void;
    } else if ( is_int(dest) && src->kind == TYPE_ENUM ) {
        return true;
    } else if (is_ptr(dest) && is_null(*operand)) {
        return true;
    } else if (is_any(dest)) {
        return true;
    } else {
        return false;
    }
}

internal_proc b32
convert_operand(Operand *operand, Type *type) {
    if (is_convertible(operand, type)) {
        cast_operand(operand, type);
        operand->is_lvalue = false;

        return true;
    }

    return false;
}

#undef CASE

internal_proc Val
convert_const(Type *dest_type, Type *src_type, Val src_val) {
    Operand operand = operand_const(src_type, src_val);
    convert_operand(&operand, dest_type);

    return operand.value;
}

internal_proc void
promote_operand(Operand *operand) {
    switch (operand->type->kind) {
        case TYPE_S8:
        case TYPE_U8:
        case TYPE_S16:
        case TYPE_U16: {
            convert_operand(operand, type_s32);
        } break;

        default: {
        } break;
    }
}

internal_proc Type *
promote_type(Type *type) {
    if ( type == type_char ) return type_u8;

    return type;
}

internal_proc void
unify_arithmetic_operands(Operand *left, Operand *right) {
    if (left->type == type_f64) {
        convert_operand(right, type_f64);
    } else if (right->type == type_f64) {
        convert_operand(left, type_f64);
    } else if (left->type == type_f32) {
        convert_operand(right, type_f32);
    } else if (right->type == type_f32) {
        convert_operand(left, type_f32);
    } else {
        promote_operand(left);
        promote_operand(right);
        if (left->type != right->type) {
            if (is_signed(left->type) == is_signed(right->type)) {
                if (type_rank(left->type) <= type_rank(right->type)) {
                    convert_operand(left, right->type);
                } else {
                    convert_operand(right, left->type);
                }
            } else if (is_signed(left->type) && type_rank(right->type) >= type_rank(left->type)) {
                convert_operand(left, right->type);
            } else if (is_signed(right->type) && type_rank(left->type) >= type_rank(right->type)) {
                convert_operand(right, left->type);
            } else if (is_signed(left->type) && type_sizeof(left->type) > type_sizeof(right->type)) {
                convert_operand(right, left->type);
            } else if (is_signed(right->type) && type_sizeof(right->type) > type_sizeof(left->type)) {
                convert_operand(left, right->type);
            } else {
                Type *type = unsigned_type(is_signed(left->type) ? left->type : right->type);
                convert_operand(left, type);
                convert_operand(right, type);
            }
        }
    }
    assert(left->type == right->type);
}

#define PTR_SIZE 8
#define PTR_ALIGN 8

global_var Map cached_ptr_types;

struct Cached_Array_Type {
    Cached_Array_Type *next;
    Type *type;
};

global_var Map cached_array_types;

struct Cached_Proc_Type {
    Type **params;
    size_t num_params;

    Type **rets;
    size_t num_rets;

    Type *type_proc;
};

global_var Cached_Proc_Type *cached_proc_types;

internal_proc size_t
type_alignof(Type *type) {
    return type->align;
}

internal_proc Type_Field
type_field(char *name, Type *type, Val value = val_null) {
    Type_Field result = {};

    result.name  = name;
    result.type  = type;
    result.value = value;

    return result;
}

internal_proc void
type_complete_struct(Type *type, Type_Field *fields, size_t num_fields) {
    assert(type->kind == TYPE_COMPLETING);

    type->kind = TYPE_STRUCT;
    type->size = 0;

    size_t offset = 0;
    size_t align = 0;
    for ( size_t i = 0; i < buf_len(fields); ++i ) {
        type->size += fields[i].type->size;
        type->align = MAX(type->size, type_alignof(fields[i].type));
        fields[i].offset = offset;
        offset += fields[i].type->size;
    }

    type->type_aggr.fields = (Type_Field *)memdup(fields, num_fields * sizeof(*fields));
    type->type_aggr.num_fields = num_fields;
}

internal_proc void
type_complete_union(Type *type, Type_Field *fields, size_t num_fields) {
    assert(type->kind == TYPE_COMPLETING);

    type->kind = TYPE_UNION;
    type->size = 0;

    size_t offset = 0;
    size_t align = 0;
    for ( size_t i = 0; i < buf_len(fields); ++i ) {
        if ( fields[i].type->size > type->size ) {
            type->size = fields[i].type->size;
        }

        type->align = MAX(type->size, type_alignof(fields[i].type));
        fields[i].offset = offset;
        offset += fields[i].type->size;
    }

    type->type_aggr.fields = (Type_Field *)memdup(fields, num_fields * sizeof(*fields));
    type->type_aggr.num_fields = num_fields;
}

internal_proc void
type_complete_enum(Type *type, Type_Field *fields, size_t num_fields) {
    assert(type->kind == TYPE_COMPLETING);

    type->kind = TYPE_ENUM;
    type->size = num_fields * type_s32->size;

    size_t offset = 0;
    for ( size_t i = 0; i < buf_len(fields); ++i ) {
        fields[i].offset = offset;
        offset += fields[i].type->size;
    }

    buf_push(fields, type_field(intern_str("count"), type_s64, val_s64(num_fields)));

    type->type_aggr.fields = (Type_Field *)memdup(fields, num_fields * sizeof(*fields));
    type->type_aggr.num_fields = num_fields;
}

internal_proc b32
has_dups(Type_Field *fields, size_t num_fields) {
    for ( size_t i = 0; i < num_fields; i++ ) {
        for ( size_t j = i + 1; j < num_fields; j++ ) {
            if ( fields[i].name == fields[j].name ) {
                return true;
            }
        }
    }
    return false;
}

internal_proc void
complete_type(Type *type) {
    if ( type->kind == TYPE_COMPLETING ) {
        error(type->sym->decl->site, "zirkuläre abhängigkeit festgestellt!");
        return;
    } else if (type->kind != TYPE_INCOMPLETE) {
        return;
    }

    type->kind = TYPE_COMPLETING;
    Decl *decl = type->sym->decl;
    assert(decl->kind == DECL_UNION || decl->kind == DECL_STRUCT || decl->kind == DECL_ENUM);

    Type_Field *fields = NULL;

    int64_t current_value = 0;

    type->type_aggr.scope = scope_enter(decl->name);
    for ( size_t i = 0; i < decl->decl_aggr.num_fields; i++ ) {
        Aggregate_Field field = decl->decl_aggr.fields[i];

        Type *field_type = 0;
        if ( field.typespec ) {
            field_type = resolve_typespec(field.typespec);
        }

        Operand operand = {};
        if ( field.expr ) {
            operand = resolve_expr(field.expr);
            field_type = operand.type;
        }

        if ( !field_type ) {
            error(field.site, "datentyp des feldes %s konnte nicht ermittelt werden", field.name);
        }

        complete_type(field_type);
        if ( field.expr ) {
            current_value = field.expr->expr_int.value;
        }

        buf_push(fields, type_field(field.name, field_type, val_s64(current_value)));
        if ( decl->kind == DECL_ENUM ) {
            sym_push_const(field.name, field_type, 0, val_s64(current_value));
        } else {
            sym_push_var(field.name, field_type);
        }
        current_value++;

        if ( field.flags & PF_USING ) {
            switch ( field_type->kind ) {
                case TYPE_STRUCT: {
                    for ( int j = 0; j < field_type->type_aggr.num_fields; ++j ) {
                        buf_push(fields, field_type->type_aggr.fields[j]);
                    }
                } break;

                case TYPE_ENUM: {
                    for ( int j = 0; j < field_type->type_aggr.num_fields; ++j ) {
                        buf_push(fields, field_type->type_aggr.fields[j]);
                    }
                } break;

                default: {
                    error(field.site, "using wird auf diesem datentyp nicht unterstützt!");
                } break;
            }
        }
    }

    if ( decl->kind == DECL_ENUM ) {
        sym_push_const(intern_str("count"), type_s64, 0, val_s64(decl->decl_aggr.num_fields));
    }

    scope_exit();

    if ( !buf_len(fields) ) {
        error(decl->site, "datenstruktur muss mindestens ein feld enthalten!");
    }

    if ( has_dups(fields, buf_len(fields)) ) {
        error(decl->site, "felder mehrfach definiert!");
    }

    if ( decl->kind == DECL_STRUCT ) {
        type_complete_struct(type, fields, buf_len(fields));
    } else if ( decl->kind == DECL_ENUM ) {
        type_complete_enum(type, fields, buf_len(fields));
    } else if ( decl->kind == DECL_UNION ) {
        type_complete_union(type, fields, buf_len(fields));
    }

    buf_push(ordered_symbols, type->sym);
}

internal_proc Type *
type_incomplete(Sym *sym) {
    Type *result = type_new(TYPE_INCOMPLETE);
    result->sym = sym;

    return result;
}

internal_proc Type *
type_ptr(Type *type) {
    Type *cached_type = (Type *)map_get(&cached_ptr_types, type);
    if ( cached_type ) {
        return cached_type;
    }

    Type *result = type_new(TYPE_PTR);
    result->size = PTR_SIZE;
    result->align = PTR_ALIGN;
    result->type_ptr.base = type;
    map_put(&cached_ptr_types, type, result);

    return result;
}

internal_proc Type *
type_array(Type *base, size_t index, b32 is_inferred = false) {
    uint64_t hash = mix_hash(ptr_hash(base), uint64_hash(index));
    uint64_t key = hash ? hash : 1;

    Cached_Array_Type *cached_type = (Cached_Array_Type *)map_get(&cached_array_types, (void *)key);
    for ( Cached_Array_Type *it = cached_type; it; it = it->next ) {
        Type *type = cached_type->type;
        if ( type->type_array.base == base && type->type_array.index == index ) {
            return type;
        }
    }

    complete_type(base);

    Type *result = type_new(TYPE_ARRAY);

    result->size                   = index * base->size;
    result->align                  = base->align;
    result->type_array.base        = base;
    result->type_array.index       = index;
    result->type_array.is_inferred = is_inferred;

    Cached_Array_Type *t = (Cached_Array_Type *)xmalloc(sizeof(Cached_Array_Type));

    t->type = result;
    t->next = cached_type;

    map_put(&cached_array_types, (void *)key, t);

    return result;
}

internal_proc Type *
type_struct(Type_Field *fields, size_t num_fields) {
    Type *result = type_new(TYPE_STRUCT);

    result->type_aggr.fields = (Type_Field *)memdup(fields, num_fields*sizeof(Type_Field));
    result->type_aggr.num_fields = num_fields;
    result->type_aggr.scope = scope_enter();

    result->size = 0;
    for ( size_t i = 0; i < buf_len(fields); ++i ) {
        result->size += fields[i].type->size;
        sym_push_var(fields[i].name, fields[i].type);
    }

    scope_exit();

    return result;
}

internal_proc Type *
type_proc(Type **params, size_t num_params, Type **rets, size_t num_rets,
        b32 is_variadic, b32 is_foreign)
{
    for ( size_t i = 0; i < buf_len(cached_proc_types); ++i ) {
        if ( cached_proc_types[i].num_params == num_params &&
             cached_proc_types[i].num_rets == num_rets )
        {
            uint32_t match = 1;
            for ( size_t j = 0; j < num_params; ++j ) {
                if ( cached_proc_types[i].params[j] != params[j] ) {
                    match = 0;
                    break;
                }
            }

            for ( size_t j = 0; j < num_rets; ++j ) {
                if ( cached_proc_types[i].rets[j] != rets[j] ) {
                    match = 0;
                    break;
                }
            }

            if ( match ) {
                return cached_proc_types[i].type_proc;
            }
        }
    }

    Type *result = type_new(TYPE_PROC);

    result->size = PTR_SIZE;
    result->align = PTR_ALIGN;
    result->is_foreign = is_foreign;
    result->type_proc.params = (Type **)memdup(params, num_params*sizeof(Type *));
    result->type_proc.num_params = num_params;
    result->type_proc.rets   = (Type **)memdup(rets, num_rets*sizeof(Type *));
    result->type_proc.num_rets = num_rets;
    result->type_proc.is_variadic = is_variadic;

    Cached_Proc_Type t = { params, num_params, rets, num_rets, result };
    buf_push(cached_proc_types, t);

    return result;
}

internal_proc void
init_typeids() {
    register_typeid(type_void);
    register_typeid(type_bool);
    register_typeid(type_char);
    register_typeid(type_string);
    register_typeid(type_u8);
    register_typeid(type_u16);
    register_typeid(type_u32);
    register_typeid(type_u64);
    register_typeid(type_s8);
    register_typeid(type_s16);
    register_typeid(type_s32);
    register_typeid(type_s64);
    register_typeid(type_f32);
    register_typeid(type_f64);
}

internal_proc Operand
ptr_decay(Operand operand) {
    if ( operand.type->kind == TYPE_ARRAY ) {
        return operand_rvalue(type_ptr(operand.type->type_array.base));
    } else {
        return operand;
    }
}

internal_proc Type *
type_decay(Type *type) {
    if (type->kind == TYPE_ARRAY) {
        type = type_ptr(type->type_array.base);
    }
    return type;
}

internal_proc Operand
operand_decay(Operand operand) {
    operand.type = type_decay(operand.type);
    operand.is_lvalue = false;

    return operand;
}

internal_proc Operand
resolve_cond_expr(Expr *expr) {
    Operand result = resolve_expr(expr);
    if ( !is_scalar(result.type) ) {
        assert(!"ausdruck der beindingung muss vom typ integer sein!");
    }

    return result;
}

internal_proc Val
eval_int_unary(Token_Kind op, Val value) {
    assert(value.kind == VAL_INT);

    switch ( op ) {
        case T_PLUS: {
            return val_int(+value.s32);
        } break;

        case T_MINUS: {
            return val_int(-value.s32);
        } break;

        case T_NEGATE: {
            return val_int(~value.s32);
        } break;

        case T_NOT: {
            return val_int(!value.s32);
        } break;

        default: {
            assert(!"nicht unterstützter operator!");
            return val_null;
        }
    }
}

internal_proc Val
eval_float_unary(Token_Kind op, Val value) {
    assert(value.kind == VAL_F32);

    switch ( op ) {
        case T_PLUS: {
            return val_f32(+value.f32);
        } break;

        case T_MINUS: {
            return val_f32(-value.f32);
        } break;

        default: {
            assert(!"nicht unterstützter operator!");
            return val_null;
        } break;
    }
}

internal_proc Val
eval_binary_op(Token_Kind op, Type *type, Val left, Val right) {
    Operand operand_left = operand_const(type, left);
    Operand operand_right = operand_const(type, right);
    Operand result;

    if ( is_signed(type) ) {
        convert_operand(&operand_left, type_s64);
        convert_operand(&operand_right, type_s64);

        int64_t x = operand_left.value.s64;
        int64_t y = operand_right.value.s64;
        int64_t r = 0;

        switch (op) {
            case T_MUL: {
                r = x * y;
            } break;

            case T_DIV: {
                r = y != 0 ? x / y: 0;
            } break;

            case T_MODULO: {
                r = y != 0 ? x % y : 0;
            } break;

            case T_BIT_AND: {
                r = x & y;
            } break;

            case T_PLUS: {
                r = x + y;
            } break;

            case T_MINUS: {
                r = x - y;
            } break;

            case T_BIT_OR: {
                r = x | y;
            } break;

            case T_EQ: {
                r = x == y;
            } break;

            case T_NEQ: {
                r = x != y;
            } break;

            case T_LT: {
                r = x < y;
            } break;

            case T_LEQ: {
                r = x <= y;
            } break;

            case T_GT: {
                r = x > y;
            } break;

            case T_GEQ: {
                r = x >= y;
            } break;

            case T_AND: {
                r = x && y;
            } break;

            case T_OR: {
                r = x || y;
            } break;

            case T_LSHIFT: {
                r = x << y;
            } break;

            case T_RSHIFT: {
                r = x >> y;
            } break;

            default: {
                assert(0);
            } break;
        }

        result = operand_const(type_s64, val_s64(r));
    } else {
        convert_operand(&operand_left, type_u64);
        convert_operand(&operand_right, type_u64);

        uint64_t x = operand_left.value.u64;
        uint64_t y = operand_right.value.u64;
        uint64_t r = 0;

        switch (op) {
            case T_MUL: {
                r = x * y;
            } break;

            case T_DIV: {
                r = y != 0 ? x / y: 0;
            } break;

            case T_MODULO: {
                r = y != 0 ? x % y : 0;
            } break;

            case '&': {
                r = x & y;
            } break;

            case T_PLUS: {
                r = x + y;
            } break;

            case T_MINUS: {
                r = x - y;
            } break;

            case '|': {
                r = x | y;
            } break;

            case T_EQ: {
                r = x == y;
            } break;

            case T_NEQ: {
                r = x != y;
            } break;

            case T_LT: {
                r = x < y;
            } break;

            case T_LEQ: {
                r = x <= y;
            } break;

            case T_GT: {
                r = x > y;
            } break;

            case T_GEQ: {
                r = x >= y;
            } break;

            case T_AND: {
                r = x && y;
            } break;

            case T_OR: {
                r = x || y;
            } break;

            default: {
                assert(0);
            } break;
        }

        result = operand_const(type_u64, val_u64(r));
    }

    convert_operand(&result, type);
    return result.value;
}

internal_proc Val
eval_unary_op(Token_Kind op, Type *type, Val value) {
    Operand operand = operand_const(type, value);

    if ( is_signed(type) ) {
        convert_operand(&operand, type_s64);
        int64_t x = operand.value.s64;
        int64_t r;

        switch (op) {
            case T_PLUS: {
                r = +x;
            } break;

            case T_MINUS: {
                r = -x;
            } break;

            case T_NEGATE: {
                r = ~x;
            } break;

            case T_NOT: {
                r = !x;
            } break;

            default: {
                assert(0);
            } break;
        }

        operand.value.s64 = r;
    } else {
        convert_operand(&operand, type_u64);
        uint64_t x = operand.value.u64;
        uint64_t r;

        switch (op) {
            case T_PLUS: {
                r = +x;
            } break;

            case T_MINUS: {
                r = x;
            } break;

            case T_NEGATE: {
                r = ~x;
            } break;

            case T_NOT: {
                r = !x;
            } break;

            default: {
                assert(0);
            } break;
        }

        operand.value.s64 = r;
    }

    convert_operand(&operand, type);
    return operand.value;
}

internal_proc Sym *
resolve_name(char *name) {
    Sym *sym = sym_get(name);

    if ( sym ) {
        resolve_sym(sym);
    }

    return sym;
}

internal_proc Operand
resolve_module_sym(Sym *sym, char *name) {
    assert(sym->module);

    Module *prev_module = module_enter(sym->module);
    Sym *name_sym = resolve_name(name);
    module_leave(prev_module);

    return operand_lvalue(name_sym->type);
}

internal_proc Type *
resolve_typespec(Typespec *typespec) {
    if ( !typespec ) { return type_void; }

    Type *result = NULL;
    switch (typespec->kind) {
        case TYPESPEC_NAME: {
            Sym *sym = resolve_name(typespec->typespec_name.value);

            if ( !sym ) {
                error(get_site(typespec), "symbol '%s' konnte nicht gefunden werden!", typespec->typespec_name.value);
            }

            result = sym->type;
            result->sym = sym;
        } break;

        case TYPESPEC_PTR: {
            result = type_ptr(resolve_typespec(typespec->typespec_ptr.elem));
        } break;

        case TYPESPEC_ARRAY: {
            Val array_size = val_s32(0);

            b32 is_inferred = typespec->typespec_array.size == 0;
            if ( typespec->typespec_array.size ) {
                array_size = resolve_expr(typespec->typespec_array.size).value;
                assert(is_int(array_size));
            }

            result = type_array(resolve_typespec(typespec->typespec_array.elem), array_size.s32, is_inferred);
        } break;

        case TYPESPEC_PROC: {
            b32 is_variadic = false;
            Type **args = NULL;
            for ( size_t i = 0; i < typespec->typespec_proc.num_params; i++ ) {
                is_variadic = typespec->kind == TYPESPEC_VARIADIC || is_variadic;
                buf_push(args, resolve_typespec(typespec->typespec_proc.params[i]));
            }

            Type **rets = NULL;
            for ( size_t i = 0; i < typespec->typespec_proc.num_rets; i++) {
                buf_push(rets, resolve_typespec(typespec->typespec_proc.rets[i]));
            }

            result = type_proc(args, buf_len(args), rets, buf_len(rets), is_variadic, false);
        } break;

        case TYPESPEC_BLOCK: {
            Type_Field *fields = NULL;
            size_t offset = 0;
            for ( int i = 0; i < typespec->typespec_block.num_decls; ++i ) {
                Decl *decl = typespec->typespec_block.decls[i];
                Type *resolved_decl_type = resolve_decl_var(decl);

                Type_Field field = type_field(decl->name, resolved_decl_type);
                field.offset = offset;
                offset += resolved_decl_type->size;
                buf_push(fields, field);
            }

            result = type_struct(fields, buf_len(fields));
        } break;

        case TYPESPEC_VARIADIC: {
            result = type_variadic;
        } break;

        case TYPESPEC_SCOPE: {
            Expr *expr = typespec->typespec_scope.expr;
            Sym *sym = resolve_name(expr->expr_field.expr->expr_name.value);

            Operand operand = resolve_module_sym(sym, expr->expr_field.field);
            complete_type(operand.type);

            result = operand.type;
        } break;

        default: {
            assert(!"nicht unterstützter typespec!");
            return NULL;
        }
    }

    result->typespec = typespec;
    return result;
}

internal_proc Type *
resolve_decl_type(Decl *decl) {
    assert(decl->kind == DECL_TYPEDEF);
    return resolve_typespec(decl->decl_typedef.typespec);
}

internal_proc Type *
resolve_decl_var(Decl *decl) {
    assert(decl->kind == DECL_VAR);

    Type *type = NULL;
    if ( decl->decl_var.typespec ) {
        type = resolve_typespec(decl->decl_var.typespec);
    }

    if ( decl->decl_var.init ) {
        Operand operand_expr = resolve_expected_expr(decl->decl_var.init, type);

        if ( type ) {
            if ( !convert_operand(&operand_expr, type) ) {
                error(get_site(decl), "datentyp der zuweisung passt nicht mit dem datentypen der variable überein!");
            }
        }

        type = operand_expr.type;
    }

    complete_type(type);

    return type;
}

internal_proc Operand
resolve_const_expr(Expr *expr) {
    Operand result = resolve_expr(expr);
    if (!result.is_const) {
        error(get_site(expr), "eine konstante erwartet");
    }
    return result;
}

internal_proc Type *
resolve_decl_const(Decl *decl, Sym *sym = NULL) {
    assert(decl->kind == DECL_CONST);

    Operand operand = resolve_const_expr(decl->decl_const.expr);
    if ( !is_scalar(operand.type) ) {
        error(get_site(decl), "skalaren wert erwartet");
    }

    if ( sym ) {
        sym->value = operand.value;
    }

    return operand.type;
}

internal_proc Type *
resolve_decl_proc(Decl *decl) {
    assert(decl->kind == DECL_PROC);
    b32 is_variadic = false;

    Type **params = NULL;
    for ( size_t i = 0; i < decl->decl_proc.num_params; ++i ) {
        Typespec *typespec = decl->decl_proc.params[i].typespec;
        is_variadic = typespec->kind == TYPESPEC_VARIADIC || is_variadic;

        buf_push(params, resolve_typespec(decl->decl_proc.params[i].typespec));
    }

    Type **rets = NULL;
    if ( !decl->decl_proc.num_rets ) {
        buf_push(rets, type_void);
    } else {
        for ( size_t i = 0; i < decl->decl_proc.num_rets; ++i ) {
            buf_push(rets, resolve_typespec(decl->decl_proc.rets[i].typespec));
        }
    }

    return type_proc(params, buf_len(params), rets, buf_len(rets), is_variadic, decl->is_foreign);
}

internal_proc void
resolve_stmt_assign(Stmt *stmt) {
    assert(stmt->kind == STMT_ASSIGN);

    Operand left = resolve_expr(stmt->stmt_assign.left);
    Token_Kind op = stmt->stmt_assign.op;

    if ( !left.is_lvalue ) {
        error(get_site(stmt), "operand auf der linken seite muss ein l-value sein");
    }

    if ( is_array(left.type) ){
        error(get_site(stmt), "einem array kann kein wert zugewiesen werden!");
    }

    if ( stmt->stmt_assign.right ) {
        Operand right = resolve_expected_expr(stmt->stmt_assign.right, left.type);
        Operand result = {};

        if ( op == T_ADD_ASSIGN || op == T_SUB_ASSIGN ) {
            if ( is_ptr(left.type) && is_int(right.type)) {
                result = operand_rvalue(left.type);
            } else if (is_arithmetic(left.type) && is_arithmetic(right.type)) {
                result = resolve_binary_op(op, left, right);
            } else {
                error(get_site(stmt), "unzulässige operation");
            }
        } else if ( op == T_EQL_ASSIGN ) {
            result = right;
        } else {
            result = resolve_binary_op(op, left, right);
        }

        result = operand_decay(result);
        if (!convert_operand(&result, left.type)) {
            error(get_site(stmt), "illegale umwandlung in der zuweisung!");
        }
    } else {
        error(get_site(stmt), "die rechte seite einer zuweisung darf nicht leer sein!");
    }
}

internal_proc b32
resolve_stmt(Stmt *stmt, Type **rets, size_t num_rets) {
    switch (stmt->kind) {
        case STMT_RETURN: {
            if ( num_rets == 0 && stmt->stmt_return.num_exprs > 0 ) {
                error(get_site(stmt), "prozedur hat den rueckgabewert void!");
            }

            if ( stmt->stmt_return.num_exprs != num_rets ) {
                assert(!"anzahl der rückgabewerte entspricht nicht der deklarierten anzahl!");
            }

            for ( size_t i = 0; i < num_rets; ++i ) {
                Operand operand = resolve_expected_expr(stmt->stmt_return.exprs[i], rets[i]);
                Operand operand_return = operand_rvalue(rets[i]);

                convert_operand(&operand, operand_return.type);
                operand = operand_decay(operand);
                if ( operand.type != operand_return.type ) {
                    assert(!"der datentyp des rückgabewertes stimmt nicht mit dem deklarierten datentyp überein!");
                }
            }

            return true;
        } break;

        case STMT_FREE: {
            Operand operand = resolve_expr(stmt->stmt_free.expr);

            if ( !is_ptr(operand.type) && !is_array(operand.type) ) {
                error(get_site(stmt), "'free' kann nur auf ptr und arrays angewendet werden!");
            }

            return false;
        } break;

        case STMT_DECL: {
            Decl *decl = stmt->stmt_decl.decl;

            switch ( decl->kind ) {
                case DECL_VAR: {
                    Type *type = resolve_decl_var(decl);
                    complete_type(type);

                    /* @TODO(serge): überlegen an welcher stelle die standardwerte, die im struct *
                     *               angegeben wurden, gesetzt werden können!                     */
                    /* @INFO(serge): wahrscheinlich am besten im gen                              */
                    char *name = decl->name;
                    if ( !sym_push_var(name, type, decl) ) {
                        error(get_site(decl), "wiederholte deklaration der variable '%s'. mögliche überschattung.", name);
                    }
                } break;

                case DECL_CONST: {
                    Operand const_operand = resolve_expr(decl->decl_const.expr);
                    Type *type = resolve_decl_const(decl);
                    complete_type(type);

                    char *name = decl->name;
                    if ( !sym_push_const(name, type, decl, const_operand.value) ) {
                        error(get_site(decl), "wiederholte deklaration der variable '%s'. mögliche überschattung.", name);
                    }
                } break;

                default: {
                    error(get_site(decl), "deklarationen werden aktuell nur für variablen und konstanten unterstützt!");
                } break;
            }

            return false;
        } break;

        case STMT_BREAK: {
            return false;
        } break;

        case STMT_CONTINUE: {
            return false;
        }

        case STMT_BLOCK: {
            return resolve_stmt_block(stmt->stmt_block.block, rets, num_rets);
        } break;

        case STMT_IF: {
            resolve_cond_expr(stmt->stmt_if.if_expr);
            b32 result = resolve_stmt_block(stmt->stmt_if.then_block, rets, num_rets);

            for ( size_t i = 0; i < stmt->stmt_if.num_else_ifs; ++i ) {
                resolve_cond_expr(stmt->stmt_if.else_ifs[i].condition);
                result = resolve_stmt_block(stmt->stmt_if.else_ifs[i].block, rets, num_rets) && result;
            }

            if ( stmt->stmt_if.else_block.num_stmts ) {
                result = resolve_stmt_block(stmt->stmt_if.else_block, rets, num_rets) && result;
            } else {
                result = false;
            }

            return result;
        } break;

        case STMT_WHILE: {
            resolve_cond_expr(stmt->stmt_while.expr);
            resolve_stmt_block(stmt->stmt_while.block, rets, num_rets);
            return false;
        } break;

        case STMT_FOR: {
            scope_enter();

            if ( stmt->stmt_for.iterator ) {
                sym_push_var(stmt->stmt_for.iterator->expr_name.value, type_s32);
            }

            Operand cond_operand = resolve_expr(stmt->stmt_for.condition);
            if ( !is_int(cond_operand.type) && !is_array(cond_operand.type) && !is_string(cond_operand.type) ) {
                error(get_site(stmt), "ausdruck der bedingung muss vom iterierbaren typ sein!");
            }

            if ( !stmt->stmt_for.iterator ) {
                /* @TODO(serge): it als symbol pushen, aber vorher prüfen ob das symbol bereits vorhanden ist */
                char *it = intern_str("it");
                assert(sym_get(it) == NULL);

                Type *iterator_type = NULL;
                if ( is_int(cond_operand.type) ) {
                    iterator_type = cond_operand.type;
                } else if ( is_array(cond_operand.type) ) {
                    if ( cond_operand.type->type_array.is_inferred ) {
                        error(get_site(stmt), "die größe des arrays muß statisch sein!");
                    }
                    iterator_type = type_s64;
                } else if ( is_string(cond_operand.type) ) {
                    iterator_type = type_char;
                }

                sym_push_var(it, iterator_type);
                stmt->stmt_for.iterator = expr_name(get_site(stmt), it);
            }

            resolve_stmt_block(stmt->stmt_for.block, rets, num_rets);
            scope_exit();

            return false;
        } break;

        case STMT_SWITCH: {
            Operand switch_operand = resolve_expr(stmt->stmt_switch.expr);
            b32 result = true;
            b32 has_default = false;

            for ( size_t i = 0; i < stmt->stmt_switch.num_switch_cases; ++i ) {
                Switch_Case switch_case = stmt->stmt_switch.switch_cases[i];

                for ( size_t j = 0; j < switch_case.num_exprs; ++j ) {
                    Operand case_result = resolve_expr(switch_case.exprs[j]);
                    convert_operand(&case_result, switch_operand.type);

                    if ( case_result.type != switch_operand.type ) {
                        assert(!"datentyp der case bedingung stimmt mit dem datentypen der switch anweisung nicht überein!");
                    }
                }
                result = resolve_stmt_block(switch_case.block, rets, num_rets) && result;

                if ( switch_case.is_default ) {
                    if ( has_default ) {
                        error(switch_case.site, "nur ein 'default' pro switch erlaubt!");
                    }

                    has_default = true;
                }
            }

            return result && has_default;
        } break;

        case STMT_ASSIGN: {
            resolve_stmt_assign(stmt);

            return false;
        } break;

        case STMT_EXPR: {
            resolve_expr(stmt->stmt_expr.expr);

            return false;
        } break;

        case STMT_USING: {
            Operand operand = resolve_expr(stmt->stmt_using.expr);

            complete_type(operand.type);
            assert(is_aggregate(operand.type));

            for ( int i = 0; i < operand.type->type_aggr.num_fields; ++i ) {
                Type_Field field = operand.type->type_aggr.fields[i];

                if ( operand.type->kind == TYPE_STRUCT ) {
                    sym_push_var(field.name, field.type);
                } else {
                    sym_push_const(field.name, field.type, 0, field.value);
                }
            }

            return false;
        } break;

        case STMT_DEFER: {
            return resolve_stmt(stmt->stmt_defer.stmt, rets, num_rets);
        } break;

        default: {
            assert(!"nicht unterstütztes statement!");

            return false;
        } break;
    }
}

internal_proc b32
resolve_stmt_block(Stmt_Block block, Type **rets, size_t num_rets) {
    scope_enter();

    b32 result = false;
    for ( size_t i = 0; i < block.num_stmts; ++i ) {
        result = result || resolve_stmt(block.stmts[i], rets, num_rets);
    }

    scope_exit();

    return result;
}

internal_proc void
resolve_proc(Sym *sym) {
    Decl *decl = sym->decl;

    assert(decl->kind == DECL_PROC);
    assert(sym->state == SYM_RESOLVED);

    scope_enter(decl->name);

    for ( size_t i = 0; i < decl->decl_proc.num_params; ++i ) {
        Proc_Param param = decl->decl_proc.params[i];

        if ( param.flags & PF_USING ) {
            Type *type = resolve_typespec(param.typespec);

            assert(type);
            if ( type->kind != TYPE_STRUCT && type->kind != TYPE_ENUM ) {
                report_error("'using' kann nur auf struct oder enum ausgeführt werden!");
            }

            for ( int j = 0; j < type->type_aggr.num_fields; ++j ) {
                Type_Field field = type->type_aggr.fields[j];
                sym_push_var(field.name, field.type);
            }
        } else {
            sym_push_var(param.name, resolve_typespec(param.typespec));
        }
    }

    Type **rets = 0;
    for ( size_t i = 0; i < decl->decl_proc.num_rets; ++i ) {
        buf_push(rets, resolve_typespec(decl->decl_proc.rets[i].typespec));
    }

    b32 returns = resolve_stmt_block(decl->decl_proc.block, rets, buf_len(rets));

    scope_exit();

    if ( decl->decl_proc.num_rets && !returns && !decl->is_incomplete ) {
        error(get_site(decl), "nicht alle zweige liefern einen wert zurueck!");
    }
}

internal_proc void
resolve_sym(Sym *sym) {
    if ( sym->state == SYM_RESOLVED ) {
        return;
    }

    if ( sym->state == SYM_RESOLVING ) {
        assert(!"schleife in der auflösung!");
        return;
    }

    assert( sym->state == SYM_UNRESOLVED );
    sym->state = SYM_RESOLVING;
    Decl *decl = sym->decl;

    switch ( sym->kind ) {
        case SYM_TYPE: {
            sym->type = resolve_decl_type(decl);
        } break;

        case SYM_VAR: {
            sym->type = resolve_decl_var(decl);
        } break;

        case SYM_CONST: {
            sym->type = resolve_decl_const(decl, sym);
        } break;

        case SYM_PROC: {
            sym->type = resolve_decl_proc(decl);
        } break;

        default: {
            assert(!"nicht unterstütztes symbol!");
        }
    }

    sym->state = SYM_RESOLVED;

    if ( decl->kind != DECL_UNION && decl->kind != DECL_STRUCT && decl->kind != DECL_ENUM ) {
        buf_push(ordered_symbols, sym);
    }
}

internal_proc Operand
operand_from_sym(Sym *sym) {
    Operand result = operand_null;

    if (sym->kind == SYM_VAR) {
        result = operand_lvalue(sym->type);
        if ( sym->type->kind == TYPE_ARRAY ) {
            result = operand_decay(result);
        }
    } else if (sym->kind == SYM_CONST) {
        result = operand_const(sym->type, sym->value);
    } else if (sym->kind == SYM_PROC) {
        result = operand_rvalue(sym->type);
    } else if (sym->kind == SYM_TYPE) {
        result = operand_rvalue(sym->type);
    } else if (sym->kind == SYM_MODULE) {
        result = operand_module(sym->module);
    } else {
        assert(0);
    }

    return result;
}

internal_proc Operand
resolve_expr_name(Expr *expr) {
    assert(expr->kind == EXPR_NAME);

    Sym *sym = resolve_name(expr->expr_name.value);

    if ( !sym ) {
        error(get_site(expr), "symbol '%s' nicht gefunden!", expr->expr_name.value);
    }

    return operand_from_sym(sym);
}

internal_proc char *
get_index_name(Expr *expr) {
    assert(expr->kind == EXPR_INDEX);

    if ( expr->expr_index.expr->kind == EXPR_NAME ) {
        return expr->expr_index.expr->expr_name.value;
    } else {
        return get_index_name(expr->expr_index.expr);
    }

    assert(0);
    return "";
}

internal_proc Type *
reduce_type(Type *type) {
    switch ( type->kind ) {
        case TYPE_PTR: {
            return reduce_type(type->type_ptr.base);
        } break;

        case TYPE_ARRAY: {
            return reduce_type(type->type_array.base);
        } break;

        default: {
            return type;
        } break;
    }
}

internal_proc Operand
resolve_expr_field(Expr *expr) {
    assert( expr->kind == EXPR_FIELD );

    Operand left = resolve_expr(expr->expr_field.expr);
    Operand result = operand_null;

    if ( left.is_module ) {
        Module *prev_module = module_enter(left.module);
        Sym *sym = resolve_name(expr->expr_field.field);
        assert(sym);
        result = operand_from_sym(sym);
        module_leave(prev_module);
    } else {
        Type *type = reduce_type(left.type);
        complete_type(type);
        assert(is_aggregate(type));

        Scope *prev = scope_set(type->type_aggr.scope);
        Sym *sym = resolve_name(expr->expr_field.field);
        assert(sym);
        result = operand_from_sym(sym);
        scope_set(prev);
    }

    return result;
}

internal_proc Operand
resolve_expr_unary(Expr *expr, Type *expected_type) {
    assert( expr->kind == EXPR_UNARY );

    Operand operand = {};
    if ( expected_type && expected_type->kind == TYPE_PTR ) {
        operand = resolve_expected_expr(expr->expr_unary.expr, expected_type->type_ptr.base);
    } else {
        operand = resolve_expr(expr->expr_unary.expr);
    }

    Type *type = operand.type;

    switch ( expr->expr_unary.op ) {
        case T_DEREF: {
            operand = operand_decay(operand);
            type = operand.type;

            if ( type->kind != TYPE_PTR ) {
                error(get_site(expr), "kann keinen nicht-pointer dereferenzieren!");
            }

            return operand_lvalue(type->type_ptr.base);
        } break;

        case T_NOT: {
            if ( !is_scalar(type) ) {
                error(get_site(expr), "'!' operator kann nur auf skalare datentypen angewendet werden!");
            }

            promote_operand(&operand);
            if (operand.is_const) {
                return operand_const(operand.type, eval_unary_op(expr->expr_unary.op, operand.type, operand.value));
            } else {
                return operand;
            }
        } break;

        case T_POINTER: {
            if ( !operand.is_lvalue ) {
                error(get_site(expr), "operand muss zuweisbar (lvalue) sein!");
            }
            return operand_rvalue(type_ptr(type));
        } break;

        default: {
            if ( !is_int(type) ) {
                error(get_site(expr), "unaere operatoren koennen nur auf integer angewendet werden!");
            }

            if ( operand.is_const ) {
                return operand_const(type, eval_unary_op(expr->expr_unary.op, operand.type, operand.value));
            } else {
                return operand_rvalue(type);
            }
        }
    }
}

internal_proc Operand
resolve_binary_op(Token_Kind op, Operand left, Operand right) {
    if (left.is_const && right.is_const) {
        return operand_const(left.type, eval_binary_op(op, left.type, left.value, right.value));
    } else {
        return operand_rvalue(left.type);
    }
}

internal_proc Operand
resolve_binary_arithmetic_op(Token_Kind op, Operand left, Operand right) {
    unify_arithmetic_operands(&left, &right);

    return resolve_binary_op(op, left, right);
}

internal_proc Operand
resolve_expr_binary(Expr *expr) {
    assert(expr->kind == EXPR_BINARY);

    Operand left  = resolve_expr_rvalue(expr->expr_binary.left);
    Operand right = resolve_expr_rvalue(expr->expr_binary.right);
    Token_Kind op = expr->expr_binary.op;

    switch (op) {
        case T_MINUS:
        case T_PLUS: {
            if (is_arithmetic(left.type) && is_arithmetic(right.type)) {
                unify_arithmetic_operands(&left, &right);
                return resolve_binary_op(op, left, right);
            } else if (is_ptr(left.type) && is_int(right.type)) {
                return operand_rvalue(left.type);
            } else if (is_ptr(right.type) && is_int(left.type)) {
                return operand_rvalue(right.type);
            } else if (is_ptr(left.type) && is_ptr(right.type)) {
                if (left.type->type_ptr.base != right.type->type_ptr.base) {
                    error(get_site(expr), "zeiger müssen auf den gleichen datentyp zeigen!");
                }
                return operand_rvalue(type_s32);
            } else {
                error(get_site(expr), "operanden müssen entweder beide integer oder pointer sein!");
                return operand_null;
            }
        } break;

        case T_LT:
        case T_LEQ:
        case T_GT:
        case T_GEQ:
        case T_EQ:
        case T_NEQ: {
            if (is_arithmetic(left.type) && is_arithmetic(right.type)) {
                Operand result = resolve_binary_arithmetic_op(op, left, right);
                cast_operand(&result, type_s32);

                return result;
            } else if (is_ptr(left.type) && is_ptr(right.type)) {
                if (left.type->type_ptr.base != right.type->type_ptr.base) {
                    error(get_site(expr), "pointer unterschiedlicher datentypen können nicht verglichen werden");
                }

                return operand_rvalue(type_s32);
            } else if ((is_null(left) && is_ptr(right.type)) || (is_null(right) && is_ptr(left.type))) {
                return operand_rvalue(type_s32);
            } else {
                error(get_site(expr), "operanden müssen entweder arithmetisch oder vom kompatiblen zeigertypen sein!");
                return operand_null;
            }
        } break;

        case T_AND:
        case T_XOR:
        case T_OR: {
            if (is_int(left.type) && is_int(right.type)) {
                return resolve_binary_arithmetic_op(op, left, right);
            } else {
                error(get_site(expr), "operanden müssen arithmetischen typ haben");
                return operand_null;
            }
        } break;

        default: {
            unify_arithmetic_operands(&left, &right);

            if ( left.is_const && right.is_const && expr->expr_binary.op != T_RANGE ) {
                return operand_const(left.type, eval_binary_op(expr->expr_binary.op, left.type, left.value, right.value));
            } else {
                return operand_rvalue(left.type);
            }
        } break;
    }
}

internal_proc Operand
resolve_expr_rvalue(Expr *expr) {
    return operand_decay(resolve_expr(expr));
}

internal_proc Operand
resolve_expr_ternary(Expr *expr, Type *expected_type) {
    assert(expr->kind == EXPR_TERNARY);

    Operand cond = resolve_expr_rvalue(expr->expr_ternary.expr);
    if (!is_scalar(cond.type)) {
        assert(!"bedingung muss skalar sein!");
    }

    Operand left = ptr_decay(resolve_expected_expr(expr->expr_ternary.then_expr, expected_type));
    Operand right = ptr_decay(resolve_expected_expr(expr->expr_ternary.else_expr, expected_type));

    if ( is_arithmetic(left.type) && is_arithmetic(right.type) ) {
        unify_arithmetic_operands(&left, &right);
        if (cond.is_const && left.is_const && right.is_const) {
            return operand_const(cond.type, cond.value.s32 ? left.value : right.value);
        } else {
            return operand_rvalue(left.type);
        }
    } else if ( is_ptr(left.type) && is_null(right) ) {
        return operand_rvalue(left.type);
    } else if ( is_null(left) && is_ptr(right.type) ) {
        return operand_rvalue(right.type);
    } else if ( left.type == right.type ) {
        return operand_rvalue(left.type);
    } else {
        error(get_site(expr), "die 'dann' und 'ansonsten' ausdrücke müssen gleichen typ haben!");
        return operand_null;
    }
}

internal_proc Operand
resolve_expr_index(Expr *expr) {
    assert(expr->kind == EXPR_INDEX);

    Operand operand = resolve_expr_rvalue(expr->expr_index.expr);
    if ( !is_ptr(operand.type) ) {
        assert(0);
    }

    Operand index = resolve_expr_rvalue(expr->expr_index.index);
    if ( !is_int(index.type) ) {
        error(expr->site, "index ausdruck muss vom datentyp integer sein!");
    }

    return operand_lvalue(operand.type->type_ptr.base);
}

internal_proc Operand
resolve_expr_cast(Expr *expr) {
    assert(expr->kind == EXPR_CAST);

    Type *type = resolve_typespec(expr->expr_cast.typespec);
    Operand result = resolve_expr_rvalue(expr->expr_cast.expr);

    if ( is_ptr(type) ) {
        if ( !is_int(result.type) && !is_ptr(type) ) {
            assert(!"unerlaubter cast!");
        }
    } else if ( is_int(type) ) {
        if ( !is_int(type) && !is_ptr(result.type) ) {
            assert(0);
        }
    } else {
        assert(0);
    }

    return operand_rvalue(type);
}

internal_proc Operand
resolve_expr_call(Expr *expr) {
    assert(expr->kind == EXPR_CALL);

    Operand proc = resolve_expr(expr->expr_call.expr);
    complete_type(proc.type);

    if ( !is_proc(proc.type) ) {
        assert(!"aufruf ausdruck auf einem nicht prozedurtyp!");
    }

    /*
       @INFO: in der if vergleichen wir mit proc.type->type_proc.num_params - 1 ... - 1, weil der variadic parameter
              auch leer sein darf
     */
    if ( is_variadic(proc.type) && ( expr->expr_call.num_args < ( proc.type->type_proc.num_params - 1 ) ) ) {
        error(get_site(expr), "zu wenig übergebener parameter an die variadic prozedur!");
    } else if ( !is_variadic(proc.type) && ( expr->expr_call.num_args != proc.type->type_proc.num_params ) ) {
        error(get_site(expr), "anzahl parameter stimmt nicht überein!");
    }

    for ( size_t i = 0; i < expr->expr_call.num_args; ++i ) {
        /*
           @TODO: darauf achten, dass der call mit benamten argumenten ausgeführt werden kann!
         */
        Type *param_type = NULL;
        if ( is_variadic(proc.type) && i >= proc.type->type_proc.num_params ) {
            param_type = proc.type->type_proc.params[proc.type->type_proc.num_params-1];
        } else {
            param_type = proc.type->type_proc.params[i];
        }

        Expr *operand_expr = expr->expr_call.args[i].expr;
        Operand arg = resolve_expected_expr(operand_expr, param_type);
        arg = operand_decay(arg);

        if ( !convert_operand(&arg, param_type) && param_type->kind != TYPE_VARIADIC ) {
            error(get_site(expr), "übergebener parameter stimmt nicht mit dem erwarteten datentyp überein!");
        }
    }

    /* @TODO: was tun bei mehreren return typen?! */
    return operand_rvalue(proc.type->type_proc.rets[0]);
}

internal_proc Operand
resolve_expr_sizeof(Expr *expr) {
    assert(expr->kind == EXPR_SIZEOF);
    Type *type = resolve_typespec(expr->expr_sizeof.typespec);
    complete_type(type);

    return operand_const(type_u64, val_u64(type->size));
}

internal_proc Type_Field *
typefield_by_name(char *name, Type *type) {
    if ( !name ) return 0;

    assert(type->kind == TYPE_STRUCT);

    for ( size_t i = 0; i < type->type_aggr.num_fields; ++i ) {
        if ( type->type_aggr.fields[i].name == name ) {
            return &type->type_aggr.fields[i];
        }
    }

    return 0;
}

internal_proc Operand
resolve_expr_compound(Expr *expr, Type *expected_type) {
    assert(expr->kind == EXPR_COMPOUND);

    if ( !expected_type && !expr->expr_compound.typespec ) {
        assert(0);
    }

    Type *type = NULL;
    if ( expr->expr_compound.typespec ) {
        type = resolve_typespec(expr->expr_compound.typespec);

        if ( expected_type && expected_type != type ) {
            assert(0);
        }
    } else {
        type = expected_type;
    }

    complete_type(type);

    if ( type->kind == TYPE_STRUCT ) {
        if ( expr->expr_compound.num_args > type->type_aggr.num_fields ) {
            error(get_site(expr), "der übergebene ausdruck enthält zu viele felder!");
        }

        for ( size_t i = 0; i < expr->expr_compound.num_args; ++i ) {
            Compound_Argument comp_arg = expr->expr_compound.args[i];

            Type_Field *type_field = 0;
            if ( expr->expr_compound.kind == EXPR_COMP_NAME ) {
                if ( !comp_arg.name ) {
                    error(get_site(comp_arg), "bei benamten compounds, müssen alle felder benamt werden!");
                }

                type_field = typefield_by_name(comp_arg.name, type);

                if ( !type_field ) {
                    error(get_site(comp_arg), "es konnte kein passendes feld gefunden werden!");
                }
            } else {
                type_field = &type->type_aggr.fields[i];
            }

            Operand operand_elem = resolve_expected_expr(comp_arg.expr, type_field->type);
            convert_operand(&operand_elem, type_field->type);

            if ( operand_elem.type != type_field->type ) {
                error(get_site(expr), "datentypen stimmen nicht überein");
            }
        }
    } else {
        assert(type->kind == TYPE_ARRAY);

        if ( expr->expr_compound.num_args > type->type_array.index && !type->type_array.is_inferred ) {
            error(get_site(expr), "zu viele argumente übergeben!");
        }

        int64_t max_index = 0;
        if ( type->type_array.is_inferred ) {
            max_index = expr->expr_compound.num_args;
        }

        for ( size_t i = 0; i < expr->expr_compound.num_args; ++i ) {
            Compound_Argument comp_arg = expr->expr_compound.args[i];
            Operand elem = resolve_expected_expr(comp_arg.expr, type->type_array.base);

            if ( comp_arg.kind != COMPOUND_INDEX && comp_arg.kind != COMPOUND_LIST ) {
                error(get_site(comp_arg), "arrays haben keine benamten felder!");
            }

            if ( comp_arg.kind == COMPOUND_INDEX ) {
                Operand operand_index = resolve_expr(comp_arg.index);

                if ( !operand_index.is_const ) {
                    error(get_site(comp_arg), "index angabe muss eine konstante sein!");
                }

                convert_operand(&operand_index, type_s64);
                if (max_index < operand_index.value.s64) {
                    max_index = operand_index.value.s64;
                }
            }

            convert_operand(&elem, type->type_array.base);
            if ( elem.type != type->type_array.base ) {
                error(get_site(comp_arg), "datentypen stimmen nicht überein");
            }
        }
        type->type_array.index = max_index;
    }

    return operand_lvalue(type);
}

internal_proc Operand
resolve_expr_typeof(Expr *expr) {
    assert(expr->kind == EXPR_TYPEOF);

    Type *type = resolve_typespec(expr->expr_typeof.typespec);
    complete_type(type);

    return operand_const(type_s64, val_s64(type->type_id));
}

internal_proc Operand
resolve_expr_typeinfo(Expr *expr) {
    assert(expr->kind == EXPR_TYPEINFO);

    Operand operand = resolve_expr(expr->expr_typeinfo.expr);
    Sym *typeid_sym = resolve_name(intern_str("typeid"));
    assert(typeid_sym);

    convert_operand(&operand, typeid_sym->type);

    if ( operand.type != typeid_sym->type ) {
        error(get_site(expr), "der operand muss eine typeid sein!");
    }

    Sym *sym = resolve_name(intern_str("Type_Info"));
    assert(sym);

    return operand_rvalue(type_ptr(sym->type));
}

internal_proc Operand
resolve_expr_alignof(Expr *expr) {
    assert(expr->kind == EXPR_ALIGNOF);

    Type *type = resolve_typespec(expr->expr_alignof.typespec);
    complete_type(type);

    return operand_const(type_s64, val_s64(type_alignof(type)));
}

internal_proc Operand
resolve_expr_offsetof(Expr *expr) {
    assert(expr->kind == EXPR_OFFSETOF);

    Type *type = resolve_typespec(expr->expr_offsetof.typespec);
    assert(type);
    complete_type(type);
    assert(type->kind == TYPE_STRUCT);

    b32 found = false;
    size_t offset = 0;
    for ( size_t i = 0; i < type->type_aggr.num_fields; ++i ) {
        Type_Field field = type->type_aggr.fields[i];

        if ( field.name == expr->expr_offsetof.expr->expr_name.value ) {
            offset = field.offset;
            found = true;
            break;
        }
    }

    if ( !found ) {
        assert(0);
        /*
        error(get_site(expr), "der datentyp '%s' enthällt kein feld '%s'",
                expr->expr_offsetof.type->expr_name.value,
                expr->expr_offsetof.field->expr_name.value);
        */
    }

    return operand_const(type_s64, val_s64(offset));
}

internal_proc Operand
resolve_expected_expr(Expr *expr, Type *expected_type) {
    Operand result = operand_null;

    switch ( expr->kind ) {
        case EXPR_INT: {
            result = operand_const(type_s32, val_int(expr->expr_int.value));
        } break;

        case EXPR_CHAR: {
            result = operand_const(type_char, val_int(expr->expr_char.value));
        } break;

        case EXPR_FLOAT: {
            result = operand_rvalue(type_f32);
        } break;

        case EXPR_STR: {
            result = operand_rvalue(type_ptr(type_char));
        } break;

        case EXPR_NAME: {
            result = resolve_expr_name(expr);
        } break;

        case EXPR_PAREN: {
            result = resolve_expr(expr->expr_paren.expr);
        } break;

        case EXPR_FIELD: {
            result = resolve_expr_field(expr);
        } break;

        case EXPR_INDEX: {
            result = resolve_expr_index(expr);
        } break;

        case EXPR_UNARY: {
            result = resolve_expr_unary(expr, expected_type);
        } break;

        case EXPR_BINARY: {
            result = resolve_expr_binary(expr);
        } break;

        case EXPR_TERNARY: {
            result = resolve_expr_ternary(expr, expected_type);
        } break;

        case EXPR_COMPOUND: {
            result = resolve_expr_compound(expr, expected_type);
        } break;

        case EXPR_CAST: {
            result = resolve_expr_cast(expr);
        } break;

        case EXPR_CALL: {
            result = resolve_expr_call(expr);
        } break;

        case EXPR_SIZEOF: {
            result = resolve_expr_sizeof(expr);
        } break;

        case EXPR_TYPEOF: {
            result = resolve_expr_typeof(expr);
        } break;

        case EXPR_ALIGNOF: {
            result = resolve_expr_alignof(expr);
        } break;

        case EXPR_OFFSETOF: {
            result = resolve_expr_offsetof(expr);
        } break;

        case EXPR_TYPEINFO: {
            result = resolve_expr_typeinfo(expr);
        } break;

        default: {
            assert(0);
        }
    }

    return result;
}

internal_proc Operand
resolve_expr(Expr *expr) {
    return resolve_expected_expr(expr, NULL);
}

internal_proc void
resolve_decls() {
    for ( int i = 0; i < buf_len(current_module->syms); ++i ) {
        Sym *sym = current_module->syms[i];
        if ( sym ) {
            resolve_name(sym->name);
        }
    }
}

internal_proc void
init_global_syms() {
    sym_install_intrinsic(intern_str("u8"),  type_u8);
    sym_install_intrinsic(intern_str("u16"), type_u16);
    sym_install_intrinsic(intern_str("u32"), type_u32);
    sym_install_intrinsic(intern_str("u64"), type_u64);

    sym_install_intrinsic(intern_str("s8"),  type_s8);
    sym_install_intrinsic(intern_str("s16"), type_s16);
    sym_install_intrinsic(intern_str("s32"), type_s32);
    sym_install_intrinsic(intern_str("s64"), type_s64);

    sym_install_intrinsic(intern_str("f32"), type_f32);
    sym_install_intrinsic(intern_str("f64"), type_f64);

    sym_install_intrinsic(intern_str("string"), type_ptr(type_char));
    sym_install_intrinsic(intern_str("char"),   type_char);
    sym_install_intrinsic(intern_str("void"),   type_void);
    sym_install_intrinsic(intern_str("bool"),   type_u8);
}

internal_proc void
init_builtin_syms() {
    sym_module_decls(module_load(os_env("SSS_PATH"), module_descriptor("builtins.core")));
    Sym *sym_any = resolve_name(intern_str("Any"));
    assert(sym_any);
    type_any = sym_any->type;
}

internal_proc void
modules_inject() {
    for ( int i = 0; i < buf_len(modules); ++i ) {
        Module *module = modules[i];
        sym_module_decls(module);
    }
}

