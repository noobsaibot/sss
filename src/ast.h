struct Decl;
struct Expr;
struct Stmt;
struct Proc_Param;
struct Type;
struct Sym;

struct Program {
    Decl **decls;
    size_t num_decls;
};

struct Note {
    Site site;
    char *tag;
};

enum Typespec_Kind {
    TYPESPEC_NONE,
    TYPESPEC_NAME,
    TYPESPEC_PROC,
    TYPESPEC_PTR,
    TYPESPEC_ARRAY,
    TYPESPEC_BLOCK,
    TYPESPEC_VARIADIC,
    TYPESPEC_SCOPE,
};

struct Typespec {
    Typespec_Kind kind;
    Site site;

    union {
        struct {
            char *value;
        } typespec_name;
        struct {
            Expr *expr;
        } typespec_scope;
        struct {
            Typespec **params;
            size_t num_params;
            Typespec **rets;
            size_t num_rets;
        } typespec_proc;
        struct {
            Typespec *elem;
            Expr *size;
        } typespec_array;
        struct {
            Typespec *elem;
        } typespec_ptr;
        struct {
            Decl **decls;
            size_t num_decls;
        } typespec_block;
    };
};

enum Param_Flags {
    PF_NONE,
    PF_USING = 1,
};
struct Proc_Param {
    uint32_t flags;
    Expr *default_value;
    char *name;
    Typespec *typespec;
};

struct Aggregate_Field {
    Site site;
    uint32_t flags;
    char *name;
    Typespec *typespec;
    Expr *expr;
};

struct Stmt_Block {
    Stmt **stmts;
    size_t num_stmts;
};

enum Decl_Kind {
    DECL_NONE,
    DECL_VAR,
    DECL_CONST,
    DECL_STRUCT,
    DECL_UNION,
    DECL_ENUM,
    DECL_TYPEDEF,
    DECL_PROC,
};

struct Decl {
    Decl_Kind kind;

    char*  name;
    char*  foreign_name;
    Site   site;
    Note*  notes;
    size_t num_notes;
    Sym*   sym;

    b32 is_incomplete;
    b32 is_foreign;

    union {
        struct {
            Typespec *typespec;
            Expr *init;
        } decl_var;
        struct {
            Typespec *typespec;
            Expr *expr;
        } decl_const;
        struct {
            Typespec *typespec;
            Aggregate_Field *fields;
            size_t num_fields;
        } decl_aggr;
        struct {
            Typespec *typespec;
        } decl_typedef;
        struct {
            Proc_Param *params;
            size_t num_params;
            Proc_Param *rets;
            size_t num_rets;
            Stmt_Block block;
        } decl_proc;
    };
};

enum Comp_Arg_Kind {
    COMPOUND_LIST,
    COMPOUND_NAME,
    COMPOUND_INDEX,
};

struct Compound_Argument {
    Site site;
    Comp_Arg_Kind kind;
    char *name;
    Expr *index;
    Expr *expr;
};

struct Call_Arg {
    Expr *name;
    Expr *expr;
};

enum Expr_Comp_Kind {
    EXPR_COMP_NONE,
    EXPR_COMP_LIST,
    EXPR_COMP_NAME,
    EXPR_COMP_INDEX,
};

enum Expr_Kind {
    EXPR_NONE,
    EXPR_ILLEGAL,
    EXPR_PAREN,
    EXPR_INT,
    EXPR_FLOAT,
    EXPR_STR,
    EXPR_CHAR,
    EXPR_NAME,
    EXPR_INDEX,
    EXPR_FIELD,
    EXPR_UNARY,
    EXPR_BINARY,
    EXPR_TERNARY,
    EXPR_CALL,
    EXPR_COMPOUND,
    EXPR_CAST,
    EXPR_SIZEOF,
    EXPR_TYPEOF,
    EXPR_TYPEINFO,
    EXPR_ALIGNOF,
    EXPR_OFFSETOF,
};

struct Expr {
    Expr_Kind kind;
    Site site;

    union {
        struct {
            char *value;
        } expr_name;
        struct {
            uint8_t  base;
            uint64_t value;
        } expr_int;
        struct {
            uint64_t value;
        } expr_char;
        struct {
            double value;
        } expr_float;
        struct {
            char* value;
        } expr_str;
        struct {
            Expr *expr;
        } expr_paren;
        struct {
            Token_Kind op;
            Expr* expr;
        } expr_unary;
        struct {
            Token_Kind op;
            Expr* left;
            Expr* right;
        } expr_binary;
        struct {
            Expr *expr;
            Expr *then_expr;
            Expr *else_expr;
        } expr_ternary;
        struct {
            Expr *expr;
            Call_Arg *args;
            size_t num_args;
        } expr_call;
        struct {
            Expr *expr;
            Expr *index;
        } expr_index;
        struct {
            Expr *expr;
            char *field;
        } expr_field;
        struct {
            Expr_Comp_Kind kind;
            Typespec *typespec;
            Compound_Argument *args;
            size_t num_args;
        } expr_compound;
        struct {
            Typespec *typespec;
            Expr *expr;
        } expr_cast;
        struct {
            Typespec *typespec;
        } expr_sizeof;
        struct {
            Typespec *typespec;
        } expr_typeof;
        struct {
            Expr *expr;
        } expr_typeinfo;
        struct {
            Typespec *typespec;
        } expr_alignof;
        struct {
            Typespec *typespec;
            Expr *expr;
        } expr_offsetof;
    };
};

struct Switch_Case {
    Site site;
    Expr **exprs;
    size_t num_exprs;
    uint32_t is_default;
    Stmt_Block block;
};

struct Else_If {
    Site site;
    Expr *condition;
    Stmt_Block block;
};

enum Stmt_Kind {
    STMT_NONE,
    STMT_DECL,
    STMT_IF,
    STMT_FOR,
    STMT_SWITCH,
    STMT_WHILE,
    STMT_RETURN,
    STMT_EXPR,
    STMT_ASSIGN,
    STMT_BREAK,
    STMT_CONTINUE,
    STMT_USING,
    STMT_BLOCK,
    STMT_DEFER,
    STMT_FREE,
};

struct Stmt {
    Stmt_Kind kind;
    Site site;

    Note *notes;
    size_t num_notes;

    union {
        struct {
            Decl *decl;
        } stmt_decl;
        struct {
            Expr *if_expr;
            Stmt_Block then_block;
            Else_If *else_ifs;
            size_t num_else_ifs;
            Stmt_Block else_block;
        } stmt_if;
        struct {
            Expr *iterator;
            Expr *init;
            Expr *condition;
            Stmt_Block block;
        } stmt_for;
        struct {
            Expr *expr;
            Switch_Case *switch_cases;
            size_t num_switch_cases;
        } stmt_switch;
        struct {
            Expr *expr;
            Stmt_Block block;
        } stmt_while;
        struct {
            Expr **exprs;
            size_t num_exprs;
        } stmt_return;
        struct {
            Expr *expr;
        } stmt_expr;
        struct {
            Token_Kind op;
            Expr *left;
            Expr *right;
        } stmt_assign;
        struct {
            Expr *expr;
        } stmt_using;
        struct {
            Stmt_Block block;
        } stmt_block;
        struct {
            Stmt *stmt;
        } stmt_defer;
        struct {
        } stmt_break;
        struct {
        } stmt_continue;
        struct {
            Expr *expr;
        } stmt_free;
    };
};

global_var Expr expr__illegal = {EXPR_ILLEGAL};
global_var Expr *expr_illegal = &expr__illegal;

