struct Riscv_Sym_Ref;
struct Riscv_Sym;
struct Riscv_Asm;
enum Reg;

internal_proc uint32_t encode_b(uint32_t imm);
internal_proc uint32_t encode_i(uint32_t imm);
internal_proc uint32_t encode_j(uint32_t imm);
internal_proc uint32_t encode_s(uint32_t imm);
internal_proc uint32_t encode_u(uint32_t imm);

internal_proc uint32_t decode_u(uint32_t data);
internal_proc uint32_t decode_j(uint32_t data);
internal_proc uint32_t decode_i(uint32_t data);
internal_proc uint32_t decode_b(uint32_t data);
internal_proc uint32_t decode_s(uint32_t data);

internal_proc uint32_t imm_lo(uint32_t imm);
internal_proc uint32_t imm_hi(uint32_t imm);

internal_proc void asm_bytes(Riscv_Asm *riscv_asm, void *data, uint32_t size);
internal_proc void asm_auipc(Riscv_Asm *riscv_asm, Reg rd, int32_t imm);
internal_proc void asm_lui(Riscv_Asm *riscv_asm, Reg rd, int32_t imm);
internal_proc void gen_pop(Riscv_Asm *riscv_asm, Reg dest);
internal_proc void gen_push(Riscv_Asm *riscv_asm, Reg reg);

#define SHIFT_MASK (1 << 5) - 1
#define PUTCHAR_ADDR 0xFFFFFF04
#define GETCHAR_ADDR 0xFFFFFF00

#define U_IMMEDIATE_MIN   -(1 << 30)
#define U_IMMEDIATE_MAX   1 << 30 - 1
#define U_IMMEDIATE_ALIGN 1 << 12

#define I_IMMEDIATE_MAX   1 << 10 - 1
#define I_IMMEDIATE_MIN   -(1 << 10)
#define I_IMMEDIATE_ALIGN 1

#define S_IMMEDIATE_MIN   I_IMMEDIATE_MIN
#define S_IMMEDIATE_MAX   I_IMMEDIATE_MAX
#define S_IMMEDIATE_ALIGN I_IMMEDIATE_ALIGN

#define J_IMMEDIATE_MAX   1 << 19 - 1
#define J_IMMEDIATE_MIN   -(1 << 19)
#define J_IMMEDIATE_ALIGN 2

#define B_IMMEDIATE_MAX   1 << 11 - 1
#define B_IMMEDIATE_MIN   -(1 << 11)
#define B_IMMEDIATE_ALIGN 2

#define NUM_SYMS_PER_ALLOC 128

enum Op {
    Op_ILLEGAL,
    Op_LUI = 1,
    Op_AUIPC,
    Op_JAL,
    Op_JALR,
    Op_BEQ,
    Op_BNE,
    Op_BLT,
    Op_BGE,
    Op_BLTU,
    Op_BGEU,
    Op_LB,
    Op_LH,
    Op_LW,
    Op_LBU,
    Op_LHU,
    Op_SB,
    Op_SH,
    Op_SW,
    Op_ADDI,
    Op_SLTI,
    Op_SLTIU,
    Op_XORI,
    Op_ORI,
    Op_ANDI,
    Op_SLLI,
    Op_SRLI,
    Op_SRAI,
    Op_ADD,
    Op_SUB,
    Op_SLL,
    Op_SLT,
    Op_SLTU,
    Op_XOR,
    Op_SRL,
    Op_SRA,
    Op_OR,
    Op_AND,
    Op_FENCE,
    Op_FENCEI,
    Op_ECALL,
    Op_EBREAK,
    Op_CSRRW,
    Op_CSRRS,
    Op_CSRRC,
    Op_CSRRWI,
    Op_CSRRSI,
    Op_CSRRCI,

    Op_Count,
};

enum Reg {
    Reg_X0,
    Reg_X1,
    Reg_X2,
    Reg_X3,
    Reg_X4,
    Reg_X5,
    Reg_X6,
    Reg_X7,
    Reg_X8,
    Reg_X9,
    Reg_X10,
    Reg_X11,
    Reg_X12,
    Reg_X13,
    Reg_X14,
    Reg_X15,
    Reg_X16,
    Reg_X17,
    Reg_X18,
    Reg_X19,
    Reg_X20,
    Reg_X21,
    Reg_X22,
    Reg_X23,
    Reg_X24,
    Reg_X25,
    Reg_X26,
    Reg_X27,
    Reg_X28,
    Reg_X29,
    Reg_X30,
    Reg_X31,
};

typedef uint32_t Word;
typedef uint16_t Csr;
typedef uint8_t  Succ_Pred;

struct Instruction {
    Op op;
    Reg rs1;
    Reg rs2;
    Reg rd;
    Word imm;
    Csr csr;
    Succ_Pred succ_pred;
};

struct Stream {
    uint8_t *ptr;
};

Op funct3_to_branch_op[8];
Op funct3_to_load_op[8];
Op funct3_to_store_op[8];
Op funct3_to_imm_op[8];
Op funct4_to_reg_op[16];
Op funct3_to_csr_op[8];

uint32_t op_to_mask[Op_Count];

internal_proc void
riscv_init() {
    funct3_to_branch_op[0b000] = Op_BEQ;
    funct3_to_branch_op[0b001] = Op_BNE;
    funct3_to_branch_op[0b100] = Op_BLT;
    funct3_to_branch_op[0b101] = Op_BGE;
    funct3_to_branch_op[0b110] = Op_BLTU;
    funct3_to_branch_op[0b111] = Op_BGEU;

    funct3_to_load_op[0b000] = Op_LB;
    funct3_to_load_op[0b001] = Op_LH;
    funct3_to_load_op[0b010] = Op_LW;
    funct3_to_load_op[0b100] = Op_LBU;
    funct3_to_load_op[0b101] = Op_LHU;

    funct3_to_store_op[0b000] = Op_SB;
    funct3_to_store_op[0b001] = Op_SH;
    funct3_to_store_op[0b010] = Op_SW;

    funct3_to_imm_op[0b000] = Op_ADDI;
    funct3_to_imm_op[0b010] = Op_SLTI;
    funct3_to_imm_op[0b011] = Op_SLTIU;
    funct3_to_imm_op[0b100] = Op_XORI;
    funct3_to_imm_op[0b110] = Op_ORI;
    funct3_to_imm_op[0b111] = Op_ANDI;
    funct3_to_imm_op[0b001] = Op_SLLI;
    funct3_to_imm_op[0b101] = Op_SRLI;

    funct4_to_reg_op[0b0000] = Op_ADD;
    funct4_to_reg_op[0b1000] = Op_SUB;
    funct4_to_reg_op[0b0001] = Op_SLL;
    funct4_to_reg_op[0b0010] = Op_SLT;
    funct4_to_reg_op[0b0011] = Op_SLTU;
    funct4_to_reg_op[0b0100] = Op_XOR;
    funct4_to_reg_op[0b0101] = Op_SRL;
    funct4_to_reg_op[0b1101] = Op_SRA;
    funct4_to_reg_op[0b0110] = Op_OR;

    funct3_to_csr_op[0b001] = Op_CSRRW;
    funct3_to_csr_op[0b010] = Op_CSRRS;
    funct3_to_csr_op[0b011] = Op_CSRRC;
    funct3_to_csr_op[0b101] = Op_CSRRWI;
    funct3_to_csr_op[0b110] = Op_CSRRSI;
    funct3_to_csr_op[0b111] = Op_CSRRCI;

    op_to_mask[Op_LUI]    = 0b00000000000000000000000000110111;
    op_to_mask[Op_AUIPC]  = 0b00000000000000000000000000010111;
    op_to_mask[Op_JAL]    = 0b00000000000000000000000001101111;
    op_to_mask[Op_BEQ]    = 0b00000000000000000000000001100011;
    op_to_mask[Op_BNE]    = 0b00000000000000000001000001100011;
    op_to_mask[Op_BLT]    = 0b00000000000000000100000001100011;
    op_to_mask[Op_BGE]    = 0b00000000000000000101000001100011;
    op_to_mask[Op_BLTU]   = 0b00000000000000000110000001100011;
    op_to_mask[Op_BGEU]   = 0b00000000000000000111000001100011;
    op_to_mask[Op_JALR]   = 0b00000000000000000000000001100111;
    op_to_mask[Op_LB]     = 0b00000000000000000000000000000011;
    op_to_mask[Op_LH]     = 0b00000000000000000001000000000011;
    op_to_mask[Op_LW]     = 0b00000000000000000010000000000011;
    op_to_mask[Op_LBU]    = 0b00000000000000000100000000000011;
    op_to_mask[Op_LHU]    = 0b00000000000000000101000000000011;
    op_to_mask[Op_ADDI]   = 0b00000000000000000000000000010011;
    op_to_mask[Op_SLTI]   = 0b00000000000000000010000000010011;
    op_to_mask[Op_SLTIU]  = 0b00000000000000000011000000010011;
    op_to_mask[Op_XORI]   = 0b00000000000000000100000000010011;
    op_to_mask[Op_ORI]    = 0b00000000000000000110000000010011;
    op_to_mask[Op_ANDI]   = 0b00000000000000000111000000010011;
    op_to_mask[Op_ADD]    = 0b00000000000000000000000000110011;
    op_to_mask[Op_SUB]    = 0b01000000000000000000000000110011;
    op_to_mask[Op_SLL]    = 0b00000000000000000001000000110011;
    op_to_mask[Op_SLT]    = 0b00000000000000000010000000110011;
    op_to_mask[Op_SLTU]   = 0b00000000000000000011000000110011;
    op_to_mask[Op_XOR]    = 0b00000000000000000100000000110011;
    op_to_mask[Op_SRL]    = 0b00000000000000000101000000110011;
    op_to_mask[Op_SRA]    = 0b01000000000000000101000000110011;
    op_to_mask[Op_OR]     = 0b00000000000000000110000000110011;
    op_to_mask[Op_AND]    = 0b00000000000000000111000000110011;
    op_to_mask[Op_SLLI]   = 0b00000000000000000001000000010011;
    op_to_mask[Op_SRLI]   = 0b00000000000000000101000000010011;
    op_to_mask[Op_SRAI]   = 0b01000000000000000101000000010011;
    op_to_mask[Op_SB]     = 0b00000000000000000000000000100011;
    op_to_mask[Op_SH]     = 0b00000000000000000001000000100011;
    op_to_mask[Op_SW]     = 0b00000000000000000010000000100011;
    op_to_mask[Op_CSRRW]  = 0b00000000000000000001000001110011;
    op_to_mask[Op_CSRRS]  = 0b00000000000000000010000001110011;
    op_to_mask[Op_CSRRC]  = 0b00000000000000000011000001110011;
    op_to_mask[Op_CSRRWI] = 0b00000000000000000101000001110011;
    op_to_mask[Op_CSRRSI] = 0b00000000000000000110000001110011;
    op_to_mask[Op_CSRRCI] = 0b00000000000000000111000001110011;
    op_to_mask[Op_FENCE]  = 0b00000000000000000000000000001111;
    op_to_mask[Op_FENCEI] = 0b00000000000000000001000000001111;
    op_to_mask[Op_ECALL]  = 0b00000000000000000000000001110011;
    op_to_mask[Op_EBREAK] = 0b00000000000100000000000001110011;
}

internal_proc uint32_t
sign_extend(uint32_t data, uint32_t width) {
    return (uint32_t)((int32_t)(data << (32 - width)) >> (32 - width));
}

internal_proc uint32_t
bits(uint32_t data, int32_t start, int32_t len) {
    return (data >> start) & ((1 << len) - 1);
}

internal_proc uint32_t
encode_instruction(Instruction instr) {
    auto mask = op_to_mask[instr.op];

    auto rd   = instr.rd  <<  7;
    auto rs1  = instr.rs1 << 15;
    auto rs2  = instr.rs2 << 20;

    switch (instr.op) {
        case Op_LUI: case Op_AUIPC: {
            return mask | rd | encode_u(instr.imm);
        } break;

        case Op_JAL: {
            return mask | rd | encode_j(instr.imm);
        } break;

        case Op_BEQ: case Op_BNE: case Op_BGEU:
        case Op_BLT: case Op_BGE: case Op_BLTU: {
            return mask | rs1 | rs2 | encode_b(instr.imm);
        } break;

        case Op_JALR: case Op_LB: case Op_LH:
        case Op_LW: case Op_LBU: case Op_LHU:
        case Op_ADDI: case Op_SLTI: case Op_SLTIU:
        case Op_XORI: case Op_ORI: case Op_ANDI:
        {
            return mask | rd | rs1 | encode_i(instr.imm);
        } break;

        case Op_ADD: case Op_SUB: case Op_SLL:
        case Op_SLT: case Op_SLTU: case Op_XOR:
        case Op_SRL: case Op_SRA: case Op_OR: case Op_AND:
        {
            return mask | rd | rs1 | rs2;
        } break;

        case Op_SLLI: case Op_SRLI: case Op_SRAI: {
            auto imm = (instr.imm & SHIFT_MASK) << 20;

            return mask | rd | rs1 | imm;
        } break;

        case Op_SB: case Op_SH: case Op_SW: {
            return mask | rs1 | rs2 | encode_s(instr.imm);
        } break;

        case Op_FENCE: {
            auto succ_pred = instr.succ_pred << 20;

            return mask | succ_pred;
        } break;

        case Op_FENCEI: case Op_ECALL: case Op_EBREAK: {
            return mask;
        } break;

        case Op_CSRRW: case Op_CSRRS: case Op_CSRRC: {
            auto csr = instr.csr << 20;

            return mask | rd | rs1 | csr;
        } break;

        case Op_CSRRWI: case Op_CSRRSI: case Op_CSRRCI: {
            auto csr = instr.csr << 20;
            auto imm = bits(instr.imm, 0, 5) << 15;

            return mask | rd | imm | csr;
        } break;

        default: {
            return 0;
        } break;
    }
}

internal_proc uint32_t
encode_b(uint32_t imm) {
    auto imm_1_4  = bits(imm, 1, 4) << 8;
    auto imm_5_10 = bits(imm, 5, 6) << 25;
    auto imm_11   = bits(imm, 11, 1) << 7;
    auto imm_12   = bits(imm, 12, 1) << 31;

    return imm_1_4 | imm_5_10 | imm_11 | imm_12;
}

internal_proc uint32_t
encode_i(uint32_t imm) {
    auto imm_0_11 = bits(imm, 0, 12) << 20;

    return imm_0_11;
}

internal_proc uint32_t
encode_j(uint32_t imm) {
    auto imm_1_10  = bits(imm, 1, 10) << 21;
    auto imm_11    = bits(imm, 11, 1) << 20;
    auto imm_12_19 = bits(imm, 12, 8) << 12;
    auto imm_20    = bits(imm, 20, 1) << 31;

    return imm_1_10 | imm_11 | imm_12_19 | imm_20;
}

internal_proc uint32_t
encode_s(uint32_t imm) {
    auto imm_0_4  = bits(imm, 0, 5) << 7;
    auto imm_5_11 = bits(imm, 5, 7) << 25;

    return imm_0_4 | imm_5_11;
}

internal_proc uint32_t
encode_u(uint32_t imm) {
    auto imm_12_31 = bits(imm, 12, 20) << 12;

    return imm_12_31;
}

internal_proc Instruction
decode_instruction(uint32_t data) {
    auto opcode = bits(data, 0, 7);
    auto funct3 = bits(data, 12, 3);
    auto funct7 = bits(data, 25, 7);
    Reg rd      = (Reg)bits(data, 7, 5);
    Reg rs1     = (Reg)bits(data, 15, 5);
    Reg rs2     = (Reg)bits(data, 20, 5);

    Instruction result = {};

    switch (opcode) {
        case 0b0110111: {
            result.op = Op_LUI;
            result.rd = rd;
            result.imm = decode_u(data);

            return result;
        } break;

        case 0b0010111: {
            result.op = Op_AUIPC;
            result.rd = rd;
            result.imm = decode_u(data);

            return result;
        } break;

        case 0b1101111: {
            result.op = Op_JAL;
            result.rd = rd;
            result.imm = decode_j(data);

            return result;
        } break;

        case 0b1100111: {
            if (funct3 == 0b000) {
                result.op = Op_JALR;
                result.rd = rd;
                result.rs1 = rs1;
                result.imm = decode_i(data);

                return result;
            }
        } break;

        case 0b1100011: {
            result.op = funct3_to_branch_op[funct3];
            result.rs1 = rs1;
            result.rs2 = rs2;
            result.imm = decode_b(data);

            return result;
        } break;

        case 0b0000011: {
            result.op = funct3_to_load_op[funct3];
            result.rd = rd;
            result.rs1 = rs1;
            result.imm = decode_i(data);

            return result;
        } break;

        case 0b0100011: {
            result.op = funct3_to_store_op[funct3];
            result.rs1 = rs1;
            result.rs2 = rs2;
            result.imm = decode_s(data);

            return result;
        } break;

        case 0b0010011: {
            auto op = funct3_to_imm_op[funct3];

            switch (op) {
                case Op_SLLI: {
                    if (funct7 == 0b0000000) {
                        result.op = Op_SLLI;
                        result.rd = rd;
                        result.rs1 = rs1;
                        result.imm = rs2;

                        return result;
                    }
                } break;

                case Op_SRLI: {
                    if (funct7 == 0b0000000) {
                        result.op = Op_SRLI;
                        result.rd = rd;
                        result.rs1 = rs1;
                        result.imm = rs2;

                        return result;
                    } else if (funct7 == 0b0100000) {
                        result.op = Op_SRAI;
                        result.rd = rd;
                        result.rs1 = rs1;
                        result.imm = rs2;

                        return result;
                    }
                } break;

                default: {
                    result.op = op;
                    result.rd = rd;
                    result.rs1 = rs1;
                    result.imm = decode_i(data);

                    return result;
                } break;
            }
        } break;

        case 0b0110011: {
            if ((funct7 & 0b1011111) == 0) {
                auto funct4 = funct3 | (bits(funct7, 5, 1) << 3);

                result.op = funct4_to_reg_op[funct4];
                result.rd = rd;
                result.rs1 = rs1;
                result.rs2 = rs2;

                return result;
            }
        } break;

        case 0b0001111: {
            if (data == 0b00000000000000000001000000001111) {
                return {Op_FENCEI};
            } else if ((data & 0b11110000000011111111111110000000) == 0) {
                result.op = Op_FENCE;
                result.succ_pred = (Succ_Pred)bits(data, 20, 8);

                return result;
            }
        } break;

        case 0b1110011: {
            if (data == 0b00000000000000000000000001110011) {
                return {Op_ECALL};
            } else if (data == 0b00000000000100000000000001110011) {
                return {Op_EBREAK};
            } else {
                result.op = funct3_to_csr_op[funct3];
                result.rd = rd;
                result.rs1 = rs1;
                result.rs2 = rs2;
                result.csr = (Csr)bits(data, 20, 12);
                result.imm = (uint32_t)bits(data, 15, 5);

                return result;
            }
        } break;

        default: {
        } break;
    }

    return {Op_ILLEGAL};
}

internal_proc uint32_t
decode_u(uint32_t data) {
    auto imm_12_31 = bits(data, 12, 20) << 12;
    auto imm_0_31  = imm_12_31;

    return sign_extend(imm_0_31, 32);
}

internal_proc uint32_t
decode_j(uint32_t data) {
    auto imm_1_10  = bits(data, 21, 10) << 1;
    auto imm_11    = bits(data, 20, 1) << 11;
    auto imm_12_19 = bits(data, 12, 8) << 12;
    auto imm_20    = bits(data, 31, 1) << 20;
    auto imm_0_20  = imm_1_10 | imm_11 | imm_12_19 | imm_20;

    return sign_extend(imm_0_20, 21);
}

internal_proc uint32_t
decode_i(uint32_t data) {
    auto imm_0_11 = bits(data, 20, 12);

    return sign_extend(imm_0_11, 12);
}

internal_proc uint32_t
decode_b(uint32_t data) {
    auto imm_1_4  = bits(data, 8, 4) << 1;
    auto imm_5_10 = bits(data, 25, 6) << 5;
    auto imm_11   = bits(data, 7, 1) << 11;
    auto imm_12   = bits(data, 31, 1) << 12;
    auto imm_0_12 = imm_1_4 | imm_5_10 | imm_11 | imm_12;

    return sign_extend(imm_0_12, 13);
}

internal_proc uint32_t
decode_s(uint32_t data) {
    auto imm_0_4  = bits(data, 7, 5);
    auto imm_5_11 = bits(data, 25, 7) << 5;
    auto imm_0_11 = imm_0_4 | imm_5_11;

    return sign_extend(imm_0_11, 12);
}

struct Bus {
    uint8_t *ram;
    uint32_t ram_start;
    uint32_t ram_end;
};

struct Hart {
    uint32_t pc;
    uint32_t regs[32];
    Bus *bus;
};

internal_proc void
print(Hart *hart) {
    printf("pc = %d\n", hart->pc);

    for ( int it = 1; it < 31; it += 4 ) {
        printf("%2d = %10d | ", it,   hart->regs[it  ]);
        printf("%2d = %10d | ", it+1, hart->regs[it+1]);
        printf("%2d = %10d | ", it+2, hart->regs[it+2]);
        printf("%2d = %10d\n",  it+3, hart->regs[it+3]);
    }
}

internal_proc uint8_t
bus_load_byte(Bus *bus, uint32_t addr) {
    if ( bus->ram_start <= addr && addr + 1 <= bus->ram_end ) {
        return *(uint8_t*)(bus->ram + addr);
    }
    return 0;
}

internal_proc uint16_t
bus_load_hword(Bus *bus, uint32_t addr) {
    if ( bus->ram_start <= addr && addr + 2 <= bus->ram_end ) {
        return *(uint16_t*)(bus->ram + addr);
    }
    return 0;
}

internal_proc uint32_t
bus_load_word(Bus *bus, uint32_t addr) {
    if ( bus->ram_start <= addr && addr + 4 <= bus->ram_end ) {
        return *(uint32_t*)(bus->ram + addr);
    } else if ( addr == GETCHAR_ADDR ) {
        auto c = getchar();
        while ( c == '\n' ) {
            c = getchar();
        }

        return c;
    }

    return 0;
}

internal_proc void
bus_store_byte(Bus *bus, uint32_t addr, uint8_t data) {
    if ( bus->ram_start <= addr && addr + 1 <= bus->ram_end ) {
        *(uint8_t*)(bus->ram + addr) = data;
    }
}

internal_proc void
bus_store_hword(Bus *bus, uint32_t addr, uint16_t data) {
    if ( bus->ram_start <= addr && addr + 2 <= bus->ram_end ) {
        *(uint16_t*)(bus->ram + addr) = data;
    }
}

internal_proc void
bus_store_word(Bus *bus, uint32_t addr, uint32_t data) {
    if ( bus->ram_start <= addr && addr + 4 <= bus->ram_end ) {
        *(uint32_t*)(bus->ram + addr) = data;
    } else if (addr == PUTCHAR_ADDR) {
        putchar(data);
    }
}

internal_proc uint32_t
fetch_instruction(Hart *hart, uint32_t addr) {
    return bus_load_word(hart->bus, addr);
}

internal_proc void
put_instruction(Stream *stream, Instruction instr) {
    *(uint32_t*)stream->ptr = encode_instruction(instr);
    stream->ptr += 4;
}

internal_proc uint32_t
read_reg(Hart *hart, Reg reg) {
    return hart->regs[reg];
}

internal_proc void
write_reg(Hart *hart, Reg reg, uint32_t data) {
    if (reg) {
        hart->regs[reg] = data;
    }
}

internal_proc void
write_csr(Hart *hart, Csr csr, uint32_t val) {
}

internal_proc uint8_t
load_byte(Hart *hart, uint8_t addr) {
    return bus_load_byte(hart->bus, addr);
}

internal_proc uint16_t
load_hword(Hart *hart, uint16_t addr) {
    return bus_load_hword(hart->bus, addr);
}

internal_proc uint32_t
load_word(Hart *hart, uint32_t addr) {
    return bus_load_word(hart->bus, addr);
}

internal_proc void
store_byte(Hart *hart, uint32_t addr, uint8_t data) {
    bus_store_byte(hart->bus, addr, data);
}

internal_proc void
store_hword(Hart *hart, uint32_t addr, uint16_t data) {
    bus_store_hword(hart->bus, addr, data);
}

internal_proc void
store_word(Hart *hart, uint32_t addr, uint32_t data) {
    bus_store_word(hart->bus, addr, data);
}

internal_proc uint32_t
read_csr(Hart *hart, Csr csr) {
    return 0;
}

internal_proc void
not_implemented() {
    // auch noch nicht implementiert
}

internal_proc void
step(Hart *hart) {
    auto pc        = hart->pc;
    auto data      = fetch_instruction(hart, pc);
    auto instr     = decode_instruction(data);
    auto rs1       = instr.rs1;
    auto rs2       = instr.rs2;
    auto rd        = instr.rd;
    auto imm       = instr.imm;
    Csr csr        = instr.csr;
    Reg rs1_val    = (Reg)hart->regs[rs1];
    Reg rs2_val    = (Reg)hart->regs[rs2];
    auto next_pc   = pc + 4;
    auto branch_pc = pc + instr.imm;

    switch (instr.op) {
        case Op_ILLEGAL: {
        } break;

        case Op_LUI: {
            write_reg(hart, rd, imm);
        } break;

        case Op_AUIPC: {
            write_reg(hart, rd, pc + imm);
        } break;

        case Op_JAL: {
            write_reg(hart, rd, next_pc);
            next_pc = branch_pc;
        } break;

        case Op_JALR: {
            write_reg(hart, rd, next_pc);
            next_pc = (rs1_val + imm) & ~1;
        } break;

        case Op_BEQ: {
            if (rs1_val == rs2_val) {
                next_pc = branch_pc;
            }
        } break;

        case Op_BNE: {
            if (rs1_val != rs2_val) {
                next_pc = branch_pc;
            }
        } break;

        case Op_BLT: {
            if ((int32_t)rs1_val < (int32_t)rs2_val) {
                next_pc = branch_pc;
            }
        } break;

        case Op_BGE: {
            if ((int32_t)rs1_val >= (int32_t)rs2_val) {
                next_pc = branch_pc;
            }
        } break;

        case Op_BLTU: {
            if (rs1_val < rs2_val) {
                next_pc = branch_pc;
            }
        } break;

        case Op_BGEU: {
            if (rs1_val >= rs2_val) {
                next_pc = branch_pc;
            }
        } break;

        case Op_LB: {
            write_reg(hart, rd, sign_extend(load_byte(hart, (uint8_t)(rs1_val + imm)), 8));
        } break;

        case Op_LH: {
            write_reg(hart, rd, sign_extend(load_hword(hart, (uint16_t)(rs1_val + imm)), 16));
        } break;

        case Op_LW: {
            write_reg(hart, rd, load_word(hart, rs1_val + imm));
        } break;

        case Op_LBU: {
            write_reg(hart, rd, load_byte(hart, (uint8_t)(rs1_val + imm)));
        } break;

        case Op_LHU: {
            write_reg(hart, rd, load_hword(hart, (uint16_t)(rs1_val + imm)));
        } break;

        case Op_SB: {
            store_byte(hart, rs1_val + imm, (uint8_t)rs2_val);
        } break;

        case Op_SH: {
            store_hword(hart, rs1_val + imm, (uint16_t)rs2_val);
        } break;

        case Op_SW: {
            store_word(hart, rs1_val + imm, rs2_val);
        } break;

        case Op_ADDI: {
            write_reg(hart, rd, rs1_val + imm);
        } break;

        case Op_SLTI: {
            write_reg(hart, rd, (int32_t)rs1_val < (int32_t)imm);
        } break;

        case Op_SLTIU: {
            write_reg(hart, rd, (int32_t)rs1_val < (int32_t)imm);
        } break;

        case Op_XORI: {
            write_reg(hart, rd, rs1_val ^ imm);
        } break;

        case Op_ORI: {
            write_reg(hart, rd, rs1_val | imm);
        } break;

        case Op_ANDI: {
            write_reg(hart, rd, rs1_val & imm);
        } break;

        case Op_SLLI: {
            write_reg(hart, rd, rs1_val << imm);
        } break;

        case Op_SRLI: {
            write_reg(hart, rd, rs1_val >> imm);
        } break;

        case Op_SRAI: {
            write_reg(hart, rd, (int32_t)rs1_val >> imm);
        } break;

        case Op_ADD: {
            write_reg(hart, rd, rs1_val + rs2_val);
        } break;

        case Op_SUB: {
            write_reg(hart, rd, rs1_val - rs2_val);
        } break;

        case Op_SLL: {
            write_reg(hart, rd, rs1_val << (rs2_val & SHIFT_MASK));
        }

        case Op_SLT: {
            write_reg(hart, rd, (int32_t)rs1_val < (int32_t)rs2_val);
        }

        case Op_SLTU: {
            write_reg(hart, rd, rs1_val < rs2_val);
        }

        case Op_XOR: {
            write_reg(hart, rd, rs1_val ^ rs2_val);
        }

        case Op_SRL: {
            write_reg(hart, rd, rs1_val >> (rs2_val & SHIFT_MASK));
        }

        case Op_SRA: {
            write_reg(hart, rd, (int32_t)rs1_val >> (rs2_val & SHIFT_MASK));
        }

        case Op_OR: {
            write_reg(hart, rd, rs1_val | rs2_val);
        }

        case Op_AND: {
            write_reg(hart, rd, rs1_val & rs2_val);
        }

        case Op_FENCE: {
            // nichts tun
        }

        case Op_FENCEI: {
            // nichts tun
        }

        case Op_ECALL: {
            not_implemented();
        }

        case Op_EBREAK: {
            not_implemented();
        }

        case Op_CSRRW: {
            if (rd) {
                auto csr_val = read_csr(hart, csr);
                write_reg(hart, rd, csr_val);
            }
            write_csr(hart, instr.csr, rs1_val);
        }

        case Op_CSRRS: {
            auto csr_val = read_csr(hart, csr);
            write_reg(hart, rd, csr_val);
            if (rs1_val) {
                write_csr(hart, csr, csr_val | rs1_val);
            }
        }

        case Op_CSRRC: {
            auto csr_val = read_csr(hart, csr);
            if (rs1_val) {
                write_reg(hart, rd, csr_val & ~rs1_val);
            }
        }

        case Op_CSRRWI: {
            if (rd) {
                auto csr_val = read_csr(hart, csr);
                write_reg(hart, rd, csr_val);
            }
            write_csr(hart, csr, imm);
        }

        case Op_CSRRSI: {
            auto csr_val = read_csr(hart, csr);
            write_reg(hart, rd, csr_val);
            if (imm) {
                write_csr(hart, csr, csr_val | imm);
            }
        }

        case Op_CSRRCI: {
            auto csr_val = read_csr(hart, csr);
            if (imm) {
                write_reg(hart, rd, csr_val & ~imm);
            }
        }
    }

    hart->pc = next_pc;
}

enum Riscv_Asm_Error {
    ASMERROR_NONE,
    ASMERROR_OVERFLOW,
};

struct Riscv_Asm {
    uint8_t *buf;
    uint32_t buf_size;
    Riscv_Asm_Error error;
    uint32_t addr;

    Riscv_Sym *free_sym;
    Riscv_Sym_Ref *free_ref;
};

enum Riscv_Sym_Ref_Kind {
    SR_LO_OFFSET,
    SR_HI_OFFSET,
};

struct Riscv_Sym_Ref {
    Riscv_Sym_Ref_Kind kind;
    uint32_t addr;
    uint32_t base;
    Riscv_Sym_Ref *next;
};

enum Riscv_Sym_State {
    SYMSTATE_UNRESOLVED,
    SYMSTATE_RESOLVED,
};

struct Riscv_Sym {
    Riscv_Sym_State state;
    uint32_t addr;
    Riscv_Sym_Ref *ref;
};

internal_proc Riscv_Sym *
sym_new(Riscv_Asm *riscv_asm) {
    Riscv_Sym *sym = (Riscv_Sym *)xcalloc(1, sizeof(Riscv_Sym));

    return sym;
}

internal_proc void
symref_resolve(Riscv_Asm *riscv_asm, Riscv_Sym *sym, Riscv_Sym_Ref *ref) {
    uint32_t *instr_ptr = (uint32_t*)(riscv_asm->buf + ref->addr);
    auto instr = decode_instruction(*instr_ptr);

    switch (ref->kind) {
        case SR_LO_OFFSET: {
            instr.imm = imm_lo(sym->addr - ref->base);
        } break;

        case SR_HI_OFFSET: {
            instr.imm = imm_hi(sym->addr - ref->base);
        } break;
    }

    *instr_ptr = encode_instruction(instr);
}

internal_proc void
sym_resolve_here(Riscv_Asm *riscv_asm, Riscv_Sym *sym) {
    auto addr = riscv_asm->addr;
    sym->state = SYMSTATE_RESOLVED;
    sym->addr = addr;

    auto ref = sym->ref;
    while (ref) {
        symref_resolve(riscv_asm, sym, ref);
        ref = ref->next;
    }
}

internal_proc Riscv_Sym *
sym_new_here(Riscv_Asm *riscv_asm) {
    Riscv_Sym *result = sym_new(riscv_asm);
    sym_resolve_here(riscv_asm, result);

    return result;
}

internal_proc void
ref_sym_offset(Riscv_Asm *riscv_asm, Riscv_Sym *sym,
        Riscv_Sym_Ref_Kind kind, uint32_t base)
{
    auto addr = riscv_asm->addr - 4;
    if (sym->state == SYMSTATE_UNRESOLVED) {
        Riscv_Sym_Ref *ref = (Riscv_Sym_Ref *)malloc(sizeof(Riscv_Sym_Ref));
        ref->kind = kind;
        ref->addr = addr;
        ref->base = base;
        ref->next = sym->ref;
        sym->ref  = ref;
    } else {
        Riscv_Sym_Ref ref = {};
        ref.kind = kind;
        ref.addr = addr;
        ref.base = base;
        symref_resolve(riscv_asm, sym, &ref);
    }
}

internal_proc b32
has_overflow(Riscv_Asm *riscv_asm, uint32_t size) {
    if ((riscv_asm->addr + size) > riscv_asm->buf_size) {
        riscv_asm->error = ASMERROR_OVERFLOW;

        return true;
    }

    return false;
}

internal_proc uint32_t
imm_lo(uint32_t imm) {
    return imm;
}

internal_proc uint32_t
imm_hi(uint32_t imm) {
    return imm + 0x800;
}

internal_proc void
asm_align(Riscv_Asm *riscv_asm, uint32_t align) {
    riscv_asm->addr = ((riscv_asm->addr + align - 1) / align) * align;
}

internal_proc void
asm_u8(Riscv_Asm *riscv_asm, uint8_t data) {
    asm_bytes(riscv_asm, &data, 1);
}

internal_proc void
asm_u16(Riscv_Asm *riscv_asm, uint16_t data) {
    asm_bytes(riscv_asm, &data, 2);
}

internal_proc void
asm_u32(Riscv_Asm *riscv_asm, uint32_t data) {
    asm_bytes(riscv_asm, &data, 4);
}

internal_proc void
asm_str(Riscv_Asm *riscv_asm, char *str) {
    asm_bytes(riscv_asm, str, (uint32_t)strlen(str));
}

internal_proc void
asm_bytes(Riscv_Asm *riscv_asm, void *data, uint32_t size) {
    if (has_overflow(riscv_asm, size)) {
        return;
    }

    memcpy(riscv_asm->buf + riscv_asm->addr, data, size);
    riscv_asm->addr += size;
}

internal_proc void
asm_instr(Riscv_Asm *riscv_asm, Instruction instr) {
    asm_u32(riscv_asm, encode_instruction(instr));
}

internal_proc void
asm_add(Riscv_Asm *riscv_asm, Reg rd, Reg rs1, Reg rs2) {
    Instruction instr = {};

    instr.op = Op_ADD;
    instr.rd = rd;
    instr.rs1 = rs1;
    instr.rs2 = rs2;

    asm_instr(riscv_asm, instr);
}

internal_proc void
asm_addi(Riscv_Asm *riscv_asm, Reg rd, Reg rs1, int32_t imm) {
    Instruction instr = {};

    instr.op = Op_ADDI;
    instr.rd = rd;
    instr.rs1 = rs1;
    instr.imm = imm;

    asm_instr(riscv_asm, instr);
}

internal_proc void
asm_slli(Riscv_Asm *riscv_asm, Reg rd, Reg rs1, int32_t imm) {
    Instruction instr = {};

    instr.op = Op_SLLI;
    instr.rd = rd;
    instr.rs1 = rs1;
    instr.imm = imm;

    asm_instr(riscv_asm, instr);
}

internal_proc void
asm_lw_reg(Riscv_Asm *riscv_asm, Reg rd, Reg rs1, int32_t imm) {
    Instruction instr = {};

    instr.op = Op_LW;
    instr.rd = rd;
    instr.rs1 = rs1;
    instr.imm = imm;

    asm_instr(riscv_asm, instr);
}

internal_proc void
asm_lw(Riscv_Asm *riscv_asm, Reg dest, Riscv_Sym *src) {
    auto base = riscv_asm->addr;
    asm_auipc(riscv_asm, dest, 0);
    ref_sym_offset(riscv_asm, src, SR_HI_OFFSET, base);
    asm_lw_reg(riscv_asm, dest, dest, 0);
    ref_sym_offset(riscv_asm, src, SR_LO_OFFSET, base);
}

internal_proc void
asm_lwi(Riscv_Asm *riscv_asm, Reg dest, uint32_t imm) {
    asm_lui(riscv_asm, dest, imm_hi(imm));
    asm_lw_reg(riscv_asm, dest, dest, imm_lo(imm));
}

internal_proc void
asm_sw_reg(Riscv_Asm *riscv_asm, Reg rs1, Reg rs2, int32_t imm) {
    Instruction instr = {};

    instr.op = Op_SW;
    instr.rs1 = rs1;
    instr.rs2 = rs2;
    instr.imm = imm;

    asm_instr(riscv_asm, instr);
}

internal_proc void
asm_sw(Riscv_Asm *riscv_asm, Riscv_Sym *dest, Reg src, Reg temp) {
    auto base = riscv_asm->addr;
    asm_auipc(riscv_asm, temp, 0);
    ref_sym_offset(riscv_asm, dest, SR_HI_OFFSET, base);
    asm_sw_reg(riscv_asm, temp, src, 0);
    ref_sym_offset(riscv_asm, dest, SR_LO_OFFSET, base);
}

internal_proc void
asm_swi(Riscv_Asm *riscv_asm, uint32_t dest, Reg src, Reg temp) {
    asm_lui(riscv_asm, temp, imm_hi(dest));
    asm_sw_reg(riscv_asm, temp, src, imm_lo(dest));
}

internal_proc void
asm_op(Riscv_Asm *riscv_asm, Op op, Reg rd, Reg rs1, Reg rs2) {
    Instruction instr = {};

    instr.op = op;
    instr.rd = rd;
    instr.rs1 = rs1;
    instr.rs2 = rs2;

    asm_instr(riscv_asm, instr);
}

internal_proc void
asm_opi(Riscv_Asm *riscv_asm, Op op, Reg rd, Reg rs1, int32_t imm) {
    Instruction instr = {};

    instr.op = op;
    instr.rd = rd;
    instr.rs1 = rs1;
    instr.imm = imm;

    asm_instr(riscv_asm, instr);
}

internal_proc void
asm_mv(Riscv_Asm *riscv_asm, Reg dest, Reg src) {
    asm_op(riscv_asm, Op_ADD, dest, src, Reg_X0);
}

internal_proc void
asm_li(Riscv_Asm *riscv_asm, Reg dest, uint32_t imm) {
    asm_opi(riscv_asm, Op_ADD, dest, Reg_X0, imm);
}

internal_proc void
asm_auipc(Riscv_Asm *riscv_asm, Reg rd, int32_t imm) {
    Instruction instr = {};

    instr.op = Op_AUIPC;
    instr.rd = rd;
    instr.imm = imm;

    asm_instr(riscv_asm, instr);
}

internal_proc void
asm_lui(Riscv_Asm *riscv_asm, Reg rd, int32_t imm) {
    Instruction instr = {};

    instr.op = Op_LUI;
    instr.rd = rd;
    instr.imm = imm;

    asm_instr(riscv_asm, instr);
}

internal_proc void
asm_beq(Riscv_Asm *riscv_asm, Reg rs1, Reg rs2, Riscv_Sym *dest) {
    auto base = riscv_asm->addr;
    Instruction instr = {};

    instr.op = Op_BEQ;
    instr.rs1 = rs1;
    instr.rs2 = rs2;

    asm_instr(riscv_asm, instr);

    ref_sym_offset(riscv_asm, dest, SR_LO_OFFSET, base);
}

internal_proc void
asm_jal(Riscv_Asm *riscv_asm, Reg rd, Riscv_Sym *dest) {
    auto base = riscv_asm->addr;
    Instruction instr = {};

    instr.op = Op_JAL;
    instr.rd = rd;

    asm_instr(riscv_asm, instr);
    ref_sym_offset(riscv_asm, dest, SR_LO_OFFSET, base);
}

internal_proc void
asm_j(Riscv_Asm *riscv_asm, Riscv_Sym *dest) {
    asm_jal(riscv_asm, Reg_X0, dest);
}

internal_proc void
asm_la(Riscv_Asm *riscv_asm, Reg rd, Riscv_Sym *src) {
    auto base = riscv_asm->addr;
    asm_auipc(riscv_asm, rd, 0);
    ref_sym_offset(riscv_asm, src, SR_HI_OFFSET, base);
    asm_addi(riscv_asm, rd, rd, 0);
    ref_sym_offset(riscv_asm, src, SR_LO_OFFSET, base);
}

internal_proc void
asm_getchar(Riscv_Asm *riscv_asm, Reg dest) {
    asm_lwi(riscv_asm, dest, GETCHAR_ADDR);
}

internal_proc void
asm_putchar(Riscv_Asm *riscv_asm, Reg src, Reg temp) {
    asm_swi(riscv_asm, PUTCHAR_ADDR, src, temp);
}

internal_proc void
gen_bin_op(Riscv_Asm *riscv_asm, Op op) {
    gen_pop(riscv_asm, Reg_X2);
    gen_pop(riscv_asm, Reg_X3);
    asm_op(riscv_asm, op, Reg_X2, Reg_X2, Reg_X3);
    gen_push(riscv_asm, Reg_X2);
}

internal_proc void
gen_pop(Riscv_Asm *riscv_asm, Reg dest) {
    asm_opi(riscv_asm, Op_ADDI, Reg_X1, Reg_X1, -4);
    asm_lw_reg(riscv_asm, dest, Reg_X1, 0);
}

internal_proc void
gen_push(Riscv_Asm *riscv_asm, Reg reg) {
    asm_sw_reg(riscv_asm, Reg_X1, reg, 0);
    asm_opi(riscv_asm, Op_ADDI, Reg_X1, Reg_X1, 4);
}

internal_proc void
gen_pushi(Riscv_Asm *riscv_asm, uint32_t imm) {
    asm_li(riscv_asm, Reg_X2, imm);
    gen_push(riscv_asm, Reg_X2);
}

internal_proc void
gen_dup(Riscv_Asm *riscv_asm) {
    gen_pop(riscv_asm, Reg_X2);
    gen_push(riscv_asm, Reg_X2);
    gen_push(riscv_asm, Reg_X2);
}

internal_proc void
gen_swap(Riscv_Asm *riscv_asm) {
    gen_pop(riscv_asm, Reg_X2);
    gen_pop(riscv_asm, Reg_X3);
    gen_push(riscv_asm, Reg_X2);
    gen_push(riscv_asm, Reg_X3);
}

internal_proc Riscv_Sym *
gen_rpn(Riscv_Asm *riscv_asm, char *str, uint32_t stack_size) {
    Riscv_Sym *stack = sym_new_here(riscv_asm);
    asm_align(riscv_asm, 4);
    riscv_asm->addr += stack_size;
    Riscv_Sym *start = sym_new_here(riscv_asm);
    asm_la(riscv_asm, Reg_X1, stack);

    while (*str) {
        switch (*str) {
            case 'd': {
                gen_dup(riscv_asm);
            } break;

            case 's': {
                gen_swap(riscv_asm);
            } break;

            case '+': {
                gen_bin_op(riscv_asm, Op_ADD);
            } break;

            case '-': {
                gen_bin_op(riscv_asm, Op_SUB);
            } break;

            case '0': case '1': case '2': case '3': case '4':
            case '5': case '6': case '7': case '8': case '9': {
                gen_pushi(riscv_asm, *str - '0');
            } break;

            case '?': {
                asm_getchar(riscv_asm, Reg_X2);
                asm_opi(riscv_asm, Op_ADDI, Reg_X2, Reg_X2, -48);
                gen_push(riscv_asm, Reg_X2);
            } break;

            case '!': {
                gen_pop(riscv_asm, Reg_X2);
                asm_opi(riscv_asm, Op_ADDI, Reg_X2, Reg_X2, 48);
                asm_putchar(riscv_asm, Reg_X2, Reg_X3);
                asm_li(riscv_asm, Reg_X2, '\n');
                asm_putchar(riscv_asm, Reg_X2, Reg_X3);
            } break;
        }
        str += 1;
    }

    return start;
}

internal_proc void
riscv_test() {
#define RAM_SIZE 256*1024
    uint8_t ram[RAM_SIZE];
    Riscv_Asm riscv_asm = {};

    riscv_asm.buf = ram;
    riscv_asm.buf_size = sizeof(ram);

    Riscv_Sym *start = gen_rpn(&riscv_asm, "??+?s-d+!", 1024);
    asm_j(&riscv_asm, start);

    /*
    count := sym_new_here(asm);
    asm_u32(asm, 10);
    result := sym_new(asm);
    asm_u32(asm, 0);

    start := sym_new_here(asm);

    repeat := sym_new_here(asm);
    asm_getchar(asm, Reg.X5);
    asm_putchar(asm, Reg.X5, Reg.X6);
    asm_j(asm, repeat);

    asm_lw(asm, Reg.X1, count);
    loop := sym_new_here(asm);
    done := sym_new(asm);
    asm_beq(asm, Reg.X1, Reg.X0, done);
    asm_add(asm, Reg.X2, Reg.X2, Reg.X1);
    asm_addi(asm, Reg.X1, Reg.X1, -1);
    asm_j(asm, loop);
    sym_resolve_here(asm, done);
    asm_sw(asm, result, Reg.X2, Reg.X1);

    global := sym_new(asm);
    asm_la(asm, Reg.X4, global);

    asm_lw(asm, Reg.X1, global);
    asm_add(asm, Reg.X1, Reg.X1, Reg.X1);
    asm_sw(asm, global, Reg.X1, Reg.X2);
    asm_lw(asm, Reg.X2, global);

    for 0..1023 {
        asm_u8(asm, 0);
    }

    sym_resolve_here(asm, global);
    asm_u32(asm, 12345678);
    */

    Bus bus = {};
    bus.ram = ram;
    bus.ram_start = 0;
    bus.ram_end = sizeof(ram);

    Hart hart = {};
    hart.pc = start->addr;
    hart.bus = &bus;

    for (;;) {
        // getchar();
        step(&hart);
        print(&hart);
    }
}

internal_proc void
riscv_main() {
    riscv_test();
}

