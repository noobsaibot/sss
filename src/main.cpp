#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <windows.h>
#include <wchar.h>

#include "lib/string.cpp"

#include "common.cpp"
#include "os.cpp"
#include "report.cpp"
#include "lex.cpp"
#include "ast.cpp"
#include "module.cpp"
#include "parser.cpp"
#include "resolve.cpp"
#include "print.cpp"
// #include "gen.cpp"
#include "riscv.cpp"
#include "sss.cpp"
// #include "test.cpp"

int main(int argc, char **argv) {
#if 0
    riscv_main();
    main_test();
#endif
    return sss_main(argc, argv);
}

