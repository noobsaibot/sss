internal_proc void
report_error(char *name, int64_t row, int64_t len, char *pos, char *fmt, ...) {
    va_list args = NULL;
    va_start(args, fmt);
    printf("FATAL: %s(%lld): ", name, row);
    vprintf(fmt, args);
    va_end(args);
    printf("\n%.*s", (int)len, pos);
    assert(0);
}

internal_proc void
report_warning(char *name, int64_t row, char *fmt, ...) {
    va_list args = NULL;
    va_start(args, fmt);
    printf("WARNUNG: %s(%lld): ", name, row);
    vprintf(fmt, args);
    va_end(args);
}

internal_proc void
report_info(char *name, int64_t row, char *fmt, ...) {
    va_list args = NULL;
    va_start(args, fmt);
    printf("INFO: %s(%lld): ", name, row);
    vprintf(fmt, args);
    va_end(args);
}

internal_proc void
report_blank(char *fmt, ...) {
    va_list args;
    va_start(args, fmt);
    vprintf(fmt, args);
    va_end(args);
}

internal_proc void
report_context(int64_t len, char *pos) {
    printf("\n%.*s", (int)len, pos);
}

#define error(Site, ...)   report_error(Site##.name, Site##.row, site_len(Site), site_pos(Site), __VA_ARGS__)
#define warning(Site, ...) report_warning(Site##.name, Site##.row, __VA_ARGS__)
#define info(Site, ...)    report_info(Site##.name, Site##.row, __VA_ARGS__)
#define report(...)        report_blank(__VA_ARGS__)
#define context(Site)      report_context(site_len(Site), site_pos(Site))

