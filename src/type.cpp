struct Type;
struct Sym;

internal_proc Type * resolve_typespec(Typespec *typespec);
internal_proc void   complete_type(Type *type);
internal_proc Type * type_incomplete(Sym *sym);
internal_proc Type * type_ptr(Type *elem);
internal_proc void   resolve_sym(Sym *sym);
internal_proc void   resolve_proc(Sym *sym);
internal_proc void   init_typeids();

enum Val_Kind {
    VAL_NONE,

    VAL_CHAR,
    VAL_BYTE,

    VAL_S8,
    VAL_S16,
    VAL_S32,
    VAL_INT = VAL_S32,
    VAL_S64,

    VAL_U8,
    VAL_U16,
    VAL_U32,
    VAL_UINT = VAL_U32,
    VAL_U64,

    VAL_F32,
    VAL_F64,

    VAL_STR,
    VAL_BOOL,
};

struct Val {
    Val_Kind kind;

    union {
        float    f32;
        double   f64;

        uint8_t  u8;
        uint16_t u16;
        uint32_t u32;
        uint64_t u64;

        int8_t   s8;
        int16_t  s16;
        int32_t  s32;
        int64_t  s64;

        uintptr_t ptr;

        char*    s;
        b32      b;
    };
};

global_var Val val_null = {};

internal_proc Val
val_new(Val_Kind kind) {
    Val result = { kind };

    return result;
}

internal_proc Val
val_f32(float value) {
    Val result = val_new(VAL_F32);

    result.f32 = value;

    return result;
}

internal_proc Val
val_f64(double value) {
    Val result = val_new(VAL_F64);

    result.f64 = value;

    return result;
}

internal_proc Val
val_s8(int8_t value) {
    Val result = val_new(VAL_S8);

    result.s8   = value;

    return result;
}

internal_proc Val
val_s16(int16_t value) {
    Val result = val_new(VAL_S16);

    result.s16  = value;

    return result;
}

internal_proc Val
val_s32(int32_t value) {
    Val result = val_new(VAL_S32);

    result.s32 = value;

    return result;
}

internal_proc Val
val_s64(int64_t value) {
    Val result = val_new(VAL_S64);

    result.s64 = value;

    return result;
}

internal_proc Val
val_int(int64_t value) {
    Val result = val_new(VAL_INT);

    result.s32  = (int32_t)value;

    return result;
}

internal_proc Val
val_u8(uint8_t value) {
    Val result = val_new(VAL_U8);

    result.u8 = value;

    return result;
}

internal_proc Val
val_u16(uint16_t value) {
    Val result = val_new(VAL_U16);

    result.u16 = value;

    return result;
}

internal_proc Val
val_u32(uint32_t value) {
    Val result = val_new(VAL_U32);

    result.u32 = value;

    return result;
}

internal_proc Val
val_u64(uint64_t value) {
    Val result = val_new(VAL_U64);

    result.u64 = value;

    return result;
}

internal_proc Val
val_str(char *value) {
    Val result = val_new(VAL_STR);

    result.s    = value;

    return result;
}

internal_proc b32
is_int(Val value) {
    return ( VAL_S8 <= value.kind && value.kind <= VAL_U64 );
}

internal_proc Val
val_add(Val a, Val b) {
    Val result = {};

    if ( a.kind == VAL_F32 || b.kind == VAL_F32 ) {
        result.kind = VAL_F32;

        if (a.kind == VAL_F32 && is_int(b)) {
            result.f32 = a.f32 + b.s32;
        } else if (a.kind == VAL_INT && b.kind == VAL_F32) {
            result.f32 = a.s32 + b.f32;
        }
    } else if ( a.kind == VAL_F64 || b.kind == VAL_F64 ) {
        result.kind = VAL_F64;

        if (a.kind == VAL_F64 && is_int(b)) {
            result.f64 = a.f64 + b.s64;
        } else if (is_int(a) && b.kind == VAL_F64) {
            result.f64 = a.s64 + b.f64;
        } else if (a.kind == VAL_F64 && b.kind == VAL_F64) {
            result.f64 = a.f64 + b.f64;
        }
    } else {
        result.kind = VAL_INT;
        result.s32  = a.s32 + b.s32;
    }

    return result;
}

internal_proc Val
val_sub(Val a, Val b) {
    Val result = {};

    if ( a.kind == VAL_F32 || b.kind == VAL_F32 ) {
        result.kind = VAL_F32;

        if (a.kind == VAL_F32 && b.kind == VAL_INT) {
            result.f32 = a.f32 - b.s32;
        } else if (a.kind == VAL_INT && b.kind == VAL_F32) {
            result.f32 = a.s32 - b.f32;
        }
    } else {
        result.kind = VAL_INT;

        result.s32 = a.s32 - b.s32;
    }

    return result;
}

internal_proc Val
val_mul(Val a, Val b) {
    Val result = {};

    if ( a.kind == VAL_F32 || b.kind == VAL_F32 ) {
        result.kind = VAL_F32;

        if (a.kind == VAL_F32 && b.kind == VAL_INT) {
            result.f32 = a.f32 * b.s32;
        } else if (a.kind == VAL_INT && b.kind == VAL_F32) {
            result.f32 = a.s32 * b.f32;
        }
    } else {
        result.kind = VAL_INT;

        result.s32 = a.s32 * b.s32;
    }

    return result;
}

internal_proc Val
val_div(Val a, Val b) {
    Val result = {};

    if ( a.kind == VAL_F32 || b.kind == VAL_F32 ) {
        result.kind = VAL_F32;

        if (a.kind == VAL_F32 && b.kind == VAL_INT) {
            result.f32 = a.f32 / b.s32;
        } else if (a.kind == VAL_INT && b.kind == VAL_F32) {
            result.f32 = a.s32 / b.f32;
        }
    } else if ( a.kind == VAL_F64 || b.kind == VAL_F64 ) {
        result.kind = VAL_F64;

        if (a.kind == VAL_F64 && is_int(b)) {
            result.f64 = a.f64 / b.s64;
        } else if (is_int(a) && b.kind == VAL_F64) {
            result.f64 = a.s64 / b.f64;
        } else if (a.kind == VAL_F64 && b.kind == VAL_F64) {
            result.f64 = a.f64 / b.f64;
        }
    } else {
        result.kind = VAL_INT;

        result.s32 = a.s32 / b.s32;
    }

    return result;
}

internal_proc Val
val_mod(Val a, Val b) {
    Val result = {};

    assert(a.kind == VAL_INT && b.kind == VAL_INT);
    result.kind = VAL_INT;
    result.s32 = a.s32 % b.s32;

    return result;
}

internal_proc Val
operator+(Val a, Val b) {
    return val_add(a, b);
}

internal_proc Val
operator-(Val a, Val b) {
    return val_sub(a, b);
}

internal_proc Val
operator-(Val a) {
    if ( a.kind == VAL_F32 || a.kind == VAL_F64 ) {
        return val_f64(-a.f64);
    }

    if ( a.kind >= VAL_S8 && a.kind <= VAL_S64 ) {
        return val_s64(-a.s64);
    }

    if ( a.kind >= VAL_U8 && a.kind <= VAL_U64 ) {
        return a;
    }

    return a;
}

internal_proc Val
operator*(Val a, Val b) {
    return val_mul(a, b);
}

internal_proc Val
operator/(Val a, Val b) {
    return val_div(a, b);
}

internal_proc Val
val_bool(Token_Kind op, Val a, Val b) {
    assert(a.kind == b.kind);
    Val result = {VAL_BOOL};

    switch ( op ) {
        case T_EQ: {
            if ( a.kind == VAL_INT ) {
                result.b = (a.s32 == b.s32);
            } else if ( a.kind == VAL_BOOL ) {
                result.b = (a.b == b.b);
            } else {
                assert(!"nicht unterstützter datentyp für vergleichsoperator!");
            }
        } break;

        case T_NEQ: {
            if ( a.kind == VAL_INT ) {
                result.b = (a.s32 != b.s32);
            } else if ( a.kind == VAL_BOOL ) {
                result.b = (a.b != b.b);
            } else {
                assert(!"nicht unterstützter datentyp für vergleichsoperator!");
            }
        } break;
    }

    return result;
}

internal_proc Val
val_op(Token_Kind op, Val left, Val right) {
    switch ( op ) {
        case T_PLUS: {
            return val_add(left, right);
        } break;

        case T_MINUS: {
            return val_sub(left, right);
        } break;

        case T_MUL: {
            return val_mul(left, right);
        } break;

        case T_DIV: {
            return val_div(left, right);
        } break;

        case T_MODULO: {
            return val_mod(left, right);
        } break;

        case T_NEQ:
        case T_EQ: {
            return val_bool(op, left, right);
        } break;

        default: {
            assert(0);
            return val_null;
        } break;
    }
}

enum Sym_State {
    SYM_UNRESOLVED,
    SYM_RESOLVING,
    SYM_RESOLVED,
};

enum Sym_Kind {
    SYM_NONE,
    SYM_VAR,
    SYM_CONST,
    SYM_PROC,
    SYM_TYPE,
};

struct Sym {
    Sym_State state;
    Sym_Kind kind;

    char *name;
    Decl *decl;
    Type *type;
    Val value;
};

enum { MAX_LOCAL_SYMS = 1024 };
global_var Sym **  ordered_symbols;
global_var Map     global_symbols_map;
global_var Sym **  global_symbols_buf;
global_var Sym     local_symbols[MAX_LOCAL_SYMS];
global_var Sym *   local_symbols_end = local_symbols;

internal_proc Sym *
sym_new(Sym_Kind kind, char *name, Decl *decl, Sym_State state = SYM_UNRESOLVED) {
    Sym *result = (Sym *)calloc(1, sizeof(Sym));

    result->kind  = kind;
    result->state = state;
    result->name  = name;
    result->decl  = decl;

    return result;
}

internal_proc Sym *
sym_get_local(char *name) {
    for (Sym *it = local_symbols_end; it != local_symbols; it--) {
        Sym *sym = it-1;
        if (sym->name == name) {
            return sym;
        }
    }
    return NULL;
}

internal_proc Sym *
sym_get(char *name) {
    Sym *sym = sym_get_local(name);
    return sym ? sym : (Sym *)map_get(&global_symbols_map, (void *)name);
}

internal_proc Sym *
sym_decl(Expr *expr, Decl *decl) {
    Sym_Kind kind = SYM_NONE;

    switch ( decl->kind ) {
        case DECL_STRUCT:
        case DECL_UNION:
        case DECL_ENUM:
        case DECL_TYPEDEF: {
            kind = SYM_TYPE;
        } break;

        case DECL_VAR: {
            kind = SYM_VAR;
        } break;

        case DECL_CONST: {
            kind = SYM_CONST;
        } break;

        case DECL_PROC: {
            kind = SYM_PROC;
        } break;

        default: {
            assert(0);
        } break;
    }

    assert(expr->kind == EXPR_NAME);
    Sym *result = sym_new(kind, expr->expr_name.value, decl);
    if ( decl->kind == DECL_UNION || decl->kind == DECL_STRUCT || decl->kind == DECL_ENUM ) {
        result->state = SYM_RESOLVED;
        result->type  = type_incomplete(result);
    }

    return result;
}

internal_proc void
sym_global_put(Sym *sym) {
    if ( map_get(&global_symbols_map, (void *)sym->name) ) {
        error(sym->decl->site, "mehrfache deklaration von '%s'", sym->name);
    }

    map_put(&global_symbols_map, (void *)sym->name, sym);
    buf_push(global_symbols_buf, sym);
}

internal_proc Sym *
sym_global_type(char *name, Type *type) {
    Sym *sym = sym_new(SYM_TYPE, name, NULL);
    sym->state = SYM_RESOLVED;
    sym->type = type;

    sym_global_put(sym);

    return sym;
}

internal_proc Sym *
sym_global_decl(Expr *name, Decl *decl) {
    Sym *sym = sym_decl(name, decl);
    sym_global_put(sym);
    sym->decl = decl;

    return sym;
}

internal_proc void
sym_global_decls(Decl **decls) {
    for ( int i = 0; i < buf_len(decls); ++i ) {
        Decl *decl = decls[i];

        for ( int j = 0; j < decl->num_names; ++j ) {
            sym_global_decl(decl->names[j], decl);
        }
    }
}

internal_proc b32
sym_push_const(char *name, Type *type, Decl *decl, Val value) {
    if (sym_get_local(name)) {
        return false;
    }

    if ( local_symbols_end == local_symbols + MAX_LOCAL_SYMS ) {
        assert(!"zu viele lokale deklarationen!");
    }

    Sym sym = {};

    sym.name  = name;
    sym.kind  = SYM_CONST;
    sym.state = SYM_RESOLVED;
    sym.type  = type;
    sym.decl  = decl;
    sym.value = value;

    *local_symbols_end++ = sym;

    return true;
}

internal_proc b32
sym_push_var(char *name, Type *type, Decl *decl = NULL) {
    if (sym_get_local(name)) {
        return false;
    }

    if ( local_symbols_end == local_symbols + MAX_LOCAL_SYMS ) {
        assert(!"zu viele lokale deklarationen!");
    }

    Sym sym = {};

    sym.name  = name;
    sym.kind  = SYM_VAR;
    sym.state = SYM_RESOLVED;
    sym.type  = type;
    sym.decl  = decl;

    *local_symbols_end++ = sym;

    return true;
}

internal_proc Sym *
scope_enter() {
    return local_symbols_end;
}

internal_proc void
scope_leave(Sym *ptr) {
    local_symbols_end = ptr;
}

internal_proc Sym *
sym_install(Expr *expr, Type *type) {
    assert(sym_get(expr->expr_name.value) == NULL);
    assert(expr->kind == EXPR_NAME);

    Sym *result = sym_new(SYM_TYPE, expr->expr_name.value, NULL, SYM_RESOLVED);

    result->type = type;
    map_put(&global_symbols_map, result->name, result);
    buf_push(global_symbols_buf, result);

    return result;
}

internal_proc Sym *
sym_install_intrinsic(char *name, Type *type) {
    Expr *type_name = expr_name(zero_site, intern_str(name));
    return sym_install(type_name, type);
}

internal_proc void
finalize_sym(Sym *sym) {
    resolve_sym(sym);
    if ( sym->kind == SYM_TYPE ) {
        complete_type(sym->type);
    } else if ( sym->kind == SYM_PROC ) {
        resolve_proc(sym);
    }
}

void finalize_syms(void) {
    for ( int i = 0; i != buf_len(global_symbols_buf); ++i ) {
        Sym *sym = global_symbols_buf[i];
        if ( sym->decl ) {
            finalize_sym(sym);
        }
    }
}

struct Type_Field {
    char *name;
    Type *type;
    Val value;
    size_t offset;
};

enum Type_Kind {
    TYPE_NONE,
    TYPE_INCOMPLETE,
    TYPE_COMPLETING,

    /* int, arithmetic, scalar */
    TYPE_CHAR,
    TYPE_U8,
    TYPE_U16,
    TYPE_U32,
    TYPE_UINT = TYPE_U32,
    TYPE_U64,

    TYPE_S8,
    TYPE_S16,
    TYPE_S32,
    TYPE_INT = TYPE_S32,
    TYPE_S64,

    TYPE_ENUM,
    TYPE_BOOL,
    /* ende: int */

    TYPE_F32,
    TYPE_FLOAT = TYPE_F32,
    TYPE_F64,
    /* ende: arithmetic */

    TYPE_PTR,
    TYPE_PROC,
    /* ende: scalar */

    TYPE_ARRAY,
    TYPE_STRUCT,
    TYPE_UNION,
    TYPE_STR,
    TYPE_VOID,
    TYPE_VARIADIC,

    MAX_TYPES,
};

typedef uint32_t Type_Id;
struct Type {
    Type_Kind kind;
    size_t size;
    size_t align;
    Type_Id type_id;
    b32 is_foreign;

    Sym *sym;

    union {
        struct {
            Type *base;
        } type_ptr;
        struct {
            Type *base;
            size_t index;
            b32 is_inferred;
        } type_array;
        struct {
            Type_Field *fields;
            size_t num_fields;
        } type_aggr;
        struct {
            Type **params;
            size_t num_params;
            Type **rets;
            size_t num_rets;
            b32 is_variadic;
        } type_proc;
    };
};

Map typeid_map;

internal_proc void
register_typeid(Type *type) {
    map_put(&typeid_map, (void *)(uintptr_t)type->type_id, type);
}

#define TYPE(name, kind, size, type_id) \
    global_var Type name##_val = {kind, size, size, type_id}; \
    global_var Type *name = &name##_val

TYPE(type_void,     TYPE_VOID,     0, 1);
TYPE(type_bool,     TYPE_BOOL,     4, 2);
TYPE(type_char,     TYPE_CHAR,     1, 3);

TYPE(type_u8,       TYPE_U8,       1, 4);
TYPE(type_u16,      TYPE_U16,      2, 5);
TYPE(type_u32,      TYPE_U32,      4, 6);
TYPE(type_u64,      TYPE_U64,      8, 7);

TYPE(type_s8,       TYPE_S8,       1, 8);
TYPE(type_s16,      TYPE_S16,      2, 9);
TYPE(type_s32,      TYPE_S32,      4, 10);
TYPE(type_s64,      TYPE_S64,      8, 11);

TYPE(type_f32,      TYPE_F32,      4, 12);
TYPE(type_f64,      TYPE_F64,      8, 13);
TYPE(type_string,   TYPE_STR,      8, 14);
TYPE(type_variadic, TYPE_VARIADIC, 0, 15);

Type_Id next_type_id = 16;

#undef TYPE

internal_proc Type *
typeinfo(Type_Id id) {
    if ( id == 0 ) {
        return NULL;
    }
    return (Type *)map_get(&typeid_map, (void *)(uintptr_t)id);
}

internal_proc Type *
type_new(Type_Kind kind) {
    Type *result = (Type *)xcalloc(1, sizeof(Type));

    result->kind = kind;
    result->is_foreign = false;
    result->type_id = next_type_id++;

    register_typeid(result);

    return result;
}

internal_proc b32
is_signed(Type *type) {
    b32 result = (TYPE_S8 <= type->kind && type->kind <= TYPE_S64);

    return result;
}

internal_proc b32
is_int(Type *type) {
    b32 result = (TYPE_CHAR <= type->kind && type->kind <= TYPE_BOOL);

    return result;
}

internal_proc b32
is_float(Type *type) {
    b32 result = (TYPE_F32 <= type->kind && type->kind <= TYPE_F64);

    return result;
}

internal_proc b32
is_arithmetic(Type *type) {
    b32 result = (TYPE_CHAR <= type->kind && type->kind <= TYPE_F64);

    return result;
}

internal_proc b32
is_aggregate(Type *type) {
    return ( type->kind == TYPE_UNION || type->kind == TYPE_STRUCT || type->kind == TYPE_ENUM );
}

internal_proc b32
is_scalar(Type *type) {
    b32 result = TYPE_CHAR <= type->kind && type->kind <= TYPE_PROC;

    return result;
}

internal_proc b32
is_ptr(Type *type) {
    return type->kind == TYPE_PTR;
}

internal_proc b32
is_array(Type *type) {
    return type->kind == TYPE_ARRAY;
}

internal_proc b32
is_proc(Type *type) {
    b32 result = type->kind == TYPE_PROC;

    return result;
}

internal_proc b32
is_variadic(Type *type) {
    assert(is_proc(type));
    b32 result = type->type_proc.is_variadic;

    return result;
}

internal_proc Type *
unsigned_type(Type *type) {
    switch (type->kind) {
        case TYPE_CHAR:
        case TYPE_S8:
        case TYPE_U8: {
            return type_u8;
        } break;

        case TYPE_S16:
        case TYPE_U16: {
            return type_u16;
        } break;

        case TYPE_S32:
        case TYPE_U32: {
            return type_u32;
        } break;

        case TYPE_S64:
        case TYPE_U64: {
            return type_u64;
        } break;

        default: {
            assert(0);
            return NULL;
        } break;
    }
}

internal_proc int
type_rank(Type *type) {
    int type_ranks[MAX_TYPES] = {};

    type_ranks[TYPE_CHAR] = 1;
    type_ranks[TYPE_S8]   = 1;
    type_ranks[TYPE_U8]   = 1;
    type_ranks[TYPE_S16]  = 2;
    type_ranks[TYPE_U16]  = 2;
    type_ranks[TYPE_S32]  = 3;
    type_ranks[TYPE_U32]  = 3;
    type_ranks[TYPE_S64]  = 5;
    type_ranks[TYPE_U64]  = 5;

    return type_ranks[type->kind];
}

internal_proc size_t
type_sizeof(Type *type) {
    assert(type->kind > TYPE_COMPLETING);
    assert(type->size != 0);
    return type->size;
}

struct Operand {
    Type *type;
    b32 is_lvalue;
    b32 is_const;
    Val value;
};

internal_proc Operand resolve_expr(Expr *expr);
internal_proc Operand resolve_expected_expr(Expr *expr, Type *expected_type);

Operand operand_null;

internal_proc Operand
operand_new(Type *type, b32 is_lvalue = false, b32 is_const = false,
        Val value = {})
{
    Operand result = {};

    result.type      = type;
    result.is_lvalue = is_lvalue;
    result.is_const  = is_const;
    result.value     = value;

    return result;
}

internal_proc Operand
operand_lvalue(Type *type) {
    return operand_new(type, true);
}

internal_proc Operand
operand_rvalue(Type *type) {
    return operand_new(type);
}

internal_proc Operand
operand_const(Type *type, Val value) {
    return operand_new(type, false, true, value);
}

#define CASE(k, t) \
    case k: \
        switch (type->kind) { \
        case TYPE_CHAR: \
            operand->value.s8  = (char)operand->value.t; \
            break; \
        case TYPE_U8: \
            operand->value.u8  = (unsigned char)operand->value.t; \
            break; \
        case TYPE_S8: \
            operand->value.s8  = (signed char)operand->value.t; \
            break; \
        case TYPE_S16: \
            operand->value.s16 = (short)operand->value.t; \
            break; \
        case TYPE_U16: \
            operand->value.u16 = (unsigned short)operand->value.t; \
            break; \
        case TYPE_S32: \
            operand->value.s32 = (int)operand->value.t; \
            break; \
        case TYPE_U32: \
            operand->value.u32 = (unsigned)operand->value.t; \
            break; \
        case TYPE_S64: \
            operand->value.s64 = (long long)operand->value.t; \
            break; \
        case TYPE_U64: \
            operand->value.u64 = (unsigned long long)operand->value.t; \
            break; \
        case TYPE_F32: \
            operand->value.f32 = (float)operand->value.t; \
            break; \
        case TYPE_F64: \
            operand->value.f64 = (double)operand->value.t; \
            break; \
        case TYPE_ENUM: \
            operand->value.s64 = (int64_t)operand->value.t; \
            break; \
        case TYPE_PTR: \
            operand->value.ptr = (uintptr_t)operand->value.t; \
            break; \
        default: \
            assert(0); \
            break; \
        } \
        break;

internal_proc b32 is_convertible(Operand *operand, Type *dest);
internal_proc b32
is_castable(Operand *operand, Type *dest) {
    Type *src = operand->type;
    if (is_convertible(operand, dest)) {
        return true;
    } else if (is_int(dest)) {
        return is_ptr(src);
    } else if (is_int(src)) {
        return is_ptr(dest);
    } else if (is_ptr(dest) && is_ptr(src)) {
        return true;
    } else {
        return false;
    }
}

internal_proc b32
cast_operand(Operand *operand, Type *type) {
    Type *qual_type = type;

    if (operand->type != type) {
        if (!is_castable(operand, type)) {
            return false;
        }

        if (operand->is_const) {
            if (is_float(operand->type)) {
                operand->is_const = !is_int(type);
            } else {
                switch (operand->type->kind) {
                    CASE(TYPE_BOOL,   b)
                    CASE(TYPE_U8,    u8)
                    CASE(TYPE_U16,  u16)
                    CASE(TYPE_U32,  u32)
                    CASE(TYPE_U64,  u64)
                    CASE(TYPE_S8,    s8)
                    CASE(TYPE_S16,  s16)
                    CASE(TYPE_S32,  s32)
                    CASE(TYPE_S64,  s64)
                    CASE(TYPE_ENUM, s32)
                    CASE(TYPE_PTR,  ptr)
                    default: {
                        operand->is_const = false;
                    } break;
                }
            }
        }
    }
    operand->type = qual_type;
    return true;
}

internal_proc b32
is_null(Operand operand) {
    if (operand.is_const && (is_ptr(operand.type) || is_int(operand.type))) {
        cast_operand(&operand, type_u64);
        return operand.value.u64 == 0;
    } else {
        return false;
    }
}

internal_proc b32
is_convertible(Operand *operand, Type *dest) {
    Type *src = operand->type;
    if (src == dest) {
        return true;
    } else if (is_arithmetic(src) && is_arithmetic(dest)) {
        return true;
    } else if (is_ptr(dest) && is_ptr(src)) {
        return dest->type_ptr.base == type_void || src->type_ptr.base == type_void;
    } else if ( is_int(dest) && src->kind == TYPE_ENUM ) {
        return true;
    } else if (is_ptr(dest) && is_null(*operand)) {
        return true;
    } else {
        return false;
    }
}

internal_proc b32
convert_operand(Operand *operand, Type *type) {
    if (is_convertible(operand, type)) {
        cast_operand(operand, type);

        return true;
    }

    return false;
}

#undef CASE

internal_proc Val
convert_const(Type *dest_type, Type *src_type, Val src_val) {
    Operand operand = operand_const(src_type, src_val);
    convert_operand(&operand, dest_type);

    return operand.value;
}

internal_proc void
promote_operand(Operand *operand) {
    switch (operand->type->kind) {
        case TYPE_S8:
        case TYPE_U8:
        case TYPE_S16:
        case TYPE_U16: {
            convert_operand(operand, type_s32);
        } break;

        default: {
        } break;
    }
}

internal_proc Type *
promote_type(Type *type) {
    if ( type == type_char ) return type_u8;

    return type;
}

internal_proc void
unify_arithmetic_operands(Operand *left, Operand *right) {
    if (left->type == type_f64) {
        convert_operand(right, type_f64);
    } else if (right->type == type_f64) {
        convert_operand(left, type_f64);
    } else if (left->type == type_f32) {
        convert_operand(right, type_f32);
    } else if (right->type == type_f32) {
        convert_operand(left, type_f32);
    } else {
        promote_operand(left);
        promote_operand(right);
        if (left->type != right->type) {
            if (is_signed(left->type) == is_signed(right->type)) {
                if (type_rank(left->type) <= type_rank(right->type)) {
                    convert_operand(left, right->type);
                } else {
                    convert_operand(right, left->type);
                }
            } else if (is_signed(left->type) && type_rank(right->type) >= type_rank(left->type)) {
                convert_operand(left, right->type);
            } else if (is_signed(right->type) && type_rank(left->type) >= type_rank(right->type)) {
                convert_operand(right, left->type);
            } else if (is_signed(left->type) && type_sizeof(left->type) > type_sizeof(right->type)) {
                convert_operand(right, left->type);
            } else if (is_signed(right->type) && type_sizeof(right->type) > type_sizeof(left->type)) {
                convert_operand(left, right->type);
            } else {
                Type *type = unsigned_type(is_signed(left->type) ? left->type : right->type);
                convert_operand(left, type);
                convert_operand(right, type);
            }
        }
    }
    assert(left->type == right->type);
}

#define PTR_SIZE 8
#define PTR_ALIGN 8

global_var Map cached_ptr_types;

struct Cached_Array_Type {
    Type *base;
    size_t index;
    Type *type_array;
};

global_var Cached_Array_Type *cached_array_types;

struct Cached_Proc_Type {
    Type **params;
    size_t num_params;

    Type **rets;
    size_t num_rets;

    Type *type_proc;
};

global_var Cached_Proc_Type *cached_proc_types;

internal_proc size_t
type_alignof(Type *type) {
    return type->align;
}

internal_proc Type_Field
type_field(Expr *expr, Type *type, Val value = val_null) {
    assert(expr->kind == EXPR_NAME);
    Type_Field result = {};

    result.name  = expr->expr_name.value;
    result.type  = type;
    result.value = value;

    return result;
}

internal_proc void
type_complete_struct(Type *type, Type_Field *fields, size_t num_fields) {
    assert(type->kind == TYPE_COMPLETING);

    type->kind = TYPE_STRUCT;
    type->size = 0;

    size_t offset = 0;
    size_t align = 0;
    for ( size_t i = 0; i < buf_len(fields); ++i ) {
        type->size += fields[i].type->size;
        type->align = MAX(type->size, type_alignof(fields[i].type));
        fields[i].offset = offset;
        offset += fields[i].type->size;
    }

    type->type_aggr.fields = (Type_Field *)memdup(fields, num_fields * sizeof(*fields));
    type->type_aggr.num_fields = num_fields;
}

internal_proc void
type_complete_union(Type *type, Type_Field *fields, size_t num_fields) {
    assert(type->kind == TYPE_COMPLETING);

    type->kind = TYPE_UNION;
    type->size = 0;

    size_t offset = 0;
    size_t align = 0;
    for ( size_t i = 0; i < buf_len(fields); ++i ) {
        if ( fields[i].type->size > type->size ) {
            type->size = fields[i].type->size;
        }

        type->align = MAX(type->size, type_alignof(fields[i].type));
        fields[i].offset = offset;
        offset += fields[i].type->size;
    }

    type->type_aggr.fields = (Type_Field *)memdup(fields, num_fields * sizeof(*fields));
    type->type_aggr.num_fields = num_fields;
}

internal_proc void
type_complete_enum(Type *type, Type_Field *fields, size_t num_fields) {
    assert(type->kind == TYPE_COMPLETING);

    type->kind = TYPE_ENUM;
    type->size = num_fields * type_s32->size;

    size_t offset = 0;
    for ( size_t i = 0; i < buf_len(fields); ++i ) {
        fields[i].offset = offset;
        offset += fields[i].type->size;
    }

    type->type_aggr.fields = (Type_Field *)memdup(fields, num_fields * sizeof(*fields));
    type->type_aggr.num_fields = num_fields;
}

internal_proc b32
has_dups(Type_Field *fields, size_t num_fields) {
    for ( size_t i = 0; i < num_fields; i++ ) {
        for ( size_t j = i + 1; j < num_fields; j++ ) {
            if ( fields[i].name == fields[j].name ) {
                return true;
            }
        }
    }
    return false;
}

internal_proc void
complete_type(Type *type) {
    if ( type->kind == TYPE_COMPLETING ) {
        error(type->sym->decl->site, "zirkuläre abhängigkeit festgestellt!");
        return;
    } else if (type->kind != TYPE_INCOMPLETE) {
        return;
    }

    type->kind = TYPE_COMPLETING;
    Decl *decl = type->sym->decl;
    assert(decl->kind == DECL_UNION || decl->kind == DECL_STRUCT || decl->kind == DECL_ENUM);

    Type_Field *fields = NULL;

    for ( size_t i = 0; i < decl->decl_aggr.num_fields; i++ ) {
        Aggregate_Field field = decl->decl_aggr.fields[i];

        Type *field_type = 0;
        if ( field.typespec ) {
            field_type = resolve_typespec(field.typespec);
            field.typespec->type = field_type;
        }

        Operand operand = {};
        if ( field.expr ) {
            operand = resolve_expr(field.expr);
            field_type = operand.type;
        }

        if ( !field_type ) {
            error(field.site, "datentyp des feldes %s konnte nicht ermittelt werden", field.names[0]->expr_name.value);
        }

        complete_type(field_type);

        int64_t current_value = 0;
        for ( size_t j = 0; j < field.num_names; ++j ) {
            if ( field.expr ) {
                current_value = field.expr->expr_int.value;
            }

            buf_push(fields, type_field(field.names[j], field_type, val_s64(current_value)));
            current_value++;
        }

        if ( field.flags & PF_USING ) {
            switch ( field_type->kind ) {
                case TYPE_STRUCT: {
                    for ( int j = 0; j < field_type->type_aggr.num_fields; ++j ) {
                        buf_push(fields, field_type->type_aggr.fields[j]);
                    }
                } break;

                case TYPE_ENUM: {
                    for ( int j = 0; j < field_type->type_aggr.num_fields; ++j ) {
                        buf_push(fields, field_type->type_aggr.fields[j]);
                    }
                } break;

                default: {
                    error(field.site, "using wird auf diesem datentyp nicht unterstützt!");
                } break;
            }
        }
    }

    if ( decl->kind == DECL_ENUM ) {
        buf_push(fields, type_field(expr_name(zero_site, intern_str("count")),
                    type_s64, val_s64(decl->decl_aggr.num_fields)));
    }

    if ( !buf_len(fields) ) {
        error(decl->site, "datenstruktur muss mindestens ein feld enthalten!");
    }

    if ( has_dups(fields, buf_len(fields)) ) {
        error(decl->site, "felder mehrfach definiert!");
    }

    if ( decl->kind == DECL_STRUCT ) {
        type_complete_struct(type, fields, buf_len(fields));
    } else if ( decl->kind == DECL_ENUM ) {
        type_complete_enum(type, fields, buf_len(fields));
    } else if ( decl->kind == DECL_UNION ) {
        type_complete_union(type, fields, buf_len(fields));
    }

    buf_push(ordered_symbols, type->sym);
}

internal_proc Type *
type_incomplete(Sym *sym) {
    Type *result = type_new(TYPE_INCOMPLETE);
    result->sym = sym;

    return result;
}

internal_proc Type *
type_ptr(Type *type) {
    Type *cached_type = (Type *)map_get(&cached_ptr_types, type);
    if ( cached_type ) {
        return cached_type;
    }

    Type *result = type_new(TYPE_PTR);
    result->size = PTR_SIZE;
    result->align = PTR_ALIGN;
    result->type_ptr.base = type;
    map_put(&cached_ptr_types, type, result);

    return result;
}

internal_proc Type *
type_array(Type *base, size_t index, b32 is_inferred = false) {
    /* @FIX(serge): type_array.index wird richtig gespeichert, aber beim lesen kommt ein anderer index
     *              als der gespeicherte. wird er irgendwo nachträglich geändert?!                      */
    for ( size_t i = 0; i < buf_len(cached_array_types); ++i ) {
        Type *cached_type = cached_array_types[i].type_array;
        if ( cached_type->type_array.base == base && cached_type->type_array.index == index ) {
            return cached_type;
        }
    }

    complete_type(base);

    Type *result = type_new(TYPE_ARRAY);

    result->size                   = index * base->size;
    result->align                  = base->align;
    result->type_array.base        = base;
    result->type_array.index       = index;
    result->type_array.is_inferred = is_inferred;

    Cached_Array_Type t = {};

    t.base = base;
    t.index = index;
    t.type_array = result;

    buf_push(cached_array_types, t);

    return result;
}

internal_proc Type *
type_struct(Type_Field *fields, size_t num_fields) {
    Type *result = type_new(TYPE_STRUCT);

    result->size = 0;
    for ( size_t i = 0; i < buf_len(fields); ++i ) {
        result->size += fields[i].type->size;
    }

    result->type_aggr.fields = (Type_Field *)memdup(fields, num_fields*sizeof(Type_Field));
    result->type_aggr.num_fields = num_fields;

    return result;
}

internal_proc Type *
type_proc(Type **params, size_t num_params, Type **rets, size_t num_rets,
        b32 is_variadic, b32 is_foreign)
{
    for ( size_t i = 0; i < buf_len(cached_proc_types); ++i ) {
        if ( cached_proc_types[i].num_params == num_params &&
             cached_proc_types[i].num_rets == num_rets )
        {
            uint32_t match = 1;
            for ( size_t j = 0; j < num_params; ++j ) {
                if ( cached_proc_types[i].params[j] != params[j] ) {
                    match = 0;
                    break;
                }
            }

            for ( size_t j = 0; j < num_rets; ++j ) {
                if ( cached_proc_types[i].rets[j] != rets[j] ) {
                    match = 0;
                    break;
                }
            }

            if ( match ) {
                return cached_proc_types[i].type_proc;
            }
        }
    }

    Type *result = type_new(TYPE_PROC);

    result->size = PTR_SIZE;
    result->align = PTR_ALIGN;
    result->is_foreign = is_foreign;
    result->type_proc.params = (Type **)memdup(params, num_params*sizeof(Type *));
    result->type_proc.num_params = num_params;
    result->type_proc.rets   = (Type **)memdup(rets, num_rets*sizeof(Type *));
    result->type_proc.num_rets = num_rets;
    result->type_proc.is_variadic = is_variadic;

    Cached_Proc_Type t = { params, num_params, rets, num_rets, result };
    buf_push(cached_proc_types, t);

    return result;
}

internal_proc void
init_typeids() {
    register_typeid(type_void);
    register_typeid(type_bool);
    register_typeid(type_char);
    register_typeid(type_string);
    register_typeid(type_u8);
    register_typeid(type_u16);
    register_typeid(type_u32);
    register_typeid(type_u64);
    register_typeid(type_s8);
    register_typeid(type_s16);
    register_typeid(type_s32);
    register_typeid(type_s64);
    register_typeid(type_f32);
    register_typeid(type_f64);
}

