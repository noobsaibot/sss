internal_proc void print(Decl *decl);
internal_proc void print(Expr *expr);
internal_proc void print(Typespec *typespec);
internal_proc void print(Stmt *stmt);
internal_proc void print(Stmt_Block block);
internal_proc void print(Type *type);

/* to_string {{{ */
internal_proc char *
to_string(Token_Kind t) {
    switch (t) {
        case T_EQ: {
            return "==";
        } break;

        case T_LT: {
            return "<";
        } break;

        case T_LEQ: {
            return "<=";
        } break;

        case T_GT: {
            return ">";
        } break;

        case T_GEQ: {
            return ">=";
        } break;

        case T_NEQ: {
            return "!=";
        } break;

        case T_AND: {
            return "&&";
        } break;

        case T_OR: {
            return "||";
        } break;

        case T_XOR: {
            return "^";
        } break;

        case T_NEGATE: {
            return "~";
        } break;

        case T_EQL_ASSIGN: {
            return "=";
        } break;

        case T_AND_ASSIGN: {
            return "&=";
        } break;

        case T_OR_ASSIGN: {
            return "|=";
        } break;

        case T_ADD_ASSIGN: {
            return "+=";
        } break;

        case T_SUB_ASSIGN: {
            return "-=";
        } break;

        case T_MUL_ASSIGN: {
            return "*=";
        } break;

        case T_DIV_ASSIGN: {
            return "/=";
        } break;

        case T_XOR_ASSIGN: {
            return "^=";
        } break;

        case T_MODULO_ASSIGN: {
            return "%=";
        } break;

        case T_DEREF: {
            return "<<";
        } break;

        case T_NOT: {
            return "not";
        } break;

        case T_PLUS: {
            return "+";
        } break;

        case T_MINUS: {
            return "-";
        } break;

        case T_MUL: {
            return "*";
        } break;

        case T_DIV: {
            return "/";
        } break;

        case T_RANGE: {
            return "range";
        } break;

        case T_FIELD: {
            return ".";
        } break;

        case T_MODULO: {
            return "%";
        } break;

        default: {
            return "?";
        } break;
    }
}
/* }}} */

global_var uint32_t indent = 0;

internal_proc void
inc_indent(uint32_t d = 1) {
    indent += d;
}

internal_proc void
dec_indent(uint32_t d = 1) {
    indent -= d;
}

internal_proc void
print_indent() {
    for ( size_t i = 0; i < indent; ++i ) {
        printf("    ");
    }
}

internal_proc void
print(Val val) {
    switch ( val.kind ) {
        case VAL_S8: {
            printf("%d", val.s8);
        } break;

        case VAL_S16: {
            printf("%d", val.s16);
        } break;

        case VAL_S32: {
            printf("%d", val.s32);
        } break;

        case VAL_S64: {
            printf("%lld", val.s64);
        } break;

        case VAL_U8: {
            printf("%d", val.u8);
        } break;

        case VAL_U16: {
            printf("%d", val.u16);
        } break;

        case VAL_U32: {
            printf("%d", val.u32);
        } break;

        case VAL_U64: {
            printf("%lld", val.u64);
        } break;

        case VAL_F32: {
            printf("%f", val.f32);
        } break;

        case VAL_F64: {
            printf("%g", val.f64);
        } break;

        case VAL_STR: {
            printf("%s", val.s);
        } break;

        case VAL_BOOL: {
            printf("%d", val.b);
        } break;

        default: {
            printf("none");
        } break;
    }
}

internal_proc void
print(Site site) {
    printf("(* %s %zd:%zd *)", site.name, site.row, site.col);
}

internal_proc void
print(Note *note) {
    print_indent();
    printf("(@%s)\n", note->tag);
}

internal_proc void
print(Call_Arg *arg) {
    printf("(");
    if ( arg->name ) {
        print(arg->name);
        printf(" ");
    }
    print(arg->expr);
    printf(")");
}

internal_proc void
print(Type_Field *field) {
    printf("(%s ", field->name);
    print(field->type);
    printf(")");
}

internal_proc void
print(Type *type) {
    switch ( type->kind ) {
        case TYPE_INT: {
            printf("u32");
        } break;

        case TYPE_FLOAT: {
            printf("f32");
        } break;

        case TYPE_STR: {
            printf("string");
        } break;

        case TYPE_CHAR: {
            printf("char");
        } break;

        case TYPE_VOID: {
            printf("void");
        } break;

        case TYPE_PTR: {
            printf("* ");
            print(type->type_ptr.base);
        } break;

        case TYPE_ARRAY: {
            printf("[%zd] ", type->type_array.index);
            print(type->type_array.base);
        } break;

        case TYPE_ENUM:
        case TYPE_STRUCT: {
            printf("%s ", type->sym->name);
            for ( size_t i = 0; i < type->type_aggr.num_fields; ++i ) {
                print(&type->type_aggr.fields[i]);
                if ( i < type->type_aggr.num_fields-1 ) {
                    printf(" ");
                }
            }
        } break;

        case TYPE_PROC: {
            printf("proc");
        } break;

        default: {
            assert(0);
        } break;
    }
}

internal_proc void
print(Sym *sym) {
    printf("(%s ", sym->name);
    print(sym->type);
    printf(" ");
    print(sym->value);
    printf(")");
}

internal_proc void
print(Switch_Case *switch_case) {
    if ( !switch_case ) return;

    print_indent();

    if ( switch_case->is_default ) {
        printf("(default ");
    } else {
        printf("(case ");
        for ( size_t i = 0; i < switch_case->num_exprs; ++i ) {
            print(switch_case->exprs[i]);
            if ( i < switch_case->num_exprs-1 ) {
                printf(", ");
            }
        }
    }

    printf("\n");

    inc_indent();
    print(switch_case->block);
    dec_indent();

    printf(")");
}

internal_proc void
print(Proc_Param param) {
    printf("(");
    if ( param.flags & PF_USING ) {
        printf("using ");
    }

    printf("%s: ", param.name);
    print(param.typespec);

    if ( param.default_value ) {
        printf(" = ");
        print(param.default_value);
    }
    printf(")");
}

internal_proc void
print_names(Expr **names, size_t num_names) {
    for ( size_t i = 0; i < num_names; ++i ) {
        print(names[i]);

        if ( i < num_names-1 ) {
            printf(", ");
        } else {
            printf(": ");
        }
    }
}

internal_proc void
print(Aggregate_Field *item) {
    if ( !item ) return;

    if ( item->flags & PF_USING ) {
        printf("using ");
    }

    printf("%s: ", item->name);

    if ( item->typespec ) {
        print(item->typespec);
        printf(" ");
    }

    if ( item->expr ) {
        print(item->expr);
    }
}

internal_proc void
print(Compound_Argument *arg) {
    print(arg->expr);
}

internal_proc void
print(Else_If *elseif) {
    print_indent();
    printf("(elseif ");
    print(elseif->condition);
    printf("\n");

    inc_indent();
    print(elseif->block);
    dec_indent();

    printf(")");
}

internal_proc void
print(Typespec *typespec) {
    if ( !typespec ) return;

    switch ( typespec->kind ) {
        case TYPESPEC_NAME: {
            printf("%s", typespec->typespec_name.value);
        } break;

        case TYPESPEC_PROC: {
            printf("(");
            for ( size_t i = 0; i < typespec->typespec_proc.num_params; ++i ) {
                print(typespec->typespec_proc.params[i]);
                if ( i < typespec->typespec_proc.num_params-1 ) {
                    printf(", ");
                }
            }
            printf(") -> ");
            for ( size_t i = 0; i < typespec->typespec_proc.num_rets; ++i ) {
                print(typespec->typespec_proc.rets[i]);
                if ( i < typespec->typespec_proc.num_rets-1 ) {
                    printf(", ");
                }
            }
        } break;

        case TYPESPEC_PTR: {
            printf("* ");
            print(typespec->typespec_ptr.elem);
        } break;

        case TYPESPEC_ARRAY: {
            printf("[");
            print(typespec->typespec_array.size);
            printf("] ");
            print(typespec->typespec_array.elem);
        } break;

        default: {
            assert(0);
        } break;
    }
}

internal_proc void
print(Stmt_Block block) {
    for ( size_t i = 0; i < block.num_stmts; ++i ) {
        print(block.stmts[i]);
        if ( i < block.num_stmts-1 ) {
            printf("\n");
        }
    }
}

internal_proc void
print(Stmt *stmt) {
    if ( !stmt ) return;

    for ( size_t i = 0; i < stmt->num_notes; ++i ) {
        print(&stmt->notes[i]);
    }

    print_indent();
    switch ( stmt->kind ) {
        case STMT_DECL: {
            /*
             * da statements bereits eingerückt werden, muss für
             * das print einer deklaration die einrückung verringert werden,
             * da deklarationen nochmal eine einrückung printen :/
             */
            dec_indent();
            print(stmt->stmt_decl.decl);
            inc_indent();
        } break;

        case STMT_IF: {
            printf("(if ");
            print(stmt->stmt_if.if_expr);
            printf("\n");

            inc_indent();
            print(stmt->stmt_if.then_block);
            dec_indent();

            if ( stmt->stmt_if.num_else_ifs ) {
                for ( size_t i = 0; i < stmt->stmt_if.num_else_ifs; ++i ) {
                    printf("\n");
                    print(&stmt->stmt_if.else_ifs[i]);
                }
            }

            if ( stmt->stmt_if.else_block.num_stmts ) {
                printf("\n");
                print_indent();
                printf("(else\n");

                inc_indent();
                print(stmt->stmt_if.else_block);
                dec_indent();

                printf(")");
            }
            printf(")");
        } break;

        case STMT_RETURN: {
            printf("(return ");
            for ( size_t i = 0; i < stmt->stmt_return.num_exprs; ++i ) {
                print(stmt->stmt_return.exprs[i]);
                if ( i < stmt->stmt_return.num_exprs-1 ) {
                    printf(" ");
                }
            }
            printf(")");
        } break;

        case STMT_EXPR: {
            print(stmt->stmt_expr.expr);
        } break;

        case STMT_FOR: {
            printf("(loop ");
            if ( stmt->stmt_for.iterator ) {
                print(stmt->stmt_for.iterator);
                printf(" : ");
            }
            print(stmt->stmt_for.condition);
            printf("\n");

            inc_indent();
            print(stmt->stmt_for.block);
            dec_indent();

            printf(")");
        } break;

        case STMT_WHILE: {
            printf("(while ");
            print(stmt->stmt_while.expr);
            printf("\n");

            inc_indent();
            print(stmt->stmt_while.block);
            dec_indent();
            printf(")");
        } break;

        case STMT_SWITCH: {
            printf("(switch ");
            print(stmt->stmt_switch.expr);
            for ( size_t i = 0; i < stmt->stmt_switch.num_switch_cases; ++i ) {
                printf("\n");
                print(&stmt->stmt_switch.switch_cases[i]);
            }
            printf(")");
        } break;

        case STMT_ASSIGN: {
            printf("(%s ", to_string(stmt->stmt_assign.op));
            print(stmt->stmt_assign.left);
            printf(" ");
            print(stmt->stmt_assign.right);
            printf(")");
        } break;

        case STMT_BREAK: {
            printf("(break)");
        } break;

        case STMT_USING: {
            printf("(using ");
            print(stmt->stmt_using.expr);
            printf(")");
        } break;

        case STMT_BLOCK: {
            printf("(\n");
            inc_indent();
            print(stmt->stmt_block.block);
            dec_indent();
            printf("\n");
            print_indent();
            printf(")");
        } break;

        default: {
            assert(0);
        } break;
    }
}

internal_proc void
print(Expr *expr) {
    if ( !expr ) return;

    switch ( expr->kind ) {
        case EXPR_PAREN: {
            printf("(");
            print(expr->expr_paren.expr);
            printf(")");
        } break;

        case EXPR_INT: {
            printf("%lld", expr->expr_int.value);
        } break;

        case EXPR_FLOAT: {
            printf("%f", expr->expr_float.value);
        } break;

        case EXPR_STR: {
            printf("%s", expr->expr_str.value);
        } break;

        case EXPR_CHAR: {
            printf("%c", (char)expr->expr_int.value);
        } break;

        case EXPR_NAME: {
            printf("%s", expr->expr_name.value);
        } break;

        case EXPR_INDEX: {
            printf("([] ");
            print(expr->expr_index.expr);
            printf(" ");
            print(expr->expr_index.index);
            printf(")");
        } break;

        case EXPR_FIELD: {
            printf("(. ");
            print(expr->expr_field.expr);
            printf(" ");
            printf("%s", expr->expr_field.field);
            printf(")");
        } break;

        case EXPR_UNARY: {
            printf("(%s ", to_string(expr->expr_unary.op));
            print(expr->expr_unary.expr);
            printf(")");
        } break;

        case EXPR_BINARY: {
            printf("(%s ", to_string(expr->expr_binary.op));
            print(expr->expr_binary.left);
            printf(" ");
            print(expr->expr_binary.right);
            printf(")");
        } break;

        case EXPR_CALL: {
            printf("(");
            print(expr->expr_call.expr);

            for ( size_t i = 0; i < expr->expr_call.num_args; ++i ) {
                printf(" ");
                print(&expr->expr_call.args[i]);
            }
            printf(")");
        } break;

        case EXPR_COMPOUND: {
            printf("(");
            for ( size_t i = 0; i < expr->expr_compound.num_args; ++i ) {
                print(&expr->expr_compound.args[i]);
                if ( i < expr->expr_compound.num_args-1 ) {
                    printf(" ");
                }
            }
            printf(")");
        } break;

        case EXPR_CAST: {
            printf("(cast ");
            print(expr->expr_cast.typespec);
            printf(" ");
            print(expr->expr_cast.expr);
            printf(")");
        } break;

        default: {
            assert(0);
        } break;
    }
}

internal_proc void
print(Decl *decl) {
    if ( !decl ) return;

    for ( size_t i = 0; i < decl->num_notes; ++i ) {
        print(&decl->notes[i]);
    }

    print_indent();
    switch ( decl->kind ) {
        case DECL_VAR: {
            printf("(%s: ", decl->name);
            print(decl->decl_var.typespec);
            printf(" ");
            print(decl->decl_var.init);
            printf(")");
        } break;

        case DECL_CONST: {
            printf("(const %s: ", decl->name);
            print(decl->decl_var.typespec);
            print(decl->decl_var.init);
            printf(")");
        } break;

        case DECL_STRUCT: {
            printf("(struct %s: \n", decl->name);
            inc_indent(2);
            for ( size_t i = 0; i < decl->decl_aggr.num_fields; ++i ) {
                print_indent();
                print(&decl->decl_aggr.fields[i]);
                if ( i < decl->decl_aggr.num_fields-1 ) {
                    printf("\n");
                }
            }
            dec_indent(2);
            printf(")");
        } break;

        case DECL_ENUM: {
            printf("(enum %s\n", decl->name);
            inc_indent();
            for ( size_t i = 0; i < decl->decl_aggr.num_fields; ++i ) {
                print_indent();
                print(&decl->decl_aggr.fields[i]);
                if ( i < decl->decl_aggr.num_fields-1 ) {
                    printf("\n");
                }
            }
            dec_indent();
            printf(")");
        } break;

        case DECL_PROC: {
            printf("(%s: (", decl->name);
            for ( size_t i = 0; i < decl->decl_proc.num_params; ++i ) {
                print(decl->decl_proc.params[i]);
            }
            printf(") ");
            if ( decl->decl_proc.num_rets > 0 ) {
                printf("-> ");
                for ( size_t i = 0; i < decl->decl_proc.num_rets; ++i ) {
                    print(decl->decl_proc.rets[i]);
                    if ( i < decl->decl_proc.num_rets-1 ) {
                        printf(", ");
                    }
                }
            }
            printf("\n");
            inc_indent();
            print(decl->decl_proc.block);
            dec_indent();
            printf(")");
        } break;

        case DECL_TYPEDEF: {
            printf("(typedef %s:", decl->name);
            print(decl->decl_typedef.typespec);
            printf(")");
        } break;

        default: {
            assert(0);
        } break;
    }
}
