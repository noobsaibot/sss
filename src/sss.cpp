internal_proc void
sss_compile_file(char *filename) {
    init_keywords();
    init_datatypes();
    init_commands();
    init_global_syms();
    init_typeids();
    init_builtin_syms();

    add_lexer(init_compiler(filename));
    sym_global_decls(parse_program());
    modules_inject();
    finalize_syms();
/*
    gen_c();
    file_write(create_cname(filename), gen_result, strlen(gen_result));
*/
}

internal_proc char *
sss_compile_string(char *str) {
    init_keywords();
    init_datatypes();
    init_commands();
    init_global_syms();
    init_typeids();
    init_builtin_syms();

    add_lexer(create_lexer(to_string(str)));
    sym_global_decls(parse_program());
    modules_inject();

    finalize_syms();

/*
    gen_c();
    char *result = gen_result;
    gen_result = NULL;

    return result;
*/
    return "";
}

internal_proc void
sss_test(char *filename) {
    global_module.scope = scope_new("globaler bereich");
    current_scope = global_module.scope;

    sss_compile_file(filename);
}

internal_proc bool
sss_main(int argc, char **argv) {
    if ( argc < 2 ) {
        printf("\nnutzung:\n\n\tsss.exe <dateiname>");
        exit(1);
    }

    sss_test(argv[1]);
    printf("kompilierung war erfolgreich!");

    return 0;
}
