#include "ast.h"

void *ast_alloc(size_t size) {
    assert(size != 0);
    void *ptr = xmalloc(size);
    memset(ptr, 0, size);
    return ptr;
}

void *ast_dup(void *src, size_t size) {
    if (size == 0) {
        return NULL;
    }
    void *ptr = xmalloc(size);
    memcpy(ptr, src, size);
    return ptr;
}

#define AST_DUP(x) ast_dup(x, num_##x * sizeof(*x))

internal_proc Note
note_new(Site site, char *tag) {
    Note result = {};

    result.tag = tag;
    result.site = site;

    return result;
}

internal_proc Call_Arg
call_arg(Expr *expr, Expr *name = 0) {
    Call_Arg result = {};

    result.expr = expr;
    result.name = name;

    return result;
}

internal_proc Proc_Param
proc_param(char *name, Typespec *typespec, Expr *default_value, uint32_t flags = PF_NONE) {
    Proc_Param result = {};

    result.name = name;
    result.typespec = typespec;
    result.default_value = default_value;
    result.flags = flags;

    return result;
}

internal_proc Switch_Case
switch_case( Site site, Expr **exprs, size_t num_exprs, uint32_t is_default, Stmt_Block block ) {
    Switch_Case result = {};

    result.exprs = exprs;
    result.num_exprs = num_exprs;
    result.block = block;
    result.is_default = is_default;

    return result;
}

internal_proc Aggregate_Field
aggregate_item(Site site, char *name, Typespec *typespec, Expr *expr, uint32_t flags) {
    Aggregate_Field result = {};

    result.site      = site;
    result.name      = name;
    result.typespec  = typespec;
    result.expr      = expr;
    result.flags     = flags;

    return result;
}

internal_proc Compound_Argument
compound_argument(Site site, char *name, Expr *index, Expr *expr, Comp_Arg_Kind kind) {
    Compound_Argument result = {};

    result.site = site;
    result.name = name;
    result.index = index;
    result.expr = expr;
    result.kind = kind;

    return result;
}

internal_proc Typespec *
typespec_new(Site site, Typespec_Kind kind) {
    Typespec *result = (Typespec *)xmalloc(sizeof(Typespec));

    result->site = site;
    result->kind = kind;

    return result;
}

internal_proc Typespec *
typespec_variadic(Site site) {
    Typespec *result = typespec_new(site, TYPESPEC_VARIADIC);

    return result;
}

internal_proc Typespec *
typespec_name(Site site, char *name) {
    Typespec *result = typespec_new(site, TYPESPEC_NAME);

    result->typespec_name.value = name;

    return result;
}

internal_proc Typespec *
typespec_scope(Site site, Expr *expr) {
    Typespec *result = typespec_new(site, TYPESPEC_SCOPE);

    result->typespec_scope.expr = expr;

    return result;
}

internal_proc Typespec *
typespec_proc(Site site, Typespec **params, size_t num_params, Typespec **rets, size_t num_rets) {
    Typespec *result = typespec_new(site, TYPESPEC_PROC);

    result->typespec_proc.params     = (Typespec **)AST_DUP(params);
    result->typespec_proc.num_params = num_params;
    result->typespec_proc.rets       = rets;
    result->typespec_proc.num_rets   = num_rets;

    return result;
}

internal_proc Typespec *
typespec_ptr(Site site, Typespec *elem) {
    Typespec *result = typespec_new(site, TYPESPEC_PTR);

    result->typespec_ptr.elem = elem;

    return result;
}

internal_proc Typespec *
typespec_array(Site site, Typespec *elem, Expr *size) {
    Typespec *result = typespec_new(site, TYPESPEC_ARRAY);

    result->typespec_array.elem = elem;
    result->typespec_array.size = size;

    return result;
}

internal_proc Typespec *
typespec_block(Site site, Decl **decls, size_t num_decls) {
    Typespec *result = typespec_new(site, TYPESPEC_BLOCK);

    result->typespec_block.decls     = decls;
    result->typespec_block.num_decls = num_decls;

    return result;
}

internal_proc Decl *
decl_new(Site site, Decl_Kind kind, char *name) {
    Decl *result = (Decl *)xcalloc(1, sizeof(Decl));

    result->site      = site;
    result->kind      = kind;
    result->name      = name;

    return result;
}

internal_proc Decl *
decl_var(Site site, char *name, Typespec *typespec, Expr *init = 0) {
    Decl *result = decl_new(site, DECL_VAR, name);

    result->decl_var.typespec = typespec;
    result->decl_var.init = init;

    return result;
}

internal_proc Decl *
decl_const(Site site, char *name, Typespec *typespec, Expr *expr) {
    Decl *result = decl_new(site, DECL_CONST, name);

    result->decl_const.typespec = typespec;
    result->decl_const.expr = expr;

    return result;
}

internal_proc Decl *
decl_proc(Site site, char *name, Proc_Param *params, size_t num_params,
        Proc_Param *rets, size_t num_rets, Stmt_Block block, b32 is_incomplete)
{
    Decl *result = decl_new(site, DECL_PROC, name);

    result->is_incomplete        = is_incomplete;
    result->decl_proc.params     = (Proc_Param *)AST_DUP(params);
    result->decl_proc.num_params = num_params;
    result->decl_proc.rets       = rets;
    result->decl_proc.num_rets   = num_rets;
    result->decl_proc.block      = block;

    return result;
}

internal_proc Decl *
decl_struct(Site site, char *name, Aggregate_Field *fields, size_t num_fields) {
    Decl *result = decl_new(site, DECL_STRUCT, name);

    result->decl_aggr.fields = fields;
    result->decl_aggr.num_fields = num_fields;

    return result;
}

internal_proc Decl *
decl_union(Site site, char *name, Aggregate_Field *fields, size_t num_fields) {
    Decl *result = decl_new(site, DECL_UNION, name);

    result->decl_aggr.fields = fields;
    result->decl_aggr.num_fields = num_fields;

    return result;
}

internal_proc Decl *
decl_typedef(Site site, char *name, Typespec *typespec) {
    Decl *result = decl_new(site, DECL_TYPEDEF, name);

    result->decl_typedef.typespec = typespec;

    return result;
}

internal_proc Decl *
decl_enum(Site site, char *name, Aggregate_Field *fields,
        size_t num_fields, Typespec *typespec)
{
    Decl *result = decl_new(site, DECL_ENUM, name);

    result->decl_aggr.typespec = typespec;
    result->decl_aggr.fields = fields;
    result->decl_aggr.num_fields = num_fields;

    return result;
}

internal_proc Decl *
decl_foreign(Site site, char *foreign_name, Decl *decl) {
    decl->foreign_name = foreign_name;
    decl->is_foreign   = true;

    return decl;
}

internal_proc Expr *
expr_new(Site site, Expr_Kind kind) {
    Expr *result = (Expr *)xcalloc(1, sizeof(Expr));

    result->site = site;
    result->kind = kind;

    return result;
}

internal_proc Expr *
expr_int(Site site, int64_t value, uint8_t base) {
    Expr *result = expr_new(site, EXPR_INT);

    result->expr_int.value = value;
    result->expr_int.base  = base;

    return result;
}

internal_proc Expr *
expr_char(Site site, int64_t value) {
    Expr *result = expr_new(site, EXPR_CHAR);

    result->expr_char.value = value;

    return result;
}

internal_proc Expr *
expr_float(Site site, double value) {
    Expr *result = expr_new(site, EXPR_FLOAT);

    result->expr_float.value = value;

    return result;
}

internal_proc Expr *
expr_str(Site site, char *value) {
    Expr *result = expr_new(site, EXPR_STR);

    result->expr_str.value = value;

    return result;
}

internal_proc Expr *
expr_name(Site site, char *value) {
    Expr *result = expr_new(site, EXPR_NAME);

    result->expr_name.value = value;

    return result;
}

internal_proc Expr *
expr_unary(Site site, Token_Kind op, Expr *expr) {
    Expr *result = expr_new(site, EXPR_UNARY);

    result->expr_unary.op = op;
    result->expr_unary.expr = expr;

    return result;
}

internal_proc Expr *
expr_binary(Site site, Token_Kind op, Expr *left, Expr *right) {
    Expr *result = expr_new(site, EXPR_BINARY);

    result->expr_binary.op = op;
    result->expr_binary.left = left;
    result->expr_binary.right = right;

    return result;
}

internal_proc Expr *
expr_ternary(Site site, Expr *expr, Expr *then_expr, Expr *else_expr) {
    Expr *result = expr_new(site, EXPR_TERNARY);

    result->expr_ternary.expr = expr;
    result->expr_ternary.then_expr = then_expr;
    result->expr_ternary.else_expr = else_expr;

    return result;
}

internal_proc Expr *
expr_paren(Site site, Expr *expr) {
    Expr *result = expr_new(site, EXPR_PAREN);

    result->expr_paren.expr = expr;

    return result;
}

internal_proc Expr *
expr_index(Site site, Expr *expr, Expr *index) {
    Expr *result = expr_new(site, EXPR_INDEX);

    result->expr_index.expr  = expr;
    result->expr_index.index = index;

    return result;
}

internal_proc Expr *
expr_call(Site site, Expr *expr, Call_Arg *args, size_t num_args) {
    Expr *result = expr_new(site, EXPR_CALL);

    result->expr_call.expr = expr;
    result->expr_call.args = args;
    result->expr_call.num_args = num_args;

    return result;
}

internal_proc Expr *
expr_field(Site site, Expr *expr, char *field) {
    Expr *result = expr_new(site, EXPR_FIELD);

    result->expr_field.expr  = expr;
    result->expr_field.field = field;

    return result;
}

internal_proc Expr *
expr_compound(Site site, Expr_Comp_Kind kind, Typespec *typespec,
        Compound_Argument *args, size_t num_args)
{
    Expr *result = expr_new(site, EXPR_COMPOUND);

    result->expr_compound.kind     = kind;
    result->expr_compound.typespec = typespec;
    result->expr_compound.args     = args;
    result->expr_compound.num_args = num_args;

    return result;
}

internal_proc Expr *
expr_cast(Site site, Typespec *typespec, Expr *expr) {
    Expr *result = expr_new(site, EXPR_CAST);

    result->expr_cast.typespec = typespec;
    result->expr_cast.expr = expr;

    return result;
}

internal_proc Expr *
expr_sizeof(Site site, Typespec *typespec) {
    Expr *result = expr_new(site, EXPR_SIZEOF);

    result->expr_sizeof.typespec = typespec;

    return result;
}

internal_proc Expr *
expr_typeof(Site site, Typespec *typespec) {
    Expr *result = expr_new(site, EXPR_TYPEOF);

    result->expr_typeof.typespec = typespec;

    return result;
}

internal_proc Expr *
expr_typeinfo(Site site, Expr *expr) {
    Expr *result = expr_new(site, EXPR_TYPEINFO);

    result->expr_typeinfo.expr = expr;

    return result;
}

internal_proc Expr *
expr_alignof(Site site, Typespec *typespec) {
    Expr *result = expr_new(site, EXPR_ALIGNOF);

    result->expr_alignof.typespec = typespec;

    return result;
}

internal_proc Expr *
expr_offsetof(Site site, Typespec *typespec, Expr *expr) {
    Expr *result = expr_new(site, EXPR_OFFSETOF);

    result->expr_offsetof.typespec = typespec;
    result->expr_offsetof.expr = expr;

    return result;
}

internal_proc Stmt *
stmt_new(Site site, Stmt_Kind kind) {
    Stmt *result = (Stmt *)xmalloc(sizeof(Stmt));

    result->site = site;
    result->kind = kind;
    result->num_notes = 0;

    return result;
}

internal_proc Stmt *
stmt_break(Site site) {
    Stmt *result = stmt_new(site, STMT_BREAK);

    return result;
}

internal_proc Stmt *
stmt_continue(Site site) {
    Stmt *result = stmt_new(site, STMT_CONTINUE);

    return result;
}

internal_proc Stmt *
stmt_free(Site site, Expr *expr) {
    Stmt *result = stmt_new(site, STMT_FREE);

    result->stmt_free.expr = expr;

    return result;
}

internal_proc Stmt *
stmt_decl(Site site, Decl *decl) {
    Stmt *result = stmt_new(site, STMT_DECL);

    result->stmt_decl.decl      = decl;

    return result;
}

internal_proc Else_If
stmt_elseif(Site site, Expr *expr, Stmt_Block block) {
    Else_If result = {};

    result.site = site;
    result.condition = expr;
    result.block = block;

    return result;
}

internal_proc Stmt *
stmt_if(Site site, Expr *if_expr, Stmt_Block then_block, Else_If *else_ifs, size_t num_else_ifs,
        Stmt_Block else_block)
{
    Stmt *result = stmt_new(site, STMT_IF);

    result->stmt_if.if_expr = if_expr;
    /* @TODO(serge): ast_dup block */
    result->stmt_if.then_block = then_block;
    result->stmt_if.else_ifs = (Else_If *)AST_DUP(else_ifs);
    result->stmt_if.num_else_ifs = num_else_ifs;
    /* @TODO(serge): ast_dup block */
    result->stmt_if.else_block = else_block;

    return result;
}

internal_proc Stmt *
stmt_for(Site site, Expr *iterator, Expr *init, Expr *condition, Stmt_Block block) {
    Stmt *result = stmt_new(site, STMT_FOR);

    result->stmt_for.iterator = iterator;
    result->stmt_for.init = init;
    result->stmt_for.condition = condition;
    /* @TODO(serge): ast_dup block */
    result->stmt_for.block = block;

    return result;
}

internal_proc Stmt *
stmt_switch(Site site, Expr *expr, Switch_Case *switch_cases, size_t num_switch_cases) {
    Stmt *result = stmt_new(site, STMT_SWITCH);

    result->stmt_switch.expr = expr;
    result->stmt_switch.switch_cases = switch_cases;
    result->stmt_switch.num_switch_cases = num_switch_cases;

    return result;
}

internal_proc Stmt *
stmt_while(Site site, Expr *expr, Stmt_Block block) {
    Stmt *result = stmt_new(site, STMT_WHILE);

    result->stmt_while.expr = expr;
    /* @TODO(serge): ast_dup block */
    result->stmt_while.block = block;

    return result;
}

internal_proc Stmt *
stmt_return(Site site, Expr **exprs, size_t num_exprs) {
    Stmt *result = stmt_new(site, STMT_RETURN);

    result->stmt_return.exprs     = exprs;
    result->stmt_return.num_exprs = num_exprs;

    return result;
}

internal_proc Stmt *
stmt_expr(Site site, Expr *expr) {
    Stmt *result = stmt_new(site, STMT_EXPR);

    result->stmt_expr.expr = expr;

    return result;
}

internal_proc Stmt *
stmt_assign(Site site, Token_Kind op, Expr *left, Expr *right) {
    Stmt *result = stmt_new(site, STMT_ASSIGN);

    result->stmt_assign.op = op;
    result->stmt_assign.left = left;
    result->stmt_assign.right = right;

    return result;
}

internal_proc Stmt *
stmt_using(Site site, Expr *expr) {
    Stmt *result = stmt_new(site, STMT_USING);

    result->stmt_using.expr = expr;

    return result;
}

internal_proc Stmt *
stmt_block(Site site, Stmt_Block block) {
    Stmt *result = stmt_new(site, STMT_BLOCK);

    result->stmt_block.block = block;

    return result;
}

internal_proc Stmt *
stmt_defer(Site site, Stmt *stmt) {
    Stmt *result = stmt_new(site, STMT_DEFER);

    result->stmt_defer.stmt = stmt;

    return result;
}

